import {Type, typeName} from '../Type';
import {stdClassesTypeNames} from './StdClasses';
import {stdFunctionsTypeNames} from './StdFunctions';
import {stdNamespace} from './StdNamespace';

export const stdTypeNames = {
    StdInteger: 'lang.Integer',
    StdFloat: 'lang.Float',
    StdString: 'lang.String',
    StdArray: 'lang.Array',
    StdBoolean: 'lang.Boolean',
    StdAny: 'lang.Any',
    StdPrint: 'lang.Print',
    StdPrintLine: 'lang.PrintLine',
}

export const nullType: Type = {
    name: 'Null',
    namespace: stdNamespace
}

export const isStd = (type: Type): boolean => [...stdClassesTypeNames, ...stdFunctionsTypeNames].includes(typeName(type))