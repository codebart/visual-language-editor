import {Method} from '../Class';
import {Type} from '../Type';
import {BooleanType} from './StdTypes';

export const equal = (type: Type): Method => operator(type, 'equal')
export const notEqual = (type: Type): Method => operator(type, 'notEqual')
export const greater = (type: Type): Method => operator(type, 'greater')
export const lesser = (type: Type): Method => operator(type, 'lesser')
export const greaterEqual = (type: Type): Method => operator(type, 'greaterEqual')
export const lesserEqual = (type: Type): Method => operator(type, 'lesserEqual')

const operator = (type: Type, name: string): Method => {
    return {
        name: name,
        parameters: [
            {name: 'other', type: type}
        ],
        return: BooleanType
    }
}