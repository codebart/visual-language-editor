import {Class} from '../../Class';
import {StdInteger} from './Integer';
import {equal, notEqual} from '../Operators';
import {StringType} from '../StdTypes';

export const StdString: Class = {
    constructors: [],
    fields: [],
    interfaces: [],
    methods: [
        {
            name: 'join',
            parameters: [
                {name: 'other', type: StringType}
            ],
            return: StringType
        },
        {
            name: 'substring',
            parameters: [
                {name: 'from', type: StdInteger.type},
                {name: 'to', type: StdInteger.type},
            ],
            return: StringType
        },
        {
            name: 'length',
            parameters: [],
            return: StdInteger.type
        },
        equal(StringType),
        notEqual(StringType)
    ],
    type: StringType
}
