import {Class} from '../../Class';
import {equal, notEqual} from '../Operators';
import {BooleanType} from '../StdTypes';
import {toString} from '../StdCommon';

export const StdBoolean: Class = {
    constructors: [],
    fields: [],
    interfaces: [],
    methods: [
        {
            name: 'negate',
            parameters: [],
            return: BooleanType
        },
        {
            name: 'and',
            parameters: [
                {name: 'other', type: BooleanType}
            ],
            return: BooleanType
        },
        {
            name: 'or',
            parameters: [
                {name: 'other', type: BooleanType}
            ],
            return: BooleanType
        },
        {
            name: 'xor',
            parameters: [
                {name: 'other', type: BooleanType}
            ],
            return: BooleanType
        },
        {
            name: 'nor',
            parameters: [
                {name: 'other', type: BooleanType}
            ],
            return: BooleanType
        },
        {
            name: 'nand',
            parameters: [
                {name: 'other', type: BooleanType}
            ],
            return: BooleanType
        },
        toString(),
        equal(BooleanType),
        notEqual(BooleanType),
    ],
    type: BooleanType
}
