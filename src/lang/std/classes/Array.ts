import {Class} from '../../Class';
import {AnyType, ArrayType, BooleanType, IntegerType} from '../StdTypes';

export const StdArray: Class = {
    constructors: [
        {
            name: 'Constructor',
            parameters: [

            ]
        }
    ],
    fields: [],
    interfaces: [],
    methods: [
        {
            name: 'size',
            parameters: [],
            return: IntegerType
        },
        {
            name: 'contains',
            parameters: [
                {name: 'value', type: AnyType}
            ],
            return: BooleanType
        },
        {
            name: 'get',
            parameters: [
                {name: 'index', type: IntegerType}
            ],
            return: AnyType
        },
        {
            name: 'add',
            parameters: [
                {name: 'value', type: AnyType}
            ]
        },
        {
            name: 'remove',
            parameters: [
                {name: 'index', type: IntegerType}
            ]
        },
    ],
    type: ArrayType
}
