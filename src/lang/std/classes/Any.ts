import {Class} from '../../Class';
import {AnyType} from '../StdTypes';

export const StdAny: Class = {
    constructors: [],
    fields: [],
    interfaces: [],
    methods: [],
    type: AnyType
}
