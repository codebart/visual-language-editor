import {Class} from '../../Class';
import {equal, greater, greaterEqual, lesser, lesserEqual, notEqual} from '../Operators';
import {FloatType, IntegerType} from '../StdTypes';
import {toString} from '../StdCommon';

export const StdInteger: Class = {
    constructors: [],
    fields: [],
    interfaces: [],
    methods: [
        {
            name: 'add',
            parameters: [
                {name: 'other', type: IntegerType}
            ],
            return: IntegerType
        },
        {
            name: 'sub',
            parameters: [
                {name: 'other', type: IntegerType}
            ],
            return: IntegerType
        },
        {
            name: 'mul',
            parameters: [
                {name: 'other', type: IntegerType}
            ],
            return: IntegerType
        },
        {
            name: 'div',
            parameters: [
                {name: 'other', type: IntegerType}
            ],
            return: IntegerType
        },
        {
            name: 'mod',
            parameters: [
                {name: 'other', type: IntegerType}
            ],
            return: IntegerType
        },
        {
            name: 'pow',
            parameters: [
                {name: 'other', type: IntegerType}
            ],
            return: IntegerType
        },
        {
            name: 'toFloat',
            parameters: [],
            return: FloatType
        },
        toString(),
        equal(IntegerType),
        notEqual(IntegerType),
        greater(IntegerType),
        lesser(IntegerType),
        greaterEqual(IntegerType),
        lesserEqual(IntegerType),
    ],
    type: IntegerType
}