import {Class} from '../../Class';
import {equal, greater, greaterEqual, lesser, lesserEqual, notEqual} from '../Operators';
import {FloatType, IntegerType} from '../StdTypes';
import {toString} from '../StdCommon';

export const StdFloat: Class = {
    constructors: [],
    fields: [],
    interfaces: [],
    methods: [
        {
            name: 'add',
            parameters: [
                {name: 'other', type: FloatType}
            ],
            return: FloatType
        },
        {
            name: 'sub',
            parameters: [
                {name: 'other', type: FloatType}
            ],
            return: FloatType
        },
        {
            name: 'mul',
            parameters: [
                {name: 'other', type: FloatType}
            ],
            return: FloatType
        },
        {
            name: 'div',
            parameters: [
                {name: 'other', type: FloatType}
            ],
            return: FloatType
        },
        {
            name: 'mod',
            parameters: [
                {name: 'other', type: FloatType}
            ],
            return: FloatType
        },
        {
            name: 'pow',
            parameters: [
                {name: 'other', type: FloatType}
            ],
            return: FloatType
        },
        {
            name: 'toInteger',
            parameters: [],
            return: IntegerType
        },
        toString(),
        equal(FloatType),
        notEqual(FloatType),
        greater(FloatType),
        lesser(FloatType),
        greaterEqual(FloatType),
        lesserEqual(FloatType),
    ],
    type: FloatType
}
