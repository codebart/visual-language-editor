import {Type} from '../Type';
import {stdNamespace} from './StdNamespace';

export const ArrayType: Type = {
    namespace: stdNamespace,
    name: 'Array'
}

export const BooleanType: Type = {
    namespace: stdNamespace,
    name: 'Boolean'
}

export const FloatType: Type = {
    namespace: stdNamespace,
    name: 'Float'
}

export const IntegerType: Type = {
    namespace: stdNamespace,
    name: 'Integer'
}

export const StringType: Type = {
    namespace: stdNamespace,
    name: 'String'
}

export const AnyType: Type = {
    namespace: stdNamespace,
    name: 'Any'
}