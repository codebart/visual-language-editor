import {stdNamespace} from '../StdNamespace';
import {StdString} from '../classes/String';
import {Func} from '../../Function';

export const StdPrint: Func = {
    parameters: [
        {name: 'text', type: StdString.type}
    ],
    type: {
        name: 'Print',
        namespace: stdNamespace
    }
}

export const StdPrintLine: Func = {
    parameters: [
        {name: 'text', type: StdString.type}
    ],
    type: {
        name: 'PrintLine',
        namespace: stdNamespace
    }
}

export const StdReadLine: Func = {
    parameters: [],
    type: {
        name: 'ReadLine',
        namespace: stdNamespace
    },
    return: StdString.type
}