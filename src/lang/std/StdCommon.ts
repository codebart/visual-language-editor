import {Method} from '../Class';
import {StringType} from './StdTypes';

export const toString = (): Method => ({
    name: 'toString',
    parameters: [],
    return: StringType
})