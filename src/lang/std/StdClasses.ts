import {Class} from '../Class';
import {StdFloat} from './classes/Float';
import {StdInteger} from './classes/Integer';
import {StdBoolean} from './classes/Boolean';
import {StdString} from './classes/String';
import {typeName} from '../Type';
import {StdArray} from './classes/Array';

export const stdClasses: Class[] = [
    StdFloat,
    StdInteger,
    StdBoolean,
    StdString,
    StdArray
]

export const stdClassesTypeNames: string[] = stdClasses.map(stdClass => typeName(stdClass.type));
