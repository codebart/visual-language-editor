import {StdPrint, StdPrintLine, StdReadLine} from './functions/PrintStream';
import {Func} from '../Function';
import {typeName} from '../Type';

export const stdFunctions: Func[] = [
    StdPrint,
    StdPrintLine,
    StdReadLine
]

export const stdFunctionsTypeNames: string[] = stdFunctions.map(stdClass => typeName(stdClass.type));
