import {Type} from './Type';
import {Method} from './Class';

export interface Interface {
    type: Type;
    methods: Method[];
    interfaces: Interface[];
}
