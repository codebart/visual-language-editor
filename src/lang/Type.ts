export interface Type {
    namespace: string;
    name: string;
}

export const typeName = (type: Type) => {
    if (type.namespace.length > 0) {
        return type.namespace + '.' + type.name;
    }
    return type.name;
}

export const typesEqual = (t1: Type, t2: Type): boolean => {
    return t1.namespace === t2.namespace && t1.name === t2.name;
}

const typeRegex = /(?:([a-zA-Z]*(?:\.[a-zA-Z]*)*)\.)?([a-zA-Z]*)/i;

export const typeFromString = (type: string): Type => {
    const match = typeRegex.exec(type);
    if (match) {
        return {
            name: match[2],
            namespace: match[1]
        }
    }
    throw Error();
}