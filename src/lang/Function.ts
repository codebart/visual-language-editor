import {Type} from './Type';
import {Parameter} from './Parameter';

export interface Func {
    type: Type;
    parameters: Parameter[];
    return?: Type;
}