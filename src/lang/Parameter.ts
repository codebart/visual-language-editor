import {Type} from './Type';

export interface Parameter {
    name: string;
    type: Type;
}