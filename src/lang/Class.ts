import {Type, typeName} from './Type';
import {Interface} from './Interface';
import {Parameter} from './Parameter';

export interface Class extends Interface {
    constructors: Constructor[];
    methods: Method[];
    fields: Field[];
}

export interface Field {
    name: string;
    type: Type;
}

export interface Method {
    name: string;
    parameters: Parameter[];
    return?: Type;
}

export interface Constructor {
    name: string;
    parameters: Parameter[];
}

export const containsMethod = (methods: Method[], method: Method): boolean => methods.map(m => methodsEqual(m, method)).reduce((a, b) => a || b, false);
export const methodsEqual = (m1: Method, m2: Method): boolean => m1.name === m2.name
    && m1.parameters.map(parameter => typeName(parameter.type)).containsAll(m2.parameters.map(parameter => typeName(parameter.type)));