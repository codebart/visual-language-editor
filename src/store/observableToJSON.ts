import {observable, toJS} from 'mobx';

interface ObjMap {
    [key: number]: any;
}

export const observableToJSON = (object: any): string => {
    object = toJS(object);
    let id = 0;
    const references: any[] = [object];
    const visitedObjects: any[] = [];
    const objMap: ObjMap = {}
    while (references.length > 0) {
        const ref = references.pop();
        if (ref) {
            visitedObjects.push(ref);
            objMap[id++] = ref;
            for (let refKey of Object.keys(ref)) {
                if (!isInternalKey(refKey)) {
                    const childRef = ref[refKey];
                    if (isMapped(childRef) && !visitedObjects.includes(childRef)) {
                        references.push(childRef);
                    }
                }
            }
        }
    }
    const objSerializedMap: ObjMap = {}
    for (let id of Object.keys(objMap)) {
        const value = objMap[Number(id)];
        if (value.constructor === Array) {
            const reffedArr: any[] = [];
            for (let valueElement of value) {
                reffedArr.push(`$ref:${refId(objMap, valueElement)}`);
            }
            objSerializedMap[Number(id)] = reffedArr;
        } else if (typeof value === 'object') {
            const reffedObj: any = {};
            for (let key of Object.keys(value)) {
                if (!isInternalKey(key)) {
                    const keyVal = value[key];
                    if (keyVal !== null) {
                        if (isMapped(keyVal)) {
                            reffedObj[key] = `$ref:${refId(objMap, keyVal)}`
                        } else {
                            reffedObj[key] = keyVal;
                        }
                    }
                }
            }
            objSerializedMap[Number(id)] = reffedObj;
        }
    }
    return JSON.stringify(objSerializedMap);
}

export const observableFromJSON = (json: string): any => {
    const objMap: ObjMap = JSON.parse(json);
    for (let id of Object.keys(objMap)) {
        objMap[Number(id)] = observable(objMap[Number(id)]);
    }
    for (let id of Object.keys(objMap)) {
        const obj = objMap[Number(id)];
        for (let key of Object.keys(obj)) {
            const value = obj[key];
            if (Array.isArray(value)) {
                const elements = value.clear();
                for (let arrayElement of elements) {
                    if (typeof arrayElement === 'string' && isRefId(arrayElement)) {
                        value.push(objMap[parseRefId(arrayElement)]);
                    } else {
                        value.push(arrayElement);
                    }
                }
            } else if (typeof value === 'string' && isRefId(value)) {
                obj[key] = objMap[parseRefId(value)];
            }
        }
    }
    return objMap[0];
}

const refIdRegex = /\$ref:(\d+)/i;

const isRefId = (value: string): boolean => {
    return refIdRegex.test(value);
}

const parseRefId = (value: string): number => {
    const match = refIdRegex.exec(value);
    if (!match) {
        throw Error('Invalid ref: ' + value);
    }
    return Number(match[1]);
}

const refId = (objMap: ObjMap, searched: any): number => {
    for (let id of Object.keys(objMap)) {
        const value = objMap[Number(id)];
        if (value === searched) {
            return Number(id);
        }
    }
    throw Error('Ref not found');
}

const isMapped = (obj: any): boolean => {
    return Array.isArray(obj) || typeof obj === 'object';
}

const isInternalKey = (key: string): boolean => {
    return key.includes('_');
}