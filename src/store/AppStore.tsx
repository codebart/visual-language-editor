import {useLocalObservable} from 'mobx-react-lite';
import React, {createContext, useContext} from 'react';
import {Project, Store} from './model/Store';
import {
    ParentEntryType,
    FunctionalStructureEntryType,
    StructureEntryType, ChildEntryType, classes, ClassEntry, interfaces, functions
} from './model/StructureEntry';
import {autorun, runInAction} from 'mobx';
import StructureType from './model/StructureType';
import {Class} from '../lang/Class';
import {stdClasses} from '../lang/std/StdClasses';
import {Socket} from './model/Flow';
import {Func} from '../lang/Function';
import {stdFunctions} from '../lang/std/StdFunctions';
import {CompilationTarget} from '../codegen/CompilationTarget';
import {observableFromJSON, observableToJSON} from './observableToJSON';
import {stringify} from 'flatted';
import {Interface} from '../lang/Interface';

const saveProjectInLocalStorage = (project: Project) => {
    const projects = savedProjects();
    if (!projects.includes(project.name)) {
        projects.push(project.name);
    }
    localStorage.setItem('projects', JSON.stringify(projects));
    localStorage.setItem(project.name, observableToJSON(project));
}

const savedProjects = (): string[] => {
    const storage = localStorage.getItem('projects');
    return storage ? JSON.parse(storage) : [];
}

const loadProject = (name: string): Project | null => {
    const storage = localStorage.getItem(name);
    if (!storage) {
        return null;
    }
    return observableFromJSON(storage);
}

const AppStoreContext = createContext<Store>(undefined!);

export const AppStoreProvider = ({children}: { children: any }) => {
    const appStore = useLocalObservable((): Store => ({
        projects: savedProjects(),
        canvas: {
            sockets: new Map<Socket, HTMLDivElement>()
        },
        newProject(name: string) {
            this.project = {
                name: name,
                compilationTarget: CompilationTarget.JAVA,
                structure: {
                    name: name,
                    type: StructureType.DIRECTORY,
                    children: []
                },
                tabs: []
            };
            this.projects.push(name);
            saveProjectInLocalStorage(this.project);
        },
        loadProject(name: string) {
            const loadedProject = loadProject(name);
            if (loadedProject) {
                loadedProject.tabs.clear();
                loadedProject.currentTab = undefined;
                this.project = loadedProject;
            }
        },
        saveProject() {
            if (this.projectOpen) {
                saveProjectInLocalStorage(this.currentProject);
                runInAction(() => this.lastSaved = new Date());
            }
        },
        addStructure(entry: StructureEntryType, parent: ParentEntryType) {
            if (!parent.children.find(child => child.name === entry.name)) {
                switch (parent.type) {
                    case StructureType.DIRECTORY: {
                        parent.children.push(entry);
                        break;
                    }
                    case StructureType.CLASS: {
                        parent.children.push(entry as ChildEntryType);
                        break;
                    }
                }
            }
        },
        closeProject() {
            this.project = undefined;
        },
        openTab(entry: FunctionalStructureEntryType) {
            if (!this.currentProject.tabs.includes(entry)) {
                this.currentProject.tabs.push(entry as FunctionalStructureEntryType);
            }
            this.currentProject.currentTab = entry as FunctionalStructureEntryType;
        },
        removeEntry(entry: StructureEntryType) {
            switch (entry.type) {
                case StructureType.DIRECTORY:
                    break;
                case StructureType.CLASS:
                case StructureType.FUNCTION: {
                    this.currentProject.tabs.removeElements(tab => tab === entry);
                    break;
                }
                case StructureType.METHOD: {
                    switch (entry.parent?.type) {
                        case StructureType.CLASS: {
                            entry.parent?.class.methods.removeElements(method => method.name === entry.name);
                            break;
                        }
                        case StructureType.INTERFACE: {
                            entry.parent?.interface.methods.removeElements(method => method.name === entry.name);
                            break;
                        }
                    }
                    this.currentProject.tabs.removeElements(tab => tab === entry);
                    break;
                }
                case StructureType.CONSTRUCTOR: {
                    const parent = entry.parent as ClassEntry;
                    parent.class.constructors.removeElements(constructor => constructor.name === entry.name);
                    this.currentProject.tabs.removeElements(tab => tab === entry);
                    break;
                }
            }
            if (this.currentProject.currentTab === entry) {
                this.currentProject.currentTab = undefined;
            }
            entry.parent?.children.removeElements(element => element === entry);
        },
        get classes(): Class[] {
            return [...classes(this.currentProject.structure), ...stdClasses];
        },
        get interfaces(): Interface[] {
            return [...interfaces(this.currentProject.structure)];
        },
        get functions(): Func[] {
            return [...functions(this.currentProject.structure), ...stdFunctions];
        },
        get projectOpen(): boolean {
            return !!this.project;
        },
        get currentProject(): Project {
            return this.project as Project;
        }
    }));
    autorun(() => appStore.saveProject(), {
        delay: 4000
    });
    autorun(() => {
        stringify(appStore.currentProject);
        runInAction(() => appStore.lastUpdated = new Date());
    });
    return (
        <AppStoreContext.Provider value={appStore}>
            {children}
        </AppStoreContext.Provider>
    )
}

export const useAppStore = () => {
    const context = useContext(AppStoreContext);
    if (!context) {
        throw new Error('AppStore used outside AppStoreProvider');
    }
    return context;
};