import {ParentEntryType, DirectoryEntry, FunctionalStructureEntryType, StructureEntryType} from './StructureEntry';
import {Class} from '../../lang/Class';
import {Socket} from './Flow';
import {Position} from '../../model/Position';
import {Func} from '../../lang/Function';
import {CompilationTarget} from '../../codegen/CompilationTarget';
import {Interface} from '../../lang/Interface';

export interface Store {
    projects: string[];
    project?: Project;
    canvas: Canvas;
    currentProject: Project;
    projectOpen: boolean;
    lastSaved?: Date;
    lastUpdated?: Date;
    newProject(name: string): void;
    loadProject(name: string): void;
    saveProject(): void;
    closeProject(): void;
    removeEntry(entry: StructureEntryType): void;
    openTab(entry: FunctionalStructureEntryType): void;
    addStructure(entry: StructureEntryType, parent: ParentEntryType): void;
    classes: Class[];
    interfaces: Interface[];
    functions: Func[];
}

export interface Canvas {
    sockets: Map<Socket, HTMLDivElement>;
    selectedSocket?: SelectedSocket;
}

export interface SelectedSocket {
    socketElement: HTMLDivElement;
    socket: Socket;
    currentPosition: Position;
}

export interface Project {
    name: string;
    compilationTarget: CompilationTarget;
    structure: DirectoryEntry;
    tabs: FunctionalStructureEntryType[];
    currentTab?: FunctionalStructureEntryType;
}