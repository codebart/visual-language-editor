import StructureType from './StructureType';
import {FlowImplementation} from './Flow';
import {Class, Constructor, Method} from '../../lang/Class';
import {Interface} from '../../lang/Interface';
import {Func} from '../../lang/Function';
import {typeName} from '../../lang/Type';

export type CodeGeneratingEntryType =  ClassEntry | InterfaceEntry | FunctionEntry;
export type FunctionalStructureEntryType = ClassEntry | InterfaceEntry | MethodEntry | FunctionEntry | ConstructorEntry;
export type StructureEntryType = DirectoryEntry | FunctionalStructureEntryType;
export type ParentEntryType = DirectoryEntry | ClassEntry | InterfaceEntry;
export type ChildEntryType = MethodEntry | ConstructorEntry;
export type FlowEntryType = MethodEntry | FunctionEntry | ConstructorEntry;
export type ReturningEntryType = MethodEntry | FunctionEntry;

export interface StructureEntry {
    name: string;
    type: StructureType;
    parent?: ParentEntryType;
}

export interface DirectoryEntry extends StructureEntry {
    type: StructureType.DIRECTORY;
    children: StructureEntryType[];
}

export interface ClassEntry extends StructureEntry {
    type: StructureType.CLASS;
    class: Class;
    children: ChildEntryType[];
}

export interface InterfaceEntry extends StructureEntry {
    type: StructureType.INTERFACE;
    interface: Interface;
    children: ChildEntryType[];
}

export interface MethodEntry extends StructureEntry {
    type: StructureType.METHOD;
    method: Method;
    flow: FlowImplementation;
}

export interface FunctionEntry extends StructureEntry {
    type: StructureType.FUNCTION;
    function: Func;
    flow: FlowImplementation;
}

export interface ConstructorEntry extends StructureEntry {
    type: StructureType.CONSTRUCTOR;
    constructor: Constructor;
    flow: FlowImplementation;
}

export const children = (entry: StructureEntryType): StructureEntryType[] => {
    switch (entry.type) {
        case StructureType.DIRECTORY:
        case StructureType.CLASS:
        case StructureType.INTERFACE:
            return entry.children;
        case StructureType.METHOD:
        case StructureType.FUNCTION:
        case StructureType.CONSTRUCTOR:
            return [];
    }
}

export const hasFlow = (entry: FunctionalStructureEntryType): boolean => {
    return entry.type === StructureType.FUNCTION || entry.type === StructureType.METHOD ||entry.type === StructureType.CONSTRUCTOR;
}

export const isFunctional = (entry: StructureEntryType): boolean => {
    switch (entry.type) {
        case StructureType.DIRECTORY:
            return false;
        case StructureType.CLASS:
        case StructureType.INTERFACE:
        case StructureType.METHOD:
        case StructureType.FUNCTION:
        case StructureType.CONSTRUCTOR:
            return true;
    }
}

export const childByName = (entry: ParentEntryType, name: string): ChildEntryType => {
    return entry.children.find(child => child.name === name) as ChildEntryType;
}

export const classes = (root: ParentEntryType, classesFound: Class[] = []): Class[] => {
    for (let child of root.children) {
        if (child.type === StructureType.CLASS) {
            classesFound.push(child.class);
        } else if (child.type === StructureType.DIRECTORY) {
            classes(child, classesFound);
        }
    }
    return classesFound;
}

export const interfaces = (root: ParentEntryType, interfacesFound: Interface[] = []): Interface[] => {
    for (let child of root.children) {
        if (child.type === StructureType.INTERFACE) {
            interfacesFound.push(child.interface);
        } else if (child.type === StructureType.DIRECTORY) {
            interfaces(child, interfacesFound);
        }
    }
    return interfacesFound;
}

export const functions = (root: ParentEntryType, functionsFound: Func[] = []): Func[] => {
    for (let child of root.children) {
        if (child.type === StructureType.FUNCTION) {
            functionsFound.push(child.function);
        } else if (child.type === StructureType.DIRECTORY) {
            functions(child, functionsFound);
        }
    }
    return functionsFound;
}