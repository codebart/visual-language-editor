enum StructureType {
    DIRECTORY = 'DIRECTORY',
    CLASS = 'CLASS',
    INTERFACE = 'INTERFACE',
    METHOD = 'METHOD',
    FUNCTION = 'FUNCTION',
    CONSTRUCTOR = 'CONSTRUCTOR'
}

export default StructureType;