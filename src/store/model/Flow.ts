import {makeAutoObservable} from 'mobx';
import {Position} from '../../model/Position';
import {Type, typeName} from '../../lang/Type';
import {Class, Constructor, Field, Method} from '../../lang/Class';
import {Interface} from '../../lang/Interface';
import {Func} from '../../lang/Function';
import {Parameter} from '../../lang/Parameter';
import {StdBoolean} from '../../lang/std/classes/Boolean';

export interface FlowImplementation {
    viewport: Position;
    entryPoint: ParametersFlow;
    flows: Flow[];
}

export enum FlowType {
    METHOD = 'METHOD',
    FUNCTION = 'FUNCTION',
    CONSTRUCTOR = 'CONSTRUCTOR',
    PARAMETERS = 'PARAMETERS',
    SET = 'SET',
    RETURN = 'RETURN',
    LITERAL = 'LITERAL',
    FIELD = 'FIELD',
    IF = 'IF',
    FOR = 'FOR',
    WHILE = 'WHILE',
    VARIABLE = 'VARIABLE',
    THIS = 'THIS'
}

export type Flow = MethodFlow
    | FunctionFlow
    | ConstructorFlow
    | ParametersFlow
    | SetFlow
    | ReturnFlow
    | LiteralFlow
    | FieldFlow
    | IfFlow
    | ForFlow
    | WhileFlow
    | VariableFlow
    | ThisFlow;

export interface BaseFlow {
    position: Position;
    flowType: FlowType;
}

export enum SocketType {
    ARGUMENT = 'ARGUMENT',
    FLOW = 'FLOW'
}

export enum SocketDirection {
    INPUT = 'INPUT',
    OUTPUT = 'OUTPUT'
}

export interface Socket {
    socketType: SocketType;
    direction: SocketDirection;
    connectedSockets: Socket[];
}

export interface ArgumentSocket extends Socket {
    name: string;
    argumentType: Type;
    socketType: SocketType.ARGUMENT;
    connectedSockets: ArgumentSocket[];
}

export interface InputArgumentSocket extends ArgumentSocket {
    direction: SocketDirection.INPUT;
    connectedSockets: OutputArgumentSocket[];
}

export interface OutputArgumentSocket extends ArgumentSocket {
    direction: SocketDirection.OUTPUT;
    connectedSockets: InputArgumentSocket[];
}

export interface FlowSocket extends Socket {
    socketType: SocketType.FLOW;
    connectedSockets: FlowSocket[];
}

export interface InputFlowSocket extends FlowSocket {
    direction: SocketDirection.INPUT;
    connectedSockets: OutputFlowSocket[];
}

export interface OutputFlowSocket extends FlowSocket {
    direction: SocketDirection.OUTPUT;
    connectedSockets: InputFlowSocket[];
}

export interface MethodFlow extends BaseFlow {
    flowType: FlowType.METHOD;
    method: Method;
    interface: Interface;
    objectSocket: ArgumentSocket;
    inputFlowSocket: FlowSocket;
    outputFlowSocket: FlowSocket;
    inputSockets: InputArgumentSocket[];
    outputSocket: OutputArgumentSocket | null;
}

export interface FunctionFlow extends BaseFlow {
    flowType: FlowType.FUNCTION;
    function: Func;
    inputSockets: InputArgumentSocket[];
    outputSocket: OutputArgumentSocket | null;
    inputFlowSocket: InputFlowSocket;
    outputFlowSocket: OutputFlowSocket;
}

export interface ConstructorFlow extends BaseFlow {
    flowType: FlowType.CONSTRUCTOR;
    class: Class;
    constructor: Constructor;
    inputFlowSocket: InputFlowSocket;
    outputFlowSocket: OutputFlowSocket;
    inputSockets: InputArgumentSocket[];
    outputSocket: OutputArgumentSocket;
}

export interface ParametersFlow extends BaseFlow {
    flowType: FlowType.PARAMETERS;
    outputFlowSocket: OutputFlowSocket;
    outputSockets: ArgumentSocket[];
    target: Func | Method | Constructor;
}

export interface LiteralFlow extends BaseFlow {
    flowType: FlowType.LITERAL;
    value: string;
    valueType: Type;
    outputSocket: ArgumentSocket;
}

export interface FieldFlow extends BaseFlow {
    flowType: FlowType.FIELD;
    field: Field;
    outputSocket: OutputArgumentSocket;
}

export interface IfFlow extends BaseFlow {
    flowType: FlowType.IF;
    inputFlowSocket: InputFlowSocket;
    outputFlowSocket: OutputFlowSocket;
    trueFlowSocket: OutputFlowSocket;
    falseFlowSocket: OutputFlowSocket;
    conditionSocket: InputArgumentSocket;
}

export interface ForFlow extends BaseFlow {
    flowType: FlowType.FOR;
    inputFlowSocket: InputFlowSocket;
    outputFlowSocket: OutputFlowSocket;
    startValueSocket: InputArgumentSocket | null;
    incrementValueSocket: InputArgumentSocket | null;
    endValueSocket: InputArgumentSocket;
    iterationSocket: OutputArgumentSocket;
    iterationFlowSocket: OutputFlowSocket;
}

export interface WhileFlow extends BaseFlow {
    flowType: FlowType.WHILE;
    inputFlowSocket: InputFlowSocket;
    outputFlowSocket: OutputFlowSocket;
    conditionSocket: InputArgumentSocket;
    iterationFlowSocket: OutputFlowSocket;
}

export interface SetFlow extends BaseFlow {
    flowType: FlowType.SET;
    inputFlowSocket: FlowSocket;
    outputFlowSocket: FlowSocket;
    variableSocket: InputArgumentSocket;
    valueSocket: InputArgumentSocket;
}

export interface ReturnFlow extends BaseFlow {
    flowType: FlowType.RETURN;
    inputFlowSocket: InputFlowSocket;
    inputSocket: InputArgumentSocket | null;
}

export interface VariableFlow extends BaseFlow {
    flowType: FlowType.VARIABLE;
    variableName: string;
    inputFlowSocket: InputFlowSocket;
    outputFlowSocket: OutputFlowSocket;
    inputSocket: InputArgumentSocket;
    outputSocket: OutputArgumentSocket;
}

export interface ThisFlow extends BaseFlow {
    flowType: FlowType.THIS;
    outputSocket: OutputArgumentSocket;
}

export const flowOutputSockets = (flow: Flow): Socket[] => {
    switch (flow.flowType) {
        case FlowType.FUNCTION:
        case FlowType.METHOD: {
            let sockets: Socket[] = [flow.outputFlowSocket];
            const outputSocket = flow.outputSocket;
            if (outputSocket) {
                sockets.push(outputSocket);
            }
            return sockets;
        }
        case FlowType.CONSTRUCTOR:
            return [flow.outputFlowSocket, flow.outputSocket];
        case FlowType.PARAMETERS:
            return [...flow.outputSockets, flow.outputFlowSocket];
        case FlowType.SET:
            return [flow.outputFlowSocket];
        case FlowType.RETURN:
            return [];
        case FlowType.LITERAL:
        case FlowType.FIELD:
        case FlowType.VARIABLE:
        case FlowType.THIS:
            return [flow.outputSocket];
        case FlowType.IF:
            return [flow.falseFlowSocket, flow.trueFlowSocket, flow.outputFlowSocket]
        case FlowType.WHILE:
            return [flow.outputFlowSocket, flow.iterationFlowSocket];
    }
    return [];
}

export const flowInputSockets = (flow: Flow): Socket[] => {
    switch (flow.flowType) {
        case FlowType.METHOD:
        case FlowType.FUNCTION:
        case FlowType.CONSTRUCTOR:
            return [flow.inputFlowSocket, ...flow.inputSockets];
        case FlowType.PARAMETERS:
            return [];
        case FlowType.SET:
            return [flow.inputFlowSocket, flow.variableSocket, flow.valueSocket];
        case FlowType.RETURN: {
            const sockets: Socket[] = [flow.inputFlowSocket];
            if (flow.inputSocket) {
                sockets.push(flow.inputSocket);
            }
            return sockets;
        }
        case FlowType.LITERAL:
        case FlowType.FIELD:
            return [];
        case FlowType.IF:
        case FlowType.WHILE:
            return [flow.inputFlowSocket, flow.conditionSocket];
        case FlowType.VARIABLE:
            return [flow.inputSocket];
    }
    return [];
}

export const flowSockets = (flow: Flow): Socket[] => {
    return [...flowInputSockets(flow), ...flowOutputSockets(flow)];
}

export const detachSocket = (socket: Socket): void => {
    for (let connectedSocket of socket.connectedSockets) {
        connectedSocket.connectedSockets.removeElements((s: Socket) => s === socket);
    }
    socket.connectedSockets.clear();
}

export const detachFlow = (flow: Flow): void => flowSockets(flow).forEach(detachSocket);

export const flowImplementation = (object: Func | Method | Constructor): FlowImplementation => {
    const parameters = makeAutoObservable(entryPoint(object));
    return {
        viewport: {x: 0, y: 0},
        entryPoint: parameters,
        flows: [parameters]
    }
}

export const entryPoint = (object: Func | Method | Constructor): ParametersFlow => {
    return {
        position: {x: 0, y: 0},
        flowType: FlowType.PARAMETERS,
        outputFlowSocket: outputFlowSocket(),
        outputSockets: object.parameters.map(parameterSocket),
        target: object
    }
}

export const outputFlowSocket = (): OutputFlowSocket => {
    return {
        connectedSockets: [],
        direction: SocketDirection.OUTPUT,
        socketType: SocketType.FLOW
    }
}

export const methodSocket = (type: Type): ArgumentSocket => {
    return {
        argumentType: type,
        connectedSockets: [],
        name: 'this',
        socketType: SocketType.ARGUMENT,
        direction: SocketDirection.INPUT
    }
}

export const parameterSocket = (parameter: Parameter): ArgumentSocket => {
    return {
        argumentType: parameter.type,
        connectedSockets: [],
        name: parameter.name,
        socketType: SocketType.ARGUMENT,
        direction: SocketDirection.OUTPUT
    }
}

export const argumentSocket = (parameter: Parameter): InputArgumentSocket => {
    return {
        argumentType: parameter.type,
        connectedSockets: [],
        name: parameter.name,
        socketType: SocketType.ARGUMENT,
        direction: SocketDirection.INPUT
    }
}

export const outputArgumentSocket = (parameter: Parameter): OutputArgumentSocket => {
    return {
        argumentType: parameter.type,
        connectedSockets: [],
        name: parameter.name,
        socketType: SocketType.ARGUMENT,
        direction: SocketDirection.OUTPUT
    }
}

export const anonymousOutputSocket = (type: Type): OutputArgumentSocket => {
    return {
        argumentType: type,
        connectedSockets: [],
        name: '',
        socketType: SocketType.ARGUMENT,
        direction: SocketDirection.OUTPUT
    }
}

export const anonymousInputSocket = (type: Type): InputArgumentSocket => {
    return {
        argumentType: type,
        connectedSockets: [],
        name: '',
        socketType: SocketType.ARGUMENT,
        direction: SocketDirection.INPUT
    }
}

export const literalFlow = (position: Position, type: Type): LiteralFlow => {
    return {
        position: position,
        flowType: FlowType.LITERAL,
        value: '',
        valueType: type,
        outputSocket: anonymousOutputSocket(type)
    }
}

export const literalFlowValue = (position: Position, type: Type, value: string): LiteralFlow => {
    return {
        position: position,
        flowType: FlowType.LITERAL,
        value: value,
        valueType: type,
        outputSocket: anonymousOutputSocket(type)
    }
}

export const fieldFlow = (position: Position, field: Field): FieldFlow => {
    return {
        position: position,
        flowType: FlowType.FIELD,
        field: field,
        outputSocket: outputArgumentSocket(field)
    }
}

export const inputFlowSocket = (): InputFlowSocket => {
    return {
        connectedSockets: [],
        direction: SocketDirection.INPUT,
        socketType: SocketType.FLOW
    }
}

export const returnFlow = (position: Position, type?: Type): ReturnFlow => {
    return {
        flowType: FlowType.RETURN,
        inputFlowSocket: inputFlowSocket(),
        inputSocket: type ? anonymousInputSocket(type) : null,
        position: position
    }
}

export const functionFlow = (position: Position, func: Func): FunctionFlow => {
    return {
        flowType: FlowType.FUNCTION,
        position: position,
        function: func,
        inputFlowSocket: inputFlowSocket(),
        inputSockets: func.parameters.map(argumentSocket),
        outputFlowSocket: outputFlowSocket(),
        outputSocket: func.return ? anonymousOutputSocket(func.return) : null
    }
}

export const ifFlow = (position: Position): IfFlow => {
    return {
        flowType: FlowType.IF,
        conditionSocket: argumentSocket({
            name: 'condition',
            type: StdBoolean.type
        }),
        falseFlowSocket: outputFlowSocket(),
        trueFlowSocket: outputFlowSocket(),
        inputFlowSocket: inputFlowSocket(),
        outputFlowSocket: outputFlowSocket(),
        position: position
    }
}

export const methodFlow = (position: Position, cls: Class, method: Method): MethodFlow => {
    return {
        flowType: FlowType.METHOD,
        inputFlowSocket: inputFlowSocket(),
        inputSockets: method.parameters.map(argumentSocket),
        interface: cls,
        method: method,
        objectSocket: anonymousInputSocket(cls.type),
        outputFlowSocket: outputFlowSocket(),
        outputSocket: method.return ? outputArgumentSocket({
            name: 'return',
            type: method.return
        }) : null,
        position: position
    }
}

export const whileFlow = (position: Position): WhileFlow => {
    return {
        flowType: FlowType.WHILE,
        conditionSocket: argumentSocket({
            name: 'condition',
            type: StdBoolean.type
        }),
        inputFlowSocket: inputFlowSocket(),
        iterationFlowSocket: outputFlowSocket(),
        outputFlowSocket: outputFlowSocket(),
        position: position
    }
}

export const setFlow = (position: Position, type: Type): SetFlow => {
    return {
        flowType: FlowType.SET,
        inputFlowSocket: inputFlowSocket(),
        outputFlowSocket: outputFlowSocket(),
        position: position,
        valueSocket: argumentSocket({
            name: 'value',
            type: type
        }),
        variableSocket: argumentSocket({
            name: 'variable',
            type: type
        })
    }
}

export const variableFlow = (position: Position, type: Type): VariableFlow => {
    return {
        flowType: FlowType.VARIABLE,
        position: position,
        variableName: '',
        inputFlowSocket: inputFlowSocket(),
        outputFlowSocket: outputFlowSocket(),
        inputSocket: argumentSocket({
            name: 'initializer',
            type: type
        }),
        outputSocket: outputArgumentSocket({
            name: '',
            type: type
        })
    }
}

export const thisFlow = (position: Position, type: Type): ThisFlow => {
    return {
        flowType: FlowType.THIS,
        position: position,
        outputSocket: outputArgumentSocket({
            name: 'this',
            type: type
        })
    }
}

export const constructorFlow = (position: Position, cls: Class, constructor: Constructor): ConstructorFlow => {
    return {
        class: cls,
        constructor: constructor,
        flowType: FlowType.CONSTRUCTOR,
        inputFlowSocket: inputFlowSocket(),
        inputSockets: constructor.parameters.map(argumentSocket),
        outputFlowSocket: outputFlowSocket(),
        outputSocket: outputArgumentSocket({
            name: 'object',
            type: cls.type
        }),
        position: position
    }
}

export const fullMethodFlowName = (flow: MethodFlow): string => {
    return `${typeName(flow.interface.type)}.${flow.method.name}`
}

export const fullMethodName = (type: Type, methodName: string): string => {
    return `${typeName(type)}.${methodName}`
}