export enum PropertyType {
    FIELD = 'FIELD',
    CONSTRUCTOR = 'CONSTRUCTOR',
    METHOD = 'METHOD',
    PARAMETER = 'PARAMETER',
    TYPE = 'TYPE',
    INTERFACE = 'INTERFACE'
}