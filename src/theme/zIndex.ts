interface ZIndex {
    behind: number;
    tooltip: number;
    sidebar: number;
    sidebarOverlay: number;
    modal: number;
    modalOverlay: number;
    popup: number;
    popupOverlay: number;
    navbar: number;
    editorTools: number;
    snackbar: number;
}

export const zIndex: ZIndex = {
    behind: -100,
    navbar: 100,
    sidebarOverlay: 200,
    sidebar: 300,
    popupOverlay: 400,
    popup: 500,
    modalOverlay: 600,
    modal: 700,
    tooltip: 800,
    editorTools: 900,
    snackbar: 1000
}