import {createGlobalStyle} from 'styled-components';
import {StyledIconBase} from '@styled-icons/styled-icon'

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    background: ${({theme}) => theme.color.backgroundDark};
    font-family: ${({theme}) => theme.font.family};
    font-size: ${({theme}) => theme.font.normal};
    color: ${({theme}) => theme.color.primaryFont};
    user-select: none;
  }

  code {
    font-family: source-code-pro, Menlo, Monaco, Consolas, 'Courier New', monospace;
  }

  a {
    text-decoration: none;
    color: black;

    :visited {
      color: black;
    }

    :hover {
      color: black;
    }
  }
  
  hr {
    background-color: ${({theme}) => theme.color.backgroundLight};
    height: 1px;
    margin: 0;
    border: 0 none;
  }

  ${StyledIconBase} {
    width: ${({theme}) => theme.font.normal};
    margin-left: ${({theme}) => theme.spacing.tiny};
    margin-right: ${({theme}) => theme.spacing.small};
  }
`;

export default GlobalStyle;