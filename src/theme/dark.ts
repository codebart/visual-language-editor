import {DefaultTheme} from 'styled-components';

const darkTheme: DefaultTheme = {
    color: {
        primary: '#365880',
        primaryLight: '#4b6eaf',
        primaryDark: '#0d293e',
        secondary: '#4c5052',
        secondaryLight: '#5e6060',
        background: '#313335',
        backgroundLight: '#3c3f41',
        backgroundDark: '#2b2b2b',
        primaryFont: '#bbbbbb',
        secondaryFont: '#919191',
        ternaryFont: '#f6f6f6',
        borderLight: '#515151',
        borderDark: '#323232',
        green: '#5dd15d',
        lightGreen: '#a5c261',
        orange: '#ffa500',
        red: '#ff4646',
        darkRed: '#ca3737',
        pink: '#ffc0cb',
        purple: '#c75eff',
        blue: '#28abf7',
        lightBlue: '#add8e6'
    },
    font: {
        family: '-apple-system, BlinkMacSystemFont, \'Segoe UI\', \'Roboto\', \'Oxygen\', \'Ubuntu\', \'Cantarell\', \'Fira Sans\', \'Droid Sans\', \'Helvetica Neue\', sans-serif;',
        tiny: '10px',
        small: '12px',
        normal: '14px',
        medium: '18px',
        large: '22px',
        huge: '30px',
    },
    spacing: {
        tiny: '2px',
        small: '4px',
        normal: '8px',
        medium: '16px',
        large: '24px',
        huge: '32px',
        massive: '42px',
    },
    size: {
        mini: '50px',
        tiny: '100px',
        small: '150px',
        normal: '200px',
        medium: '250px',
        large: '300px',
        big: '400px',
        huge: '600px',
        massive: '800px',
    },
    flow: {
        color: {
            boolean: '#ff0000',
            string: '#ffc0cb',
            integer: '#659bff',
            float: '#59eaff',
            array: '#9d68d3',
            object: '#ffa500',
            method: '#ffa500',
            constructor: '#a5c261',
            function: '#a20000',
            field: '#c75eff',
            any: 'white',
        }
    }
};

export default darkTheme;