import {CompilationTarget} from './CompilationTarget';
import {CodeGeneratingEntryType} from '../store/model/StructureEntry';
import {Flow} from '../store/model/Flow';
import {indentation, newLine} from './utils';

export interface CodeGenerator {
    compilationTarget: CompilationTarget;
    generate(entry: CodeGeneratingEntryType): string;
}

export interface FlowContext {
    flows: Flow[];
    node?: Flow;
    body: string;
    indentationLevel: number;
    stack: Flow[];
    inlines: Map<Flow, ResolvedFlow>;
    imports: Set<string>;
    append(line: string): void;
}

export const flowContext = (entryPoint: Flow, flows: Flow[]): FlowContext => ({
    node: entryPoint,
    flows: flows,
    body: '',
    indentationLevel: 2,
    stack: [],
    inlines: new Map(),
    imports: new Set(),
    append(line: string) {
        this.body += newLine() + indentation(this.indentationLevel) + line;
    }
})

export interface ResolvedFlow {
    value: string;
}

export const resolvedFlow = (value: string, requiresParenthesis: boolean = false): ResolvedFlow => ({
    get value() {
        return requiresParenthesis ? `(${value})` : value;
    }
})