export const errors = {
    inputArgumentSocketNotConnected: '$$input argument socket not connected$$',
    objectSocketNotConnected: '$$object socket not connected$$',
    objectSocketOutsideFlow: '$$object socket is outside flow$$',
    inputArgumentSocketOutsideFlow: '$$input argument socket is outside flow$$',
    invalidLiteralType: '$$invalid literal type$$',
    invalidLiteralValue: '$$invalid literal value$$',
}