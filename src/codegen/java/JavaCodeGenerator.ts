import {CodeGenerator, flowContext, FlowContext, resolvedFlow} from '../CodeGenerator';
import {CompilationTarget} from '../CompilationTarget';
import {Type, typeName} from '../../lang/Type';
import {ClassEntry, CodeGeneratingEntryType, FunctionEntry, InterfaceEntry, MethodEntry} from '../../store/model/StructureEntry';
import StructureType from '../../store/model/StructureType';
import {Flow, FlowType, fullMethodFlowName, FunctionFlow, IfFlow, MethodFlow, ReturnFlow, SetFlow} from '../../store/model/Flow';
import {errors} from '../errors';
import {indentation, inlinedSocketValue, literalValue, newLine, socketFlow, socketsToArgs} from '../utils';
import {functionBuilins} from './builtin/function/functionBuiltins';
import {methodBuiltins} from './builtin/method/methodBuiltins';
import {containsMethod, Method} from '../../lang/Class';
import {Parameter} from '../../lang/Parameter';
import {stdNamespace} from '../../lang/std/StdNamespace';
import {Interface} from '../../lang/Interface';
import {isStd} from '../../lang/std/Std';

export const javaCodeGenerator = (): CodeGenerator => {
    return {
        compilationTarget: CompilationTarget.JAVA,
        generate(entry: CodeGeneratingEntryType): string {
            switch (entry.type) {
                case StructureType.CLASS:
                    return classTemplate(entry).trim();
                case StructureType.INTERFACE:
                    return interfaceTemplate(entry).trim();
                case StructureType.FUNCTION:
                    return functionTemplate(entry).trim();
            }
            return '';
        }
    }
}

const packageString = (namespace: string) => namespace ? `package ${namespace};` : '';
const parametersString = (parameters: Parameter[]): string => parameters.map(parameter => `${javaTypeName(parameter.type)} ${parameter.name}`).join(', ');
const methodReturnTypeString = (type?: Type): string => type ? javaTypeName(type) : 'void';
const importsString = (imports: Type[]): string => imports.map(importTarget => `import ${typeName(importTarget)};`).join(newLine());
const interfaceImports = (iface: Interface): Type[] => {
    return [
        ...iface.interfaces.map(i => i.type),
        ...iface.methods.filter(method => method.return && !isStd(method.return)).map(method => method.return as Type),
        ...iface.methods.flatMap(method => method.parameters).filter(parameter => !isStd(parameter.type)).map(parameter => parameter.type)
    ];
}

const interfaceTemplate = (entry: InterfaceEntry): string => {
    const namespace = entry.interface.type.namespace;
    const name = entry.interface.type.name;
    const imports = interfaceImports(entry.interface);
    const methods = entry.interface.methods.map(interfaceMethodTemplate).join(newLine(2));
    return `
${packageString(namespace)}

${importsString(imports)}

public interface ${name} {

${methods}

}
`.trim();
}

const interfaceMethodTemplate = (method: Method): string => {
    return `${indentation(1)}${methodReturnTypeString(method.return)} ${method.name}(${parametersString(method.parameters)});`;
}

const functionMethodTemplate = (entry: FunctionEntry): string => {
    const body = methodBody(flowContext(entry.flow.entryPoint, entry.flow.flows));
    return `${indentation(1)}public static ${methodReturnTypeString(entry.function.return)} ${entry.function.type.name.uncapitalize()}(${parametersString(entry.function.parameters)}) {${body}
    }`;
}

const functionTemplate = (entry: FunctionEntry): string => {
    return `${packageString(entry.function.type.namespace)}
    
public class ${entry.function.type.name} {
${functionMethodTemplate(entry)}
}
    `
}

const classTemplate = (entry: ClassEntry): string => {
    const fields = entry.class.fields;
    const imports: Type[] = [];
    const methods = entry.children.filter(child => child.type === StructureType.METHOD).map(child => methodTemplate(entry, child as MethodEntry));
    const name = entry.class.type.name;
    const methodsString = methods.join(newLine());
    const fieldsString = fields.map(field => `private ${field.type.name} ${field.name};`).join(`${newLine()}${indentation(1)}`);
    const interfaces = entry.class.interfaces.map(iface => javaTypeName(iface.type));
    const interfacesString = interfaces.length > 0 ? ` implements ${interfaces.join(', ')}` : '';
    return `
${packageString(entry.class.type.namespace)}
${importsString(imports)}
public class ${name}${interfacesString} {
    ${fieldsString}
    ${methodsString}
}
`.trim();
}

const methodTemplate = (classEntry: ClassEntry, methodEntry: MethodEntry): string => {
    const method = methodEntry.method;
    const body = methodBody(flowContext(methodEntry.flow.entryPoint, methodEntry.flow.flows));
    const interfaceMethods = classEntry.class.interfaces.flatMap(iface => iface.methods)
    const override = containsMethod(interfaceMethods, method);
    const overrideString = override ? `${newLine()}${indentation()}@Override` : '';
    return `${overrideString}
    public ${methodReturnTypeString(method.return)} ${method.name}(${parametersString(method.parameters)}) {${body}
    }`
}

const methodBody = (context: FlowContext): string => {
    let i = 0;
    // Inline constants
    for (let flow of context.flows) {
        switch (flow.flowType) {
            case FlowType.THIS: {
                context.inlines.set(flow, resolvedFlow('this'));
                break;
            }
            case FlowType.LITERAL: {
                context.inlines.set(flow, literalValue(flow));
                break;
            }
            case FlowType.FIELD: {
                context.inlines.set(flow, resolvedFlow(`this.${flow.field.name}`));
                break;
            }
        }
    }

    while ((context.node || context.stack.length > 0) && i++ < 100) {
        if (!context.node) {
            context.node = context.stack.pop() as unknown as Flow;
            processStackPop(context);
        }
        console.group(i + ': Processing ', context.node.flowType)
        switch (context.node.flowType) {
            case FlowType.PARAMETERS: {
                context.node = socketFlow(context.node.outputFlowSocket.connectedSockets[0], context.flows);
                break;
            }
            case FlowType.FUNCTION: {
                processFunction(context.node, context);
                break;
            }
            case FlowType.METHOD: {
                processMethod(context.node, context);
                break;
            }
            case FlowType.RETURN: {
                processReturn(context.node, context);
                break;
            }
            case FlowType.IF: {
                processIf(context.node, context);
                break;
            }
            case FlowType.SET: {
                processSet(context.node, context);
                break;
            }
            case FlowType.CONSTRUCTOR:
            case FlowType.WHILE: {
                context.node = undefined;
                break;
            }
        }
        console.groupEnd();
    }
    return context.body;
}

const processStackPop = (context: FlowContext): void => {
    const node = context.node;
    context.indentationLevel--;
    switch (node?.flowType) {
        case FlowType.METHOD:
        case FlowType.FUNCTION:
        case FlowType.CONSTRUCTOR:
        case FlowType.RETURN:
        case FlowType.IF:
        case FlowType.FOR:
        case FlowType.WHILE: {
            const previousNode = socketFlow(node?.inputFlowSocket.connectedSockets[0], context.flows);
            switch (previousNode?.flowType) {
                case FlowType.IF: {
                    context.append('}');
                    break;
                }
            }
            break;
        }
    }
}

const processSet = (set: SetFlow, context: FlowContext): void => {



}

const processIf = (node: IfFlow, context: FlowContext): void => {
    const conditionFlow = socketFlow(node.conditionSocket.connectedSockets[0], context.flows);
    if (!conditionFlow) {
        context.append(errors.inputArgumentSocketNotConnected);
        return
    }
    const inlined = context.inlines.get(conditionFlow);
    if (!inlined) {
        context.append(errors.inputArgumentSocketOutsideFlow);
        return;
    }
    const result = `if (${inlined.value}) {`;
    context.append(result);
    context.indentationLevel++;

    const afterNode = socketFlow(node.outputFlowSocket.connectedSockets[0], context.flows);
    if (afterNode) {
        context.stack.push(afterNode);
    }
    const falseNode = socketFlow(node.falseFlowSocket.connectedSockets[0], context.flows);
    if (falseNode) {
        context.stack.push(falseNode);
    }
    context.node = socketFlow(node.trueFlowSocket.connectedSockets[0], context.flows);
}

const processFunction = (node: FunctionFlow, context: FlowContext): void => {
    const builtin = functionBuilins.get(typeName(node.function.type));
    if (builtin) {
        console.log('Using function builtin:', typeName(node.function.type));
        const result = builtin(node, context);
        if (!node.outputSocket) {
            context.append(result.value);
        }
    } else {

    }
    context.node = socketFlow(node.outputFlowSocket.connectedSockets[0], context.flows);
}

const processMethod = (node: MethodFlow, context: FlowContext): void => {
    const builtin = methodBuiltins.get(fullMethodFlowName(node));
    if (builtin) {
        console.log('Using method builtin:', fullMethodFlowName(node));
        const result = builtin(node, context);
        if (!node.outputSocket) {
            context.append(result.value);
        }
    } else {
        console.log('Using object method:', node.method.name);
        const objectValue = inlinedSocketValue(node.objectSocket, context);
        const args = socketsToArgs(node.inputSockets, context);
        const result = resolvedFlow(`${objectValue}.${node.method.name}(${args.join(', ')})`);
        context.inlines.set(node, result);
    }
    context.node = socketFlow(node.outputFlowSocket.connectedSockets[0], context.flows);
}

const processReturn = (node: ReturnFlow, context: FlowContext): void => {
    if (node.inputSocket) {
        console.log('Inserting return statement')
        const value = inlinedSocketValue(node.inputSocket, context);
        context.append(`return ${value};`);
    }
    context.node = undefined;
}

const javaTypeName = (type: Type): string => {
    if (type.namespace === stdNamespace) {
        return type.name;
    }
    return type.name;
}