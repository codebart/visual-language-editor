import {fullMethodName, MethodFlow} from '../../../../store/model/Flow';
import {StdString} from '../../../../lang/std/classes/String';
import {StdInteger} from '../../../../lang/std/classes/Integer';
import {StdFloat} from '../../../../lang/std/classes/Float';
import {StdBoolean} from '../../../../lang/std/classes/Boolean';
import {inlinedSocketValue, socketsToArgs} from '../../../utils';
import {FlowContext, ResolvedFlow, resolvedFlow} from '../../../CodeGenerator';

type MethodBuiltin = (flow: MethodFlow, context: FlowContext) => ResolvedFlow;

const operatorBuiltin = (operator: string): MethodBuiltin =>
    (flow: MethodFlow, context: FlowContext): ResolvedFlow => {
        const left = inlinedSocketValue(flow.objectSocket, context);
        const right = inlinedSocketValue(flow.inputSockets[0], context);
        const result = resolvedFlow(`${left} ${operator} ${right}`, true);
        context.inlines.set(flow, result);
        return result;
    }

const toStringBuiltin: MethodBuiltin = (flow: MethodFlow, context: FlowContext): ResolvedFlow => {
    const value = inlinedSocketValue(flow.objectSocket, context);
    const result = resolvedFlow(`String.valueOf(${value})`);
    context.inlines.set(flow, result);
    return result;
}

const staticMethodObjectBuiltin = (name: string): MethodBuiltin =>
    (flow: MethodFlow, context: FlowContext): ResolvedFlow => {
        const args = socketsToArgs([flow.objectSocket, ...flow.inputSockets], context);
        const result = resolvedFlow(`${name}(${args.join(', ')})`);
        context.inlines.set(flow, result);
        return result;
    }

export const methodBuiltins: Map<string, MethodBuiltin> = new Map();
methodBuiltins.set(fullMethodName(StdString.type, 'join'), operatorBuiltin('+'));
methodBuiltins.set(fullMethodName(StdInteger.type, 'add'), operatorBuiltin('+'));
methodBuiltins.set(fullMethodName(StdInteger.type, 'sub'), operatorBuiltin('-'));
methodBuiltins.set(fullMethodName(StdInteger.type, 'mul'), operatorBuiltin('*'));
methodBuiltins.set(fullMethodName(StdInteger.type, 'div'), operatorBuiltin('/'));
methodBuiltins.set(fullMethodName(StdInteger.type, 'mod'), operatorBuiltin('%'));
methodBuiltins.set(fullMethodName(StdInteger.type, 'equal'), operatorBuiltin('=='));
methodBuiltins.set(fullMethodName(StdFloat.type, 'add'), operatorBuiltin('+'));
methodBuiltins.set(fullMethodName(StdFloat.type, 'sub'), operatorBuiltin('-'));
methodBuiltins.set(fullMethodName(StdFloat.type, 'mul'), operatorBuiltin('*'));
methodBuiltins.set(fullMethodName(StdFloat.type, 'div'), operatorBuiltin('/'));
methodBuiltins.set(fullMethodName(StdFloat.type, 'pow'), staticMethodObjectBuiltin('Math.pow'));
methodBuiltins.set(fullMethodName(StdFloat.type, 'equal'), operatorBuiltin('=='));
methodBuiltins.set(fullMethodName(StdBoolean.type, 'and'), operatorBuiltin('&&'));
methodBuiltins.set(fullMethodName(StdBoolean.type, 'or'), operatorBuiltin('||'));
methodBuiltins.set(fullMethodName(StdBoolean.type, 'equal'), operatorBuiltin('=='));
methodBuiltins.set(fullMethodName(StdInteger.type, 'toString'), toStringBuiltin);
methodBuiltins.set(fullMethodName(StdFloat.type, 'toString'), toStringBuiltin);
methodBuiltins.set(fullMethodName(StdBoolean.type, 'toString'), toStringBuiltin);