import {FunctionFlow} from '../../../../store/model/Flow';
import {typeName} from '../../../../lang/Type';
import {StdPrint, StdPrintLine} from '../../../../lang/std/functions/PrintStream';
import {socketsToArgs} from '../../../utils';
import {FlowContext, resolvedFlow, ResolvedFlow} from '../../../CodeGenerator';

type FunctionBuiltin = (flow: FunctionFlow, context: FlowContext) => ResolvedFlow;

const customFunctionNameBuiltin = (name: string): FunctionBuiltin =>
    (flow: FunctionFlow, context: FlowContext): ResolvedFlow => {
        const args = socketsToArgs(flow.inputSockets, context);
        const result = resolvedFlow(`${name}(${args.join(', ')})`);
        context.inlines.set(flow, result);
        return result;
    }

export const functionBuilins: Map<string, FunctionBuiltin> = new Map();
functionBuilins.set(typeName(StdPrintLine.type), customFunctionNameBuiltin('System.out.println'));
functionBuilins.set(typeName(StdPrint.type), customFunctionNameBuiltin('System.out.print'));