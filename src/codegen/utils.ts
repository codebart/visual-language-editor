import {Flow, flowSockets, LiteralFlow, Socket} from '../store/model/Flow';
import {typeName} from '../lang/Type';
import {StdString} from '../lang/std/classes/String';
import {StdInteger} from '../lang/std/classes/Integer';
import {StdBoolean} from '../lang/std/classes/Boolean';
import {StdFloat} from '../lang/std/classes/Float';
import {errors} from './errors';
import {FlowContext, resolvedFlow, ResolvedFlow} from './CodeGenerator';

export const newLine = (count?: number): string => '\n'.repeat(count ? count : 1);

export const socketFlow = (socket: Socket, flows: Flow[]): Flow | undefined => {
    for (let flow of flows) {
        if (flowSockets(flow).includes(socket)) {
            return flow;
        }
    }
}

export const literalValue = (flow: LiteralFlow): ResolvedFlow => {
    if (!flow.value) {
        return resolvedFlow(errors.invalidLiteralValue);
    } else if (!flow.valueType) {
        return resolvedFlow(errors.invalidLiteralType);
    }
    switch (typeName(flow.valueType)) {
        case typeName(StdString.type): {
            return resolvedFlow(`"${flow.value}"`);
        }
        case typeName(StdInteger.type):
        case typeName(StdBoolean.type): {
            return resolvedFlow(flow.value);
        }
        case typeName(StdFloat.type): {
            return resolvedFlow(`${Number(flow.value)}f`);
        }
    }
    return resolvedFlow(errors.invalidLiteralType);
}

const indentationSpaces: number = 4;

export const indentation = (indentationLevel?: number): string => {
    return ' '.repeat(indentationSpaces * (indentationLevel ? indentationLevel : 1));
}

export const socketsToArgs = (sockets: Socket[], context: FlowContext): string[] => {
    const args: string[] = [];
    for (let inputSocket of sockets) {
        const inputFlow = socketFlow(inputSocket.connectedSockets[0], context.flows);
        if (!inputFlow) {
            args.push(errors.inputArgumentSocketNotConnected)
            continue;
        }
        const inlined = context.inlines.get(inputFlow);
        if (!inlined) {
            args.push(errors.inputArgumentSocketOutsideFlow)
            continue;
        }
        args.push(inlined.value);
    }
    return args;
}

export const inlinedSocketValue = (socket: Socket, context: FlowContext): string => {
    if (!socket) {
        return errors.inputArgumentSocketNotConnected;
    }
    const inputFlow = socketFlow(socket.connectedSockets[0], context.flows);
    if (!inputFlow) {
        return errors.inputArgumentSocketNotConnected;
    }
    const inlined = context.inlines.get(inputFlow);
    if (!inlined) {
        return errors.inputArgumentSocketOutsideFlow;
    }
    return inlined.value;
}