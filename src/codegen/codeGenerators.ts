import {CodeGenerator} from './CodeGenerator';
import {CompilationTarget} from './CompilationTarget';
import {javaCodeGenerator} from './java/JavaCodeGenerator';

interface CodeGenerators {
    [key: string]: CodeGenerator;
}

export const codeGenerators: CodeGenerators = {
    [CompilationTarget.JAVA]: javaCodeGenerator()
}