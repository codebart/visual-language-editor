export enum CompilationTarget {
    JAVA = 'JAVA',
    PYTHON = 'PYTHON',
    JAVASCRIPT = 'JAVASCRIPT'
}