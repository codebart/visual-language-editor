export interface Position {
    x: number;
    y: number;
}

export const add = (a: Position, b: Position): Position => {
    return {
        x: a.x + b.x,
        y: a.y + b.y
    }
}

export const sub = (a: Position, b: Position): Position => {
    return {
        x: a.x - b.x,
        y: a.y - b.y
    }
}

export const abs = (position: Position): Position => {
    return {
        x: Math.abs(position.x),
        y: Math.abs(position.y)
    }
}