import {PropertyType} from '../../../store/model/PropertyType';
import CloseIcon from '../../shared/CloseIcon';
import React from 'react';
import styled from 'styled-components';
import {useAppStore} from '../../../store/AppStore';
import {ChildEntryType} from '../../../store/model/StructureEntry';
import {observer} from 'mobx-react-lite';
import {propertyIcon} from '../../shared/Icons';

interface PropertyProps {
    entry?: ChildEntryType;
    name: string;
    type: PropertyType;
    metadata?: string;
    onRemove?(): void;
}

export const Property = observer(({entry, name, type, metadata, onRemove}: PropertyProps) => {
    const store = useAppStore();
    const openTab = () => {
        if (entry && [PropertyType.CONSTRUCTOR, PropertyType.METHOD].includes(type)) {
            store.openTab(entry);
        }
    }
    return (
        <PropertyContainer>
            <PropertySectionContainer onClick={openTab}>
                <PropertySection>
                    {propertyIcon(type)} {name}
                </PropertySection>
                <PropertySection>
                    {metadata && (<PropertyMetadata>[{metadata}]</PropertyMetadata>)}
                </PropertySection>
            </PropertySectionContainer>
            <CloseIcon onClick={onRemove} title={'Remove'}/>
        </PropertyContainer>
    )
})

const PropertySectionContainer = styled.div`
  display: flex;
  justify-content: space-between;
  width: 100%;
`

const PropertySection = styled.div`
  display: flex;
  align-items: center;
`

const PropertyMetadata = styled.span`
  font-weight: bold;
`

const PropertyContainer = styled.div`
  display: flex;
  align-items: center;
  padding-left: ${({theme}) => theme.spacing.large};
  justify-content: space-between;
  
  ${CloseIcon} {
    visibility: hidden;
  }
  
  :hover {
    cursor: pointer;
    background-color: ${({theme}) => theme.color.primaryLight};
    
    ${CloseIcon} {
      visibility: visible;
    }
  }
`