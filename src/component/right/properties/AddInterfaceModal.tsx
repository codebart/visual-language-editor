import React, {useState} from 'react';
import {Modal} from '../../shared/Modal';
import styled from 'styled-components';
import {Combobox} from '../../shared/Combobox';
import {Button} from '../../shared/Button';
import {VisibilityControl} from '../../../hooks/useVisibility';
import {useAppStore} from '../../../store/AppStore';
import {observer} from 'mobx-react-lite';
import {Type, typeFromString, typeName, typesEqual} from '../../../lang/Type';
import {Interface} from '../../../lang/Interface';

type InterfaceAdded = (iface: Interface) => void;

interface AddReturnTypeModalProps {
    visibility: VisibilityControl;
    interfaceAdded: InterfaceAdded;
}

export const AddInterfaceModal = observer(({visibility, interfaceAdded}: AddReturnTypeModalProps) => {
    const {interfaces} = useAppStore();
    const types = interfaces.map(iface => typeName(iface.type));
    const emptyValue = typeFromString(types[0]);
    const [state, setState] = useState<Type>(emptyValue);
    const changeType = (type: string) => {
        setState(typeFromString(type));
    };
    const onSubmit = () => {
        interfaceAdded(interfaces.find(iface => typesEqual(iface.type, state)) as Interface);
        visibility.hide();
        setState(emptyValue);
    };
    return (
        <Modal id={'addInterface'} name={'Add interface'} closeButton visibility={visibility} size={'large'}>
            <SelectTypeContainer>
                <Combobox items={types} onChange={changeType}/>
            </SelectTypeContainer>
            <Actions>
                <Button onClick={onSubmit}><b>Add</b></Button>
            </Actions>
        </Modal>
    )
})

const SelectTypeContainer = styled.div`
  padding: ${({theme}) => theme.spacing.normal};
`

const Actions = styled.div`
  display: flex;
  justify-content: end;
  padding: ${({theme}) => theme.spacing.normal};
`