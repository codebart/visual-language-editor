import React from 'react';
import {PropertiesCategory} from '../PropertiesCategory';
import {Property} from '../Property';
import {ConstructorEntry} from '../../../../store/model/StructureEntry';
import {PropertyType} from '../../../../store/model/PropertyType';
import styled from 'styled-components';
import {AddNamedTypeModal} from '../AddNamedTypeModal';
import {useVisibility} from '../../../../hooks/useVisibility';
import {observer} from 'mobx-react-lite';
import {useEntry} from '../../../../hooks/useEntry';
import {wrap} from '../../../../util/wrap';

interface ConstructorPropertiesProps {
    entry: ConstructorEntry;
}

export const ConstructorProperties = observer(({entry}: ConstructorPropertiesProps) => {
    const addParameterModalVisibility = useVisibility(false);
    const {addParameter, removeParameter} = useEntry(entry);
    return (
        <PropertiesCategories>
            <PropertiesCategory onAddClick={addParameterModalVisibility.show} name={'Parameters'}>
                {entry.constructor.parameters.map(parameter => <Property onRemove={wrap(removeParameter)(parameter)} key={parameter.name} name={parameter.name} type={PropertyType.PARAMETER} metadata={parameter.type.name}/>)}
            </PropertiesCategory>
            <AddNamedTypeModal name={'Add parameter'} namedTypeAdded={addParameter} visibility={addParameterModalVisibility}/>
        </PropertiesCategories>
    )
})

const PropertiesCategories = styled.div`
  padding-top: ${({theme}) => theme.spacing.small};
  padding-left: ${({theme}) => theme.spacing.small};
`