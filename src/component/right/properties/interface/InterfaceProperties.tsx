import React from 'react';
import {PropertyType} from '../../../../store/model/PropertyType';
import {Property} from '../Property';
import {PropertiesCategory} from '../PropertiesCategory';
import styled from 'styled-components';
import {
    childByName,
    InterfaceEntry,
} from '../../../../store/model/StructureEntry';
import {observer} from 'mobx-react-lite';
import {useVisibility} from '../../../../hooks/useVisibility';
import {NameInputModal} from '../../../shared/NameInputModal';
import {useEntry} from '../../../../hooks/useEntry';
import {wrap} from '../../../../util/wrap';
import {AddInterfaceModal} from '../AddInterfaceModal';

interface InterfacePropertiesProps {
    entry: InterfaceEntry;
}

export const InterfaceProperties = observer(({entry}: InterfacePropertiesProps) => {
    const addMethodModalVisibility = useVisibility(false);
    const addInterfaceModalVisibility = useVisibility(false);
    const {addMethod, removeMethod, addInterface, removeInterface} = useEntry(entry);
    return (
        <PropertiesCategories>
            <PropertiesCategory onAddClick={addInterfaceModalVisibility.show} name={'Interfaces'}>
                {entry.interface.interfaces.map(iface => <Property onRemove={wrap(removeInterface)(iface)} entry={childByName(entry, iface.type.name)} key={iface.type.name} name={iface.type.name} type={PropertyType.INTERFACE}/>)}
            </PropertiesCategory>
            <PropertiesCategory onAddClick={addMethodModalVisibility.show} name={'Methods'}>
                {entry.interface.methods.map(method => <Property onRemove={wrap(removeMethod)(method)} entry={childByName(entry, method.name)} key={method.name} name={method.name} type={PropertyType.METHOD}/>)}
            </PropertiesCategory>
            <NameInputModal name={'Add method'} callback={addMethod} visibility={addMethodModalVisibility}/>
            <AddInterfaceModal interfaceAdded={addInterface} visibility={addInterfaceModalVisibility}/>
        </PropertiesCategories>
    )
})

const PropertiesCategories = styled.div`
  padding-top: ${({theme}) => theme.spacing.small};
  padding-left: ${({theme}) => theme.spacing.small};
`