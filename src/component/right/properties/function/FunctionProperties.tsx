import React from 'react';
import {PropertyType} from '../../../../store/model/PropertyType';
import {Property} from '../Property';
import {PropertiesCategory} from '../PropertiesCategory';
import styled from 'styled-components';
import {FunctionEntry} from '../../../../store/model/StructureEntry';
import {observer} from 'mobx-react-lite';
import {useVisibility} from '../../../../hooks/useVisibility';
import {AddNamedTypeModal} from '../AddNamedTypeModal';
import {AddReturnTypeModal} from '../AddReturnTypeModal';
import {typeName} from '../../../../lang/Type';
import {useEntry} from '../../../../hooks/useEntry';
import {wrap} from '../../../../util/wrap';

interface FunctionPropertiesProps {
    entry: FunctionEntry;
}

export const FunctionProperties = observer(({entry}: FunctionPropertiesProps) => {
    const addParameterModalVisibility = useVisibility(false);
    const addReturnModalVisibility = useVisibility(false);
    const {addParameter, removeParameter, addReturnType, removeReturnType} = useEntry(entry);
    return (
        <PropertiesCategories>
            <PropertiesCategory onAddClick={addParameterModalVisibility.show} name={'Parameters'}>
                {entry.function.parameters.map(parameter => <Property onRemove={wrap(removeParameter)(parameter)} key={parameter.name} name={parameter.name} type={PropertyType.PARAMETER} metadata={parameter.type.name}/>)}
            </PropertiesCategory>
            <PropertiesCategory onAddClick={addReturnModalVisibility.show} name={'Return type'}>
                {entry.function.return && (
                    <Property onRemove={removeReturnType} name={typeName(entry.function.return)} type={PropertyType.TYPE}/>
                )}
            </PropertiesCategory>
            <AddNamedTypeModal name={'Add parameter'} namedTypeAdded={addParameter} visibility={addParameterModalVisibility}/>
            <AddReturnTypeModal returnTypeAdded={addReturnType} visibility={addReturnModalVisibility}/>
        </PropertiesCategories>
    )
})

const PropertiesCategories = styled.div`
  padding-top: ${({theme}) => theme.spacing.small};
  padding-left: ${({theme}) => theme.spacing.small};
`