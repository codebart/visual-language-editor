import React, {ReactChild, useState} from 'react';
import {ChevronDown} from '@styled-icons/boxicons-solid/ChevronDown';
import {ChevronUp} from '@styled-icons/boxicons-solid/ChevronUp';
import styled from 'styled-components';
import {PlusMedical} from '@styled-icons/boxicons-regular/PlusMedical';
import {observer} from 'mobx-react-lite';

interface PropertiesCategoryProps {
    name: string;
    children: ReactChild | ReactChild[] | undefined;
    onAddClick?(): void;
}

export const PropertiesCategory = observer(({name, children, onAddClick}: PropertiesCategoryProps) => {
    const [expanded, setExpanded] = useState(true);
    const changeExpanded = () => setExpanded(!expanded);
    return (
        <PropertiesCategoryContainer>
            <PropertiesCategoryHeader>
                <PropertiesCategoryName onClick={changeExpanded}>
                    {expanded ? <ChevronDown/> : <ChevronUp/>}
                    {name}
                </PropertiesCategoryName>
                <AddIcon onClick={onAddClick} title={'Add'}/>
            </PropertiesCategoryHeader>
            {expanded && (
                <PropertiesCategoryList>
                    {children}
                </PropertiesCategoryList>
            )}
        </PropertiesCategoryContainer>
    )
})

const PropertiesCategoryContainer = styled.div`
  margin-top: ${({theme}) => theme.spacing.small};
`

const PropertiesCategoryHeader = styled.div`
  display: flex;
  justify-content: space-between;
`

const PropertiesCategoryName = styled.div`
  :hover {
    cursor: pointer;
  }
`

const PropertiesCategoryList = styled.div`
  margin-top: ${({theme}) => theme.spacing.small};
`

const AddIcon = styled(PlusMedical)`
  :hover {
    cursor: pointer;
    color: ${({theme}) => theme.color.ternaryFont};
  }
`
