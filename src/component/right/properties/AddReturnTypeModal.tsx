import React, {useState} from 'react';
import {Modal} from '../../shared/Modal';
import styled from 'styled-components';
import {Combobox} from '../../shared/Combobox';
import {Button} from '../../shared/Button';
import {VisibilityControl} from '../../../hooks/useVisibility';
import {useAppStore} from '../../../store/AppStore';
import {observer} from 'mobx-react-lite';
import {Type, typeFromString, typeName} from '../../../lang/Type';

type ReturnTypeAdded = (type: Type) => void;

interface AddReturnTypeModalProps {
    visibility: VisibilityControl;
    returnTypeAdded: ReturnTypeAdded;
}

export const AddReturnTypeModal = observer(({visibility, returnTypeAdded}: AddReturnTypeModalProps) => {
    const {classes} = useAppStore();
    const types = classes.map(element => typeName(element.type));
    const emptyValue = typeFromString(types[0]);
    const [state, setState] = useState<Type>(emptyValue);
    const changeType = (type: string) => {
        setState(typeFromString(type));
    };
    const onSubmit = () => {
        returnTypeAdded(state);
        visibility.hide();
        setState(emptyValue);
    };
    return (
        <Modal id={'addNamedType'} name={'Add return type'} closeButton visibility={visibility} size={'large'}>
            <SelectTypeContainer>
                <Combobox items={types} onChange={changeType}/>
            </SelectTypeContainer>
            <Actions>
                <Button onClick={onSubmit}><b>Add</b></Button>
            </Actions>
        </Modal>
    )
})

const SelectTypeContainer = styled.div`
  padding: ${({theme}) => theme.spacing.normal};
`

const Actions = styled.div`
  display: flex;
  justify-content: end;
  padding: ${({theme}) => theme.spacing.normal};
`