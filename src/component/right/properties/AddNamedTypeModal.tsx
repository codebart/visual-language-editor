import React, {useState} from 'react';
import {Modal} from '../../shared/Modal';
import {Input} from '../../shared/Input';
import styled from 'styled-components';
import {Combobox} from '../../shared/Combobox';
import {Button} from '../../shared/Button';
import {VisibilityControl} from '../../../hooks/useVisibility';
import {useAppStore} from '../../../store/AppStore';
import {observer} from 'mobx-react-lite';
import {Field} from '../../../lang/Class';
import {Parameter} from '../../../lang/Parameter';
import {typeFromString, typeName} from '../../../lang/Type';

type NamedTypeAdded = (field: Field | Parameter) => void;

interface AddNamedTypeModalProps {
    name: string;
    visibility: VisibilityControl;
    namedTypeAdded: NamedTypeAdded;
}

export const AddNamedTypeModal = observer(({name, visibility, namedTypeAdded}: AddNamedTypeModalProps) => {
    const {classes} = useAppStore();
    const types = classes.map(element => typeName(element.type));
    const emptyValue = {
        name: '',
        type: typeFromString(types[0])
    };
    const [state, setState] = useState<Field | Parameter>(emptyValue);
    const changeName = ({currentTarget: {value}}: React.FormEvent<HTMLInputElement>) => setState({
        name: value,
        type: state.type
    })
    const changeType = (type: string) => {
        setState({
            name: state.name,
            type: typeFromString(type)
        })
    };
    const onSubmit = () => {
        namedTypeAdded(state);
        visibility.hide();
        setState(emptyValue);
    };
    return (
        <Modal id={'addNamedType'} name={name} closeButton visibility={visibility} size={'large'}>
            <Input onChange={changeName} autoFocus expand placeholder={'Name...'}/>
            <hr/>
            <SelectTypeContainer>
                Type:
                <Combobox size={'normal'} items={types} onChange={changeType}/>
            </SelectTypeContainer>
            <Actions>
                <Button onClick={onSubmit}><b>Add</b></Button>
            </Actions>
        </Modal>
    )
})

const SelectTypeContainer = styled.div`
  padding: ${({theme}) => theme.spacing.normal};
`

const Actions = styled.div`
  display: flex;
  justify-content: end;
  padding: ${({theme}) => theme.spacing.normal};
`