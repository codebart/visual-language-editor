import React from 'react';
import {PropertyType} from '../../../../store/model/PropertyType';
import {Property} from '../Property';
import {PropertiesCategory} from '../PropertiesCategory';
import styled from 'styled-components';
import {
    childByName,
    ClassEntry,
} from '../../../../store/model/StructureEntry';
import {observer} from 'mobx-react-lite';
import {AddNamedTypeModal} from '../AddNamedTypeModal';
import {useVisibility} from '../../../../hooks/useVisibility';
import {NameInputModal} from '../../../shared/NameInputModal';
import {useEntry} from '../../../../hooks/useEntry';
import {wrap} from '../../../../util/wrap';
import {AddInterfaceModal} from '../AddInterfaceModal';

interface ClassPropertiesProps {
    entry: ClassEntry;
}

export const ClassProperties = observer(({entry}: ClassPropertiesProps) => {
    const addFieldModalVisibility = useVisibility(false);
    const addMethodModalVisibility = useVisibility(false);
    const addConstructorModalVisibility = useVisibility(false);
    const addInterfaceModalVisibility = useVisibility(false);
    const {addMethod, removeMethod, addField, removeField, addConstructor, removeConstructor, addInterface, removeInterface} = useEntry(entry);
    return (
        <PropertiesCategories>
            <PropertiesCategory onAddClick={addInterfaceModalVisibility.show} name={'Interfaces'}>
                {entry.class.interfaces.map(iface => <Property onRemove={wrap(removeInterface)(iface)} entry={childByName(entry, iface.type.name)} key={iface.type.name} name={iface.type.name} type={PropertyType.INTERFACE}/>)}
            </PropertiesCategory>
            <PropertiesCategory onAddClick={addFieldModalVisibility.show} name={'Fields'}>
                {entry.class.fields.map(field => <Property onRemove={wrap(removeField)(field)} entry={childByName(entry, field.name)} key={field.name} name={field.name} type={PropertyType.FIELD} metadata={field.type.name}/>)}
            </PropertiesCategory>
            <PropertiesCategory onAddClick={addConstructorModalVisibility.show} name={'Constructors'}>
                {entry.class.constructors.map(constructor => <Property onRemove={wrap(removeConstructor)(constructor)} entry={childByName(entry, constructor.name)} key={constructor.name} name={constructor.name} type={PropertyType.CONSTRUCTOR} metadata={constructor.parameters.map(parameter => parameter.type.name).join(', ')}/>)}
            </PropertiesCategory>
            <PropertiesCategory onAddClick={addMethodModalVisibility.show} name={'Methods'}>
                {entry.class.methods.map(method => <Property onRemove={wrap(removeMethod)(method)} entry={childByName(entry, method.name)} key={method.name} name={method.name} type={PropertyType.METHOD}/>)}
            </PropertiesCategory>
            <AddNamedTypeModal name={'Add field'} namedTypeAdded={addField} visibility={addFieldModalVisibility}/>
            <NameInputModal name={'Add method'} callback={addMethod} visibility={addMethodModalVisibility}/>
            <NameInputModal name={'Add constructor'} callback={addConstructor} visibility={addConstructorModalVisibility}/>
            <AddInterfaceModal interfaceAdded={addInterface} visibility={addInterfaceModalVisibility}/>
        </PropertiesCategories>
    )
})

const PropertiesCategories = styled.div`
  padding-top: ${({theme}) => theme.spacing.small};
  padding-left: ${({theme}) => theme.spacing.small};
`