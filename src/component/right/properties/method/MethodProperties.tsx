import React from 'react';
import {PropertiesCategory} from '../PropertiesCategory';
import {Property} from '../Property';
import {MethodEntry} from '../../../../store/model/StructureEntry';
import {PropertyType} from '../../../../store/model/PropertyType';
import styled from 'styled-components';
import {AddNamedTypeModal} from '../AddNamedTypeModal';
import {useVisibility} from '../../../../hooks/useVisibility';
import {AddReturnTypeModal} from '../AddReturnTypeModal';
import {observer} from 'mobx-react-lite';
import {typeName} from '../../../../lang/Type';
import {useEntry} from '../../../../hooks/useEntry';
import {wrap} from '../../../../util/wrap';

interface MethodPropertiesProps {
    entry: MethodEntry;
}

export const MethodProperties = observer(({entry}: MethodPropertiesProps) => {
    const addParameterModalVisibility = useVisibility(false);
    const addReturnModalVisibility = useVisibility(false);
    const {addParameter, removeParameter, addReturnType, removeReturnType} = useEntry(entry);
    return (
        <PropertiesCategories>
            <PropertiesCategory onAddClick={addParameterModalVisibility.show} name={'Parameters'}>
                {entry.method.parameters.map(parameter => <Property onRemove={wrap(removeParameter)(parameter)} key={parameter.name} name={parameter.name} type={PropertyType.PARAMETER} metadata={parameter.type.name}/>)}
            </PropertiesCategory>
            <PropertiesCategory onAddClick={addReturnModalVisibility.show} name={'Return type'}>
                {entry.method.return && (
                    <Property onRemove={removeReturnType} name={typeName(entry.method.return)} type={PropertyType.TYPE}/>
                )}
            </PropertiesCategory>
            <AddNamedTypeModal name={'Add parameter'} namedTypeAdded={addParameter} visibility={addParameterModalVisibility}/>
            <AddReturnTypeModal returnTypeAdded={addReturnType} visibility={addReturnModalVisibility}/>
        </PropertiesCategories>
    )
})

const PropertiesCategories = styled.div`
  padding-top: ${({theme}) => theme.spacing.small};
  padding-left: ${({theme}) => theme.spacing.small};
`