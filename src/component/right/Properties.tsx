import React, {useCallback} from 'react';
import styled, {DefaultTheme} from 'styled-components';
import {Settings} from '@styled-icons/material/Settings';
import {Minus} from '@styled-icons/entypo/Minus';
import {useVisibility} from '../../hooks/useVisibility';
import {observer} from 'mobx-react-lite';
import {useAppStore} from '../../store/AppStore';
import StructureType from '../../store/model/StructureType';
import {ClassProperties} from './properties/class/ClassProperties';
import {FunctionProperties} from './properties/function/FunctionProperties';
import {MethodProperties} from './properties/method/MethodProperties';
import {ConstructorProperties} from './properties/constructor/ConstructorProperties';
import {InterfaceProperties} from './properties/interface/InterfaceProperties';
import {entryIcon} from '../shared/Icons';

const Properties = observer(() => {
    const {currentProject: {currentTab}} = useAppStore();
    const {visible, show, hide} = useVisibility(true);
    const properties = useCallback(() => {
        switch (currentTab?.type) {
            case StructureType.INTERFACE:
                return <InterfaceProperties entry={currentTab}/>
            case StructureType.METHOD:
                return <MethodProperties entry={currentTab}/>;
            case StructureType.CLASS:
                return <ClassProperties entry={currentTab}/>;
            case StructureType.FUNCTION:
                return <FunctionProperties entry={currentTab}/>;
            case StructureType.CONSTRUCTOR:
                return <ConstructorProperties entry={currentTab}/>;
        }
    }, [currentTab]);
    return (
        <PropertiesContainer expanded={visible && !!currentTab}>
            <PropertiesHeader expanded={visible && !!currentTab}>
                <PropertiesHeaderLeft expanded={visible && !!currentTab} onClick={show}>
                    <Settings/>
                    <span>Properties</span>
                </PropertiesHeaderLeft>
                {visible && <Minimize onClick={hide}/>}
            </PropertiesHeader>
            {visible && !!currentTab && (
                <PropertiesTarget>
                    {entryIcon(currentTab.type)} {currentTab.name}
                </PropertiesTarget>
            )}
            {visible && !!currentTab && properties()}
        </PropertiesContainer>
    )
})

const Minimize = styled(Minus)`
  :hover {
    cursor: pointer;
    color: ${({theme}) => theme.color.ternaryFont};
  }
`

interface PropertiesHeaderLeftProps {
    expanded: boolean;
    theme?: DefaultTheme;
}

const PropertiesHeaderLeft = styled.div<PropertiesHeaderLeftProps>`
  writing-mode: ${({expanded}) => expanded ? 'auto' : 'tb-rl'};
  transform: ${({expanded}) => expanded ? 'none' : 'rotate(-180deg)'};
  display: flex;
  
  span {
    margin-top: ${({theme, expanded}) => expanded ? '0' : theme.spacing.small};
  }

  :hover {
    cursor: ${({expanded}) => expanded ? 'auto' : 'pointer'};
  }
`

const PropertiesTarget = styled.div`
  margin-top: ${({theme}) => theme.spacing.small};
  margin-left: ${({theme}) => theme.spacing.small};
  color: ${({theme}) => theme.color.ternaryFont};
  display: flex;
  align-items: center;
  font-weight: bold;
`

interface PropertiesContainerProps {
    expanded: boolean;
    theme?: DefaultTheme;
}

const PropertiesContainer = styled.aside<PropertiesContainerProps>`
  background-color: ${({theme}) => theme.color.backgroundLight};
  height: 100%;
  border-left: 1px solid ${({theme}) => theme.color.borderDark};
  display: flex;
  flex-direction: column;
  width: ${({expanded}) => expanded ? '15rem' : '2rem'};
  overflow-y: auto;
  overflow-x: hidden;
`

interface PropertiesHeaderProps {
    expanded: boolean;
    theme?: DefaultTheme;
}

const PropertiesHeader = styled.div<PropertiesHeaderProps>`
  padding-left: ${({theme}) => theme.spacing.small};
  color: ${({theme}) => theme.color.ternaryFont};
  display: flex;
  align-items: center;
  height: ${({expanded}) => expanded ? '2rem' : '6rem'};
  border-bottom: 1px solid ${({theme}) => theme.color.borderLight};
  justify-content: space-between;
`

export default Properties;