import {DefaultTheme} from 'styled-components';
import {stdTypeNames} from '../../../lang/std/Std';
import {Type, typeName} from '../../../lang/Type';

export const typeColor = (type: Type, theme: DefaultTheme) => {
    switch (typeName(type)) {
        case stdTypeNames.StdInteger:
            return theme.flow.color.integer;
        case stdTypeNames.StdFloat:
            return theme.flow.color.float;
        case stdTypeNames.StdString:
            return theme.flow.color.string;
        case stdTypeNames.StdArray:
            return theme.flow.color.array;
        case stdTypeNames.StdBoolean:
            return theme.flow.color.boolean;
        case stdTypeNames.StdAny:
            return theme.flow.color.any;
        default:
            return theme.flow.color.object;
    }
}
