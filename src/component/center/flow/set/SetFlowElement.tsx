import React from 'react';
import styled from 'styled-components';
import {SetFlow} from '../../../../store/model/Flow';
import {SetFlowHeader} from './SetFlowHeader';
import {FlowElementContainer} from '../FlowElementContainer';
import {Position} from '../../../../model/Position';
import {ArgumentSocketElement} from '../socket/ArgumentSocketElement';
import {observer} from 'mobx-react-lite';
import {Movable} from '../../../shared/Movable';
import {FlowSocketElement} from '../socket/FlowSocketElement';

interface SetFlowProps {
    flow: SetFlow;
    viewport: Position;
    zoom: number;
}

export const SetFlowElement = observer(({flow, viewport, zoom}: SetFlowProps) => {
    return (
        <FlowElementContainer position={{...flow.position}} viewport={{...viewport}} zoom={zoom}>
            <Movable position={flow.position}>
                <SetFlowHeader flow={flow}/>
            </Movable>
            <SetFlowContainer>
                <InputSockets>
                    <FlowElementSocketList>
                        <FlowSocketElement socket={flow.inputFlowSocket}/>
                        <ArgumentSocketElement socket={flow.variableSocket}/>
                        <ArgumentSocketElement socket={flow.valueSocket}/>
                    </FlowElementSocketList>
                </InputSockets>
                <OutputSockets>
                    <FlowElementSocketList>
                        <FlowSocketElement socket={flow.outputFlowSocket}/>
                    </FlowElementSocketList>
                </OutputSockets>
            </SetFlowContainer>
        </FlowElementContainer>
    )
})

const SetFlowContainer = styled.div`
  display: flex;
  justify-content: space-between;
  min-width: 10rem;
`

const InputSockets = styled.div`
  display: flex;
  padding: ${({theme}) => theme.spacing.small};
`

const OutputSockets = styled.div`
  display: flex;
  padding: ${({theme}) => theme.spacing.small};
  justify-content: flex-end;
`

const FlowElementSocketList = styled.div`
  display: flex;
  flex-direction: column;
`