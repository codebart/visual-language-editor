import React from 'react';
import styled from 'styled-components';
import {SetFlow} from '../../../../store/model/Flow';
import {BracesVariable} from '@styled-icons/fluentui-system-filled/BracesVariable';
import CloseIcon from '../../../shared/CloseIcon';
import {useFlow} from '../../../../hooks/useFlow';

interface SetFlowHeaderProps {
    flow: SetFlow;
}

export const SetFlowHeader = ({flow}: SetFlowHeaderProps) => {
    const {removeFlow} = useFlow(flow);
    return (
        <SetFlowHeaderContainer>
            <SetFlowHeaderElementInfo>
                <BracesVariable/>
                <SetFlowHeaderElementName>
                    Set variable
                </SetFlowHeaderElementName>
            </SetFlowHeaderElementInfo>
            <CloseIconContainer>
                <CloseIcon onClick={removeFlow} title={'Remove'}/>
            </CloseIconContainer>
        </SetFlowHeaderContainer>
    )
}

const CloseIconContainer = styled.div`
  display: flex;
  align-items: center;
`

const SetFlowHeaderContainer = styled.div`
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  padding: ${({theme}) => theme.spacing.small};
  border: 1px solid rgba(255, 255, 255, 0.6);
  background-color: rgba(255, 255, 255, 0.3);
  display: flex;
  justify-content: space-between;

  :hover {
    cursor: move;
  }
`

const SetFlowHeaderElementInfo = styled.div`
  display: flex;
  align-items: center;
`

const SetFlowHeaderElementName = styled.div`
  font-weight: bold;
`