import React from 'react';
import styled from 'styled-components';
import {IfFlow} from '../../../../store/model/Flow';
import {BranchFork} from '@styled-icons/fluentui-system-regular/BranchFork';
import CloseIcon from '../../../shared/CloseIcon';
import {useFlow} from '../../../../hooks/useFlow';

interface IfFlowHeaderProps {
    flow: IfFlow;
}

export const IfFlowHeader = ({flow}: IfFlowHeaderProps) => {
    const {removeFlow} = useFlow(flow);
    return (
        <IfFlowHeaderContainer>
            <IfFlowHeaderElementInfo>
                <BranchFork/>
                <IfFlowHeaderElementName>
                    If
                </IfFlowHeaderElementName>
            </IfFlowHeaderElementInfo>
            <CloseIconContainer>
                <CloseIcon onClick={removeFlow} title={'Remove'}/>
            </CloseIconContainer>
        </IfFlowHeaderContainer>
    )
}

const CloseIconContainer = styled.div`
  display: flex;
  align-items: center;
`

const IfFlowHeaderContainer = styled.div`
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  padding: ${({theme}) => theme.spacing.small};
  border: 1px solid rgba(255, 255, 255, 0.6);
  background-color: rgba(255, 255, 255, 0.3);
  display: flex;
  justify-content: space-between;

  :hover {
    cursor: move;
  }
`

const IfFlowHeaderElementInfo = styled.div`
  display: flex;
  align-items: center;
`

const IfFlowHeaderElementName = styled.div`
  font-weight: bold;
`