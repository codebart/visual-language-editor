import {IfFlow} from '../../../../store/model/Flow';
import {Position} from '../../../../model/Position';
import {FlowElementContainer} from '../FlowElementContainer';
import React from 'react';
import styled from 'styled-components';
import {observer} from 'mobx-react-lite';
import {FlowSocketElement} from '../socket/FlowSocketElement';
import {ArgumentSocketElement} from '../socket/ArgumentSocketElement';
import {IfFlowHeader} from './IfFlowHeader';
import {Movable} from '../../../shared/Movable';

interface IfFlowProps {
    flow: IfFlow;
    viewport: Position;
    zoom: number;
}

export const IfFlowElement = observer(({flow, viewport, zoom}: IfFlowProps) => {
    return (
        <FlowElementContainer position={{...flow.position}} viewport={{...viewport}} zoom={zoom}>
            <Movable position={flow.position}>
                <IfFlowHeader flow={flow}/>
            </Movable>
            <IfFlowContainer>
                <InputSockets>
                    <FlowSocketElement socket={flow.inputFlowSocket}/>
                    <ArgumentSocketElement socket={flow.conditionSocket}/>
                </InputSockets>
                <OutputSockets>
                    <OutputFlowSocket>
                        <OutputFlowSocketLabel>true</OutputFlowSocketLabel>
                        <FlowSocketElement socket={flow.trueFlowSocket}/>
                    </OutputFlowSocket>
                    <OutputFlowSocket>
                        <OutputFlowSocketLabel>false</OutputFlowSocketLabel>
                        <FlowSocketElement socket={flow.falseFlowSocket}/>
                    </OutputFlowSocket>
                    <OutputFlowSocket>
                        <OutputFlowSocketLabel>after</OutputFlowSocketLabel>
                        <FlowSocketElement socket={flow.outputFlowSocket}/>
                    </OutputFlowSocket>
                </OutputSockets>
            </IfFlowContainer>
        </FlowElementContainer>
    )
})

const IfFlowContainer = styled.div`
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  padding: ${({theme}) => theme.spacing.small};
  border-radius: 5px;
  width: 11rem;
`

const InputSockets = styled.div`
`

const OutputSockets = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`

const OutputFlowSocket = styled.div`
  display: flex;  
`

const OutputFlowSocketLabel = styled.span`
  margin-right: ${({theme}) => theme.spacing.normal};
  color: ${({theme}) => theme.color.ternaryFont};
`