import React, {useCallback} from 'react';
import styled, {DefaultTheme} from 'styled-components';
import {VariableFlow} from '../../../../store/model/Flow';
import {FlowElementContainer} from '../FlowElementContainer';
import {Position} from '../../../../model/Position';
import {ArgumentSocketElement} from '../socket/ArgumentSocketElement';
import {observer} from 'mobx-react-lite';
import {Movable} from '../../../shared/Movable';
import {Input} from '../../../shared/Input';
import {typeColor} from '../typeColor';
import {useFlow} from '../../../../hooks/useFlow';
import {Type} from '../../../../lang/Type';
import CloseIcon from '../../../shared/CloseIcon';

interface VariableFlowProps {
    flow: VariableFlow;
    viewport: Position;
    zoom: number;
}

export const VariableFlowElement = observer(({flow, viewport, zoom}: VariableFlowProps) => {
    const {removeFlow} = useFlow(flow);
    const onChange = useCallback((value: string): void => {
        flow.variableName = value.replaceAll(/[^a-zA-Z]/g, '');
    }, [flow]);
    return (
        <FlowElementContainer position={{...flow.position}} viewport={{...viewport}} zoom={zoom}>
            <VariableFlowContainer type={flow.outputSocket.argumentType}>
                <Movable position={flow.position}>
                    <VariableIcon title={'Variable'} type={flow.outputSocket.argumentType}>
                        V
                    </VariableIcon>
                </Movable>
                <CloseIconContainer>
                    <CloseIcon onClick={removeFlow} title={'Remove'}/>
                </CloseIconContainer>
                <VariableNameInput value={flow.variableName} expand placeholder={'Variable name'} onChange={(event) => onChange(event.currentTarget.value)}/>
                <OutputSocket>
                    <ArgumentSocketElement socket={flow.outputSocket}/>
                </OutputSocket>
            </VariableFlowContainer>
        </FlowElementContainer>
    )
})

const VariableNameInput = styled(Input)`
  text-align: right;
  padding-right: 0;
`

const CloseIconContainer = styled.div`
  display: flex;
  align-items: center;
`

interface VariableFlowContainerProps {
    type: Type;
    theme?: DefaultTheme;
}

const VariableFlowContainer = styled.div<VariableFlowContainerProps>`
  display: flex;
  align-items: center;
  padding-right: ${({theme}) => theme.spacing.normal};
  padding-left: ${({theme}) => theme.spacing.small};
  border: 1px solid ${({theme, type}) => typeColor(type, theme)};
  border-radius: 5px;
  width: 10rem;
  height: 2rem;
`

const VariableIcon = styled.div<VariableFlowContainerProps>`
  background-color: ${({theme, type}) => typeColor(type, theme)};
  color: ${({theme}) => theme.color.backgroundDark};
  width: ${({theme}) => theme.font.normal};
  height: ${({theme}) => theme.font.normal};
  font-size: ${({theme}) => theme.font.small};
  border-radius: 25%;
  display: flex;
  align-items: center;
  font-weight: bold;
  justify-content: center;
  margin-right: ${({theme}) => theme.spacing.small};
`

const OutputSocket = styled.div`
  margin-left: ${({theme}) => theme.spacing.normal};
`