import React from 'react';
import styled from 'styled-components';
import {FunctionFlow} from '../../../../store/model/Flow';
import {FunctionFlowHeader} from './FunctionFlowHeader';
import {FlowElementContainer} from '../FlowElementContainer';
import {Position} from '../../../../model/Position';
import {ArgumentSocketElement} from '../socket/ArgumentSocketElement';
import {observer} from 'mobx-react-lite';
import {Movable} from '../../../shared/Movable';
import {FlowSocketElement} from '../socket/FlowSocketElement';

interface FunctionFlowProps {
    flow: FunctionFlow;
    viewport: Position;
    zoom: number;
}

export const FunctionFlowElement = observer(({flow, viewport, zoom}: FunctionFlowProps) => {
    return (
        <FlowElementContainer position={{...flow.position}} viewport={{...viewport}} zoom={zoom}>
            <Movable position={flow.position}>
                <FunctionFlowHeader flow={flow}/>
            </Movable>
            <FunctionFlowContainer>
                <InputSockets>
                    <FlowElementSocketList>
                        <FlowSocketElement socket={flow.inputFlowSocket}/>
                        {flow.inputSockets.map(socket => <ArgumentSocketElement key={socket.name} socket={socket}/>)}
                    </FlowElementSocketList>
                </InputSockets>
                <OutputSockets>
                    <FlowElementSocketList>
                        <FlowSocketElement socket={flow.outputFlowSocket}/>
                        {flow.outputSocket && (
                            <ArgumentSocketElement socket={flow.outputSocket}/>
                        )}
                    </FlowElementSocketList>
                </OutputSockets>
            </FunctionFlowContainer>
        </FlowElementContainer>
    )
})

const FunctionFlowContainer = styled.div`
  display: flex;
  justify-content: space-between;
  min-width: 10rem;
`

const InputSockets = styled.div`
  display: flex;
  padding: ${({theme}) => theme.spacing.small};
`

const OutputSockets = styled.div`
  display: flex;
  padding: ${({theme}) => theme.spacing.small};
  justify-content: flex-end;
`

const FlowElementSocketList = styled.div`
  display: flex;
  flex-direction: column;
`