import React from 'react';
import styled from 'styled-components';
import {FunctionFlow} from '../../../../store/model/Flow';
import {Functions} from '@styled-icons/material/Functions';
import {typeName} from '../../../../lang/Type';
import CloseIcon from '../../../shared/CloseIcon';
import {useFlow} from '../../../../hooks/useFlow';

interface FunctionFlowHeaderProps {
    flow: FunctionFlow;
}

export const FunctionFlowHeader = ({flow}: FunctionFlowHeaderProps) => {
    const {removeFlow} = useFlow(flow);
    return (
        <FunctionFlowHeaderContainer>
            <FunctionFlowHeaderElementInfo>
                <Functions/>
                <FunctionFlowHeaderElementName>
                    <FunctionFlowElementHeaderTitle>
                        {typeName(flow.function.type)}
                    </FunctionFlowElementHeaderTitle>
                    Function
                </FunctionFlowHeaderElementName>
            </FunctionFlowHeaderElementInfo>
            <CloseIconContainer>
                <CloseIcon onClick={removeFlow} title={'Remove'}/>
            </CloseIconContainer>
        </FunctionFlowHeaderContainer>
    )
}

const CloseIconContainer = styled.div`
  display: flex;
  align-items: center;
`

const FunctionFlowHeaderContainer = styled.div`
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  padding: ${({theme}) => theme.spacing.small};
  border: 1px solid rgba(255, 0, 0, 1);
  background-color: rgba(255, 0, 0, 0.5);
  display: flex;
  justify-content: space-between;

  :hover {
    cursor: move;
  }
`

const FunctionFlowElementHeaderTitle = styled.div`
  font-weight: bold;
`

const FunctionFlowHeaderElementInfo = styled.div`
  display: flex;
  align-items: center;
`

const FunctionFlowHeaderElementName = styled.div`
`