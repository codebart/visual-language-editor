import React from 'react';
import styled from 'styled-components';
import {ParametersFlow} from '../../../../store/model/Flow';
import {ParametersFlowHeader} from './ParametersFlowHeader';
import {FlowElementContainer} from '../FlowElementContainer';
import {Position} from '../../../../model/Position';
import {ArgumentSocketElement} from '../socket/ArgumentSocketElement';
import {observer} from 'mobx-react-lite';
import {Movable} from '../../../shared/Movable';
import {FlowSocketElement} from '../socket/FlowSocketElement';

interface ParametersFlowProps {
    flow: ParametersFlow;
    viewport: Position;
    zoom: number;
}

export const ParametersFlowElement = observer(({flow, viewport, zoom}: ParametersFlowProps) => {
    return (
        <FlowElementContainer position={{...flow.position}} viewport={{...viewport}} zoom={zoom}>
            <Movable position={flow.position}>
                <ParametersFlowHeader flow={flow}/>
            </Movable>
            <FlowElementSockets flow={flow}/>
        </FlowElementContainer>
    )
})

interface FlowElementSocketsProps {
    flow: ParametersFlow;
}

const FlowElementSockets = observer(({flow}: FlowElementSocketsProps) => {
    return (
        <FlowElementSocketsContainer>
            <FlowElementSocketList>
                <FlowSocketElement socket={flow.outputFlowSocket}/>
                {flow.outputSockets.map(socket => <ArgumentSocketElement key={socket.name} socket={socket}/>)}
            </FlowElementSocketList>
        </FlowElementSocketsContainer>
    )
})

const FlowElementSocketsContainer = styled.div`
  display: flex;
  padding: ${({theme}) => theme.spacing.small};
  justify-content: flex-end;
`

const FlowElementSocketList = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`