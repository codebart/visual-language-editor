import React from 'react';
import styled from 'styled-components';
import {ParametersFlow} from '../../../../store/model/Flow';
import {Parking} from '@styled-icons/boxicons-solid/Parking';
import {Func} from '../../../../lang/Function';
import {Constructor, Method} from '../../../../lang/Class';

const name = (target: Func | Method | Constructor) => {
    if ('type' in target) {
        return target.type.name;
    }
    return target.name;
}

interface ParametersFlowHeaderProps {
    flow: ParametersFlow;
}

export const ParametersFlowHeader = ({flow}: ParametersFlowHeaderProps) => {
    return (
        <ParametersFlowHeaderContainer>
            <ParametersFlowHeaderElementInfo>
                <Parking/>
                <ParametersFlowHeaderElementName>
                    <ParametersFlowElementHeaderTitle>
                        {name(flow.target)}
                    </ParametersFlowElementHeaderTitle>
                    Parameters
                </ParametersFlowHeaderElementName>
            </ParametersFlowHeaderElementInfo>
        </ParametersFlowHeaderContainer>
    )
}

const ParametersFlowHeaderContainer = styled.div`
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  padding: ${({theme}) => theme.spacing.small};
  border: 1px solid rgba(255, 255, 255, 0.6);
  background-color: rgba(255, 255, 255, 0.3);
  display: flex;
  justify-content: space-between;

  :hover {
    cursor: move;
  }
`

const ParametersFlowElementHeaderTitle = styled.div`
  font-weight: bold;
`

const ParametersFlowHeaderElementInfo = styled.div`
  display: flex;
  align-items: center;
`

const ParametersFlowHeaderElementName = styled.div`
`