import {LiteralFlow} from '../../../../store/model/Flow';
import {Position} from '../../../../model/Position';
import {FlowElementContainer} from '../FlowElementContainer';
import React, {useCallback} from 'react';
import {Input} from '../../../shared/Input';
import {ArgumentSocketElement} from '../socket/ArgumentSocketElement';
import styled, {DefaultTheme} from 'styled-components';
import {typeColor} from '../typeColor';
import {Type, typesEqual} from '../../../../lang/Type';
import {Movable} from '../../../shared/Movable';
import {observer} from 'mobx-react-lite';
import {StdBoolean} from '../../../../lang/std/classes/Boolean';
import {Combobox} from '../../../shared/Combobox';
import CloseIcon from '../../../shared/CloseIcon';
import {useFlow} from '../../../../hooks/useFlow';
import {BooleanType, StringType} from '../../../../lang/std/StdTypes';
import {StdArray} from '../../../../lang/std/classes/Array';

interface LiteralFlowProps {
    flow: LiteralFlow;
    viewport: Position;
    zoom: number;
}

export const LiteralFlowElement = observer(({flow, viewport, zoom}: LiteralFlowProps) => {
    const {removeFlow} = useFlow(flow);
    const onChange = useCallback((value: string): void => {
        flow.value = value;
    }, [flow]);
    const isArray = typesEqual(flow.outputSocket.argumentType, StdArray.type);
    const isBoolean = typesEqual(flow.outputSocket.argumentType, StdBoolean.type);
    return (
        <FlowElementContainer position={{...flow.position}} viewport={{...viewport}} zoom={zoom}>
            <LiteralFlowContainer type={flow.outputSocket.argumentType}>
                <Movable position={flow.position}>
                    <LiteralIcon title={'Literal value'} type={flow.outputSocket.argumentType}>
                        L
                    </LiteralIcon>
                </Movable>
                {!isArray && (
                    <CloseIconContainer>
                        <CloseIcon onClick={removeFlow} title={'Remove'}/>
                    </CloseIconContainer>
                )}
                {isBoolean && (
                    <Combobox defaultValue={flow.value} items={['true', 'false']} onChange={onChange}/>
                )}
                {isArray && (
                    <EmptyArray>
                        Empty array
                    </EmptyArray>
                )}
                {!isBoolean && !isArray && (
                    <Input value={flow.value} expand placeholder={'Value...'} onChange={(event) => onChange(event.currentTarget.value)}/>
                )}
                <OutputSocket>
                    <ArgumentSocketElement socket={flow.outputSocket}/>
                </OutputSocket>
            </LiteralFlowContainer>
        </FlowElementContainer>
    )
})

const EmptyArray = styled.div`
  margin-left: ${({theme}) => theme.spacing.small};
`

const CloseIconContainer = styled.div`
  display: flex;
  align-items: center;
`

const width = (type: Type): number => {
    if (typesEqual(type, BooleanType)) {
        return 9;
    } else if (typesEqual(type, StringType)) {
        return 12;
    }
    return 8;
}

interface LiteralFlowStyleProps {
    type: Type;
    theme?: DefaultTheme;
}

const LiteralFlowContainer = styled.div<LiteralFlowStyleProps>`
  display: flex;
  align-items: center;
  padding-right: ${({theme}) => theme.spacing.small};
  padding-left: ${({theme}) => theme.spacing.small};
  border: 1px solid ${({theme, type}) => typeColor(type, theme)};
  border-radius: 5px;
  width: ${({type}) => width(type)}rem;
  height: 2rem;
`

const LiteralIcon = styled.div<LiteralFlowStyleProps>`
  background-color: ${({theme, type}) => typeColor(type, theme)};
  color: ${({theme}) => theme.color.backgroundDark};
  width: ${({theme}) => theme.font.normal};
  height: ${({theme}) => theme.font.normal};
  font-size: ${({theme}) => theme.font.small};
  border-radius: 25%;
  display: flex;
  align-items: center;
  font-weight: bold;
  justify-content: center;
  margin-right: ${({theme}) => theme.spacing.small};
`

const OutputSocket = styled.div`
  margin-left: ${({theme}) => theme.spacing.normal};
`