import {FieldFlow} from '../../../../store/model/Flow';
import {Position} from '../../../../model/Position';
import {FlowElementContainer} from '../FlowElementContainer';
import React from 'react';
import {ArgumentSocketElement} from '../socket/ArgumentSocketElement';
import styled, {DefaultTheme} from 'styled-components';
import {Movable} from '../../../shared/Movable';
import {observer} from 'mobx-react-lite';
import CloseIcon from '../../../shared/CloseIcon';
import {useFlow} from '../../../../hooks/useFlow';

interface FieldFlowProps {
    flow: FieldFlow;
    viewport: Position;
    zoom: number;
}

export const FieldFlowElement = observer(({flow, viewport, zoom}: FieldFlowProps) => {
    const {removeFlow} = useFlow(flow);
    return (
        <FlowElementContainer position={{...flow.position}} viewport={{...viewport}} zoom={zoom}>
            <FieldFlowContainer>
                <Movable position={flow.position}>
                    <FieldIcon title={'Field'}>
                        F
                    </FieldIcon>
                </Movable>
                <CloseIconContainer>
                    <CloseIcon onClick={removeFlow} title={'Remove'}/>
                </CloseIconContainer>
                <ArgumentSocketElement socket={flow.outputSocket}/>
            </FieldFlowContainer>
        </FlowElementContainer>
    )
})

const CloseIconContainer = styled.div`
  display: flex;
  align-items: center;
`

interface FieldFlowStyleProps {
    theme?: DefaultTheme;
}

const FieldFlowContainer = styled.div<FieldFlowStyleProps>`
  display: flex;
  align-items: center;
  padding-right: ${({theme}) => theme.spacing.small};
  padding-left: ${({theme}) => theme.spacing.small};
  border: 1px solid ${({theme}) => theme.color.purple};
  border-radius: 5px;
  justify-content: space-between;
  height: 2rem;
`

const FieldIcon = styled.div<FieldFlowStyleProps>`
  background-color: ${({theme}) => theme.color.purple};
  color: ${({theme}) => theme.color.backgroundDark};
  width: ${({theme}) => theme.font.normal};
  height: ${({theme}) => theme.font.normal};
  font-size: ${({theme}) => theme.font.small};
  border-radius: 25%;
  display: flex;
  align-items: center;
  font-weight: bold;
  justify-content: center;
  margin-right: ${({theme}) => theme.spacing.small};
`