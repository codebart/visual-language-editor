import React from 'react';
import styled from 'styled-components';
import {WhileFlow} from '../../../../store/model/Flow';
import {Loop} from '@styled-icons/open-iconic/Loop';
import CloseIcon from '../../../shared/CloseIcon';
import {useFlow} from '../../../../hooks/useFlow';

interface WhileFlowHeaderProps {
    flow: WhileFlow;
}

export const WhileFlowHeader = ({flow}: WhileFlowHeaderProps) => {
    const {removeFlow} = useFlow(flow);
    return (
        <WhileFlowHeaderContainer>
            <WhileFlowHeaderElementInfo>
                <Loop/>
                <WhileFlowHeaderElementName>
                    While
                </WhileFlowHeaderElementName>
            </WhileFlowHeaderElementInfo>
            <CloseIconContainer>
                <CloseIcon onClick={removeFlow} title={'Remove'}/>
            </CloseIconContainer>
        </WhileFlowHeaderContainer>
    )
}

const CloseIconContainer = styled.div`
  display: flex;
  align-items: center;
`

const WhileFlowHeaderContainer = styled.div`
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  padding: ${({theme}) => theme.spacing.small};
  border: 1px solid rgba(255, 255, 255, 0.6);
  background-color: rgba(255, 255, 255, 0.3);
  display: flex;
  justify-content: space-between;

  :hover {
    cursor: move;
  }
`

const WhileFlowHeaderElementInfo = styled.div`
  display: flex;
  align-items: center;
`

const WhileFlowHeaderElementName = styled.div`
  font-weight: bold;
`
