import {WhileFlow} from '../../../../store/model/Flow';
import {Position} from '../../../../model/Position';
import {FlowElementContainer} from '../FlowElementContainer';
import React from 'react';
import styled from 'styled-components';
import {observer} from 'mobx-react-lite';
import {WhileFlowHeader} from './WhileFlowHeader';
import {Movable} from '../../../shared/Movable';
import {FlowSocketElement} from '../socket/FlowSocketElement';
import {ArgumentSocketElement} from '../socket/ArgumentSocketElement';

interface WhileFlowProps {
    flow: WhileFlow;
    viewport: Position;
    zoom: number;
}

export const WhileFlowElement = observer(({flow, viewport, zoom}: WhileFlowProps) => {
    return (
        <FlowElementContainer position={{...flow.position}} viewport={{...viewport}} zoom={zoom}>
            <Movable position={flow.position}>
                <WhileFlowHeader flow={flow}/>
            </Movable>
            <WhileFlowContainer>
                <InputSockets>
                    <FlowSocketElement socket={flow.inputFlowSocket}/>
                    <ArgumentSocketElement socket={flow.conditionSocket}/>
                </InputSockets>
                <OutputSockets>
                    <OutputFlowSocket>
                        <OutputFlowSocketLabel>After</OutputFlowSocketLabel>
                        <FlowSocketElement socket={flow.outputFlowSocket}/>
                    </OutputFlowSocket>
                    <OutputFlowSocket>
                        <OutputFlowSocketLabel>Iteration</OutputFlowSocketLabel>
                        <FlowSocketElement socket={flow.iterationFlowSocket}/>
                    </OutputFlowSocket>
                </OutputSockets>
            </WhileFlowContainer>
        </FlowElementContainer>
    )
})

const WhileFlowContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  padding: ${({theme}) => theme.spacing.small};
  border-radius: 5px;
  width: 11rem;
`

const InputSockets = styled.div`
    
`

const OutputSockets = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-end;
`

const OutputFlowSocket = styled.div`
  display: flex;  
`

const OutputFlowSocketLabel = styled.span`
  margin-right: ${({theme}) => theme.spacing.normal};
  color: ${({theme}) => theme.color.ternaryFont};
`