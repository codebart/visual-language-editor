import React, {useCallback, useState} from 'react';
import styled from 'styled-components';
import usePortal from '../../../hooks/usePortal';
import {add, Position} from '../../../model/Position';
import {createPortal} from 'react-dom';
import {useVisibility, VisibilityControl} from '../../../hooks/useVisibility';
import {useShortcut} from '../../../hooks/useShortcut';
import {MenuModal} from '../../shared/MenuModal';
import {ChildEntryType, ClassEntry, FlowEntryType} from '../../../store/model/StructureEntry';
import StructureType from '../../../store/model/StructureType';
import {fieldFlow, Flow, functionFlow, ifFlow, literalFlow, literalFlowValue, methodFlow, returnFlow, setFlow, thisFlow, variableFlow, whileFlow} from '../../../store/model/Flow';
import {zIndex} from '../../../theme/zIndex';
import {useAppStore} from '../../../store/AppStore';
import {Library} from '@styled-icons/fluentui-system-filled/Library';
import {typeName} from '../../../lang/Type';
import {isStd} from '../../../lang/std/Std';
import {StdInteger} from '../../../lang/std/classes/Integer';
import {StdFloat} from '../../../lang/std/classes/Float';
import {observer} from 'mobx-react-lite';
import {StdBoolean} from '../../../lang/std/classes/Boolean';
import {StdString} from '../../../lang/std/classes/String';
import {AddReturnTypeModal} from '../../right/properties/AddReturnTypeModal';
import {StdArray} from '../../../lang/std/classes/Array';

interface FlowContextMenuProps {
    entry: FlowEntryType;
    position: Position;
    relativePosition: Position;
    viewport: Position;
    visibility: VisibilityControl;
    onFlowAdded(flow: Flow): void;
}

export const FlowContextMenu = observer(({entry, position, relativePosition, viewport, visibility, onFlowAdded}: FlowContextMenuProps) => {
    const portal = usePortal('flowContextMenu', visibility.visible);
    const store = useAppStore();
    const addTypeModal = useVisibility(false);
    const [modalContext, setModalContext] = useState<Function>(() => () => {});
    useShortcut({key: 'escape'}, visibility.hide);
    const addFlow = (flow: Flow) => {
        visibility.hide();
        onFlowAdded(flow);
    }
    const showModal = useCallback((modalContext: Function) => {
        setModalContext(() => modalContext);
        addTypeModal.show();
    }, [addTypeModal]);
    if (!visibility.visible) {
        return null;
    }
    const flowPosition = add({x: -viewport.x, y: -viewport.y}, relativePosition);
    const portalElement = createPortal((
        <FlowContextMenuWrapper onClick={visibility.hide}>
            <FlowContextMenuContainer position={position}>
                <MenuModal sections={[{
                    items: [
                        {text: 'Function', children: [
                                {items: store.functions.map(func => ({
                                        text: func.type.name,
                                        icon: isStd(func.type) ? Library : undefined,
                                        onClick: () => addFlow(functionFlow(flowPosition, func))
                                    }))}
                            ]},
                        {text: 'Method', children: [
                                {items: store.classes.map(cls => ({
                                        text: typeName(cls.type),
                                        icon: isStd(cls.type) ? Library : undefined,
                                        children: [
                                            {items: cls.methods.map(method => ({
                                                    text: `${method.name}(${method.parameters.map(parameter => parameter.type.name).join(', ')})`,
                                                    icon: isStd(cls.type) ? Library : undefined,
                                                    onClick: () => addFlow(methodFlow(flowPosition, cls, method))
                                                }))}
                                        ]
                                    }))}
                            ]},
                        {text: 'Constructor', children: [
                                {items: store.classes.map(cls => ({
                                        text: typeName(cls.type),
                                        icon: isStd(cls.type) ? Library : undefined,
                                        children: [
                                            {items: cls.methods.map(method => ({
                                                    text: `${method.name}(${method.parameters.map(parameter => parameter.type.name).join(', ')})`,
                                                    icon: isStd(cls.type) ? Library : undefined,
                                                    onClick: () => addFlow(methodFlow(flowPosition, cls, method))
                                                }))}
                                        ]
                                    }))}
                            ]},
                        {text: 'Logic', children: [
                                {items: [
                                        {text: 'If', icon: Library, onClick: () => addFlow(ifFlow(flowPosition))},
                                        {text: 'While', icon: Library, onClick: () => addFlow(whileFlow(flowPosition))},
                                        {text: 'Return', icon: Library, enabled: () => entry.type !== StructureType.CONSTRUCTOR, onClick: () => {
                                                if (entry.type === StructureType.METHOD) {
                                                    addFlow(returnFlow(flowPosition, entry.method.return));
                                                } else if (entry.type === StructureType.FUNCTION) {
                                                    addFlow(returnFlow(flowPosition, entry.function.return));
                                                }
                                            }}
                                    ]}
                            ]},
                        {text: 'Literal', children: [
                                {items: [
                                        {text: 'Array', icon: Library, onClick: () => addFlow(literalFlow(flowPosition, StdArray.type))},
                                        {text: 'String', icon: Library, onClick: () => addFlow(literalFlow(flowPosition, StdString.type))},
                                        {text: 'Integer', icon: Library, onClick: () => addFlow(literalFlow(flowPosition, StdInteger.type))},
                                        {text: 'Float', icon: Library, onClick: () => addFlow(literalFlow(flowPosition, StdFloat.type))},
                                        {text: 'Boolean', icon: Library, children: [
                                                {items: [
                                                        {text: 'True', icon: Library, onClick: () => addFlow(literalFlowValue(flowPosition, StdBoolean.type, 'true'))},
                                                        {text: 'False', icon: Library, onClick: () => addFlow(literalFlowValue(flowPosition, StdBoolean.type, 'false'))}
                                                    ]}
                                            ]},
                                    ]}
                            ]},
                        {text: 'Field', enabled: () => (entry.type === StructureType.METHOD || entry.type === StructureType.CONSTRUCTOR) && ((entry as ChildEntryType).parent as ClassEntry).class.fields.length > 0, children: [
                                {items: (entry.type === StructureType.METHOD || entry.type === StructureType.CONSTRUCTOR) ? ((entry as ChildEntryType).parent as ClassEntry).class.fields.map(field => ({
                                        text: `${field.type.name} ${field.name}`,
                                        onClick: () => addFlow(fieldFlow(flowPosition, field))
                                    })) : []}
                            ]},
                        {text: 'Variable', children: [
                                {items: [
                                        {
                                            text: `Variable`,
                                            icon: Library,
                                            onClick: () => showModal(variableFlow)
                                        },
                                        {
                                            text: `Set`,
                                            icon: Library,
                                            onClick: () => showModal(setFlow)
                                        }
                                    ]}
                            ]},
                        {text: 'This', icon: Library, enabled: () => entry.type === StructureType.METHOD || entry.type === StructureType.CONSTRUCTOR, onClick: () => addFlow(thisFlow(flowPosition, (entry.parent as ClassEntry).class.type))},
                    ]
                }]}/>
            </FlowContextMenuContainer>
        </FlowContextMenuWrapper>
    ), portal);
    return (
        <>
            <AddReturnTypeModal returnTypeAdded={type => addFlow(modalContext(flowPosition, type))} visibility={addTypeModal}/>
            {portalElement}
        </>
    )
})

const FlowContextMenuWrapper = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  z-index: ${zIndex.modal};
`

interface FlowContextMenuContainerProps {
    position: Position;
}

const FlowContextMenuContainer = styled.div<FlowContextMenuContainerProps>`
  position: absolute;
  left: ${({position}) => position.x}px;
  top: calc(${({position}) => position.y}px - 2rem);
`