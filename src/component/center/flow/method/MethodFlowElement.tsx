import React from 'react';
import styled from 'styled-components';
import {MethodFlow} from '../../../../store/model/Flow';
import {MethodFlowHeader} from './MethodFlowHeader';
import {FlowElementContainer} from '../FlowElementContainer';
import {Position} from '../../../../model/Position';
import {ArgumentSocketElement} from '../socket/ArgumentSocketElement';
import {observer} from 'mobx-react-lite';
import {Movable} from '../../../shared/Movable';
import {FlowSocketElement} from '../socket/FlowSocketElement';

interface MethodFlowProps {
    flow: MethodFlow;
    viewport: Position;
    zoom: number;
}

export const MethodFlowElement = observer(({flow, viewport, zoom}: MethodFlowProps) => {
    return (
        <FlowElementContainer position={{...flow.position}} viewport={{...viewport}} zoom={zoom}>
            <Movable position={flow.position}>
                <MethodFlowHeader flow={flow}/>
            </Movable>
            <MethodFlowContainer>
                <InputSockets>
                    <FlowSocketElement socket={flow.inputFlowSocket}/>
                    {flow.inputSockets.map(socket => <ArgumentSocketElement key={socket.name} socket={socket}/>)}
                </InputSockets>
                <OutputSockets>
                    <FlowSocketElement socket={flow.outputFlowSocket}/>
                    {flow.outputSocket && (
                        <ArgumentSocketElement socket={flow.outputSocket}/>
                    )}
                </OutputSockets>
            </MethodFlowContainer>
        </FlowElementContainer>
    )
})

const MethodFlowContainer = styled.div`
  display: flex;
  justify-content: space-between;
  min-width: 10rem;
`

const InputSockets = styled.div`
  display: flex;
  flex-direction: column;
  padding: ${({theme}) => theme.spacing.small};
`

const OutputSockets = styled.div`
  display: flex;
  flex-direction: column;
  padding: ${({theme}) => theme.spacing.small};
  align-items: flex-end;
`