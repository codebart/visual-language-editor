import React from 'react';
import styled from 'styled-components';
import {Functions} from '@styled-icons/material/Functions';
import CloseIcon from '../../../shared/CloseIcon';
import {ArgumentSocketElement} from '../socket/ArgumentSocketElement';
import {MethodFlow} from '../../../../store/model/Flow';
import {typeName} from '../../../../lang/Type';
import {observer} from 'mobx-react-lite';
import {useFlow} from '../../../../hooks/useFlow';

interface MethodHeaderProps {
    flow: MethodFlow;
}

export const MethodFlowHeader = observer(({flow}: MethodHeaderProps) => {
    const {removeFlow} = useFlow(flow);
    return (
        <MethodFlowHeaderContainer>
            <MethodFlowHeaderElementInfo>
                <ArgumentSocketElement socket={flow.objectSocket}/>
                <Functions/>
                <MethodFlowHeaderElementName>
                    <MethodFlowElementHeaderTitle>
                        {flow.method.name}
                    </MethodFlowElementHeaderTitle>
                    {typeName(flow.interface.type)}
                </MethodFlowHeaderElementName>
            </MethodFlowHeaderElementInfo>
            <MethodFlowHeaderClose>
                <CloseIcon onClick={removeFlow} title={'Remove'}/>
            </MethodFlowHeaderClose>
        </MethodFlowHeaderContainer>
    )
})

const MethodFlowHeaderContainer = styled.div`
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  padding: ${({theme}) => theme.spacing.small};
  border: 1px solid rgba(255, 136, 0, 0.5);
  background-color: rgba(255, 136, 0, 0.5);
  display: flex;
  justify-content: space-between;

  :hover {
    cursor: move;
  }
`

const MethodFlowElementHeaderTitle = styled.div`
  font-weight: bold;

  :hover {
    cursor: pointer;
    text-decoration: underline;
  }
`

const MethodFlowHeaderElementInfo = styled.div`
  display: flex;
  align-items: center;
`

const MethodFlowHeaderClose = styled.div`
  display: flex;
  align-items: center;
`

const MethodFlowHeaderElementName = styled.div`
`