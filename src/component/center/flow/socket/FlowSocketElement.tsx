import styled from 'styled-components';
import {detachSocket, FlowSocket} from '../../../../store/model/Flow';
import {useSocket} from '../../../../hooks/useSocket';
import {MenuModal, MenuSectionDefinition} from '../../../shared/MenuModal';
import React from 'react';
import {Cut} from '@styled-icons/boxicons-regular/Cut';
import {observer} from 'mobx-react-lite';
import {wrap} from '../../../../util/wrap';

interface LogicSocketProps {
    socket: FlowSocket;
}

export const FlowSocketElement = observer(({socket}: LogicSocketProps) => {
    const connected: boolean = socket.connectedSockets.length > 0;
    const {containerRef, onMouseDown, onSocketDrop} = useSocket(socket);
    const menuSections: MenuSectionDefinition[] = [
        {items: [{text: 'Detach', icon: Cut, onClick: wrap(detachSocket)(socket)}]}
    ];
    return (
        <LogicSocketContainer>
            <LogicSocketIcon title={'Flow'} ref={containerRef} onMouseDown={onMouseDown} onMouseUp={onSocketDrop} connected={connected}>
                {connected && (
                    <DetachMenuContainer>
                        <MenuModal sections={menuSections}/>
                    </DetachMenuContainer>
                )}
            </LogicSocketIcon>
        </LogicSocketContainer>
    )
});

const DetachMenuContainer = styled.div`
  position: absolute;
  top: -1.1rem;
  left: -1.1rem;
  transform: rotate(-45deg);
`

const LogicSocketContainer = styled.div`
  display: flex;
  align-items: center;
`

interface LogicSocketIconProps {
    connected: boolean;
}

const LogicSocketIcon = styled.div<LogicSocketIconProps>`
  border: 3px solid white;
  width: 6px;
  height: 6px;
  transform: rotate(45deg);
  margin: ${({theme}) => theme.spacing.tiny};
  margin-top: ${({theme}) => theme.spacing.small};
  margin-bottom: ${({theme}) => theme.spacing.small};
  background-color: ${({connected}) => connected ? 'white' : 'transparent'};
  position: relative;

  > ${DetachMenuContainer} {
    opacity: 0;
    visibility: hidden;
    transition: all;
  }

  :hover > ${DetachMenuContainer} {
    opacity: 1;
    visibility: visible;
    transition: all;
    transition-delay: 0.5s;
  }

  :hover {
    cursor: pointer;
  }
`