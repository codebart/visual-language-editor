import React from 'react';
import styled, {DefaultTheme} from 'styled-components';
import {typeColor} from '../typeColor';
import {Type, typeName, typesEqual} from '../../../../lang/Type';
import colorShade from '../../../../util/colorShade';
import {MenuModal, MenuSectionDefinition} from '../../../shared/MenuModal';
import {Cut} from '@styled-icons/boxicons-regular/Cut';
import {useSocket} from '../../../../hooks/useSocket';
import {observer} from 'mobx-react-lite';
import {ArgumentSocket, detachSocket, SocketDirection} from '../../../../store/model/Flow';
import {wrap} from '../../../../util/wrap';
import {AnyType} from '../../../../lang/std/StdTypes';

interface ArgumentSocketProps {
    socket: ArgumentSocket;
}

export const ArgumentSocketElement = observer(({socket}: ArgumentSocketProps) => {
    const {containerRef, onMouseDown, onSocketDrop} = useSocket(socket);
    const connected: boolean = socket.connectedSockets.length > 0;
    const menuSections: MenuSectionDefinition[] = [
        {items: [{text: 'Detach', icon: Cut, onClick: wrap(detachSocket)(socket)}]}
    ];
    return (
        <ArgumentElementSocketContainer direction={socket.direction}>
            {socket.name.length > 0 && (
                <ArgumentElementSocketName>
                    {socket.name}
                </ArgumentElementSocketName>
            )}
            <ArgumentElementSocket socket={socket} onMouseUp={onSocketDrop} onMouseDown={onMouseDown} ref={containerRef} title={typeName(socket.argumentType)} objectType={socket.argumentType} connected={connected} direction={socket.direction}>
                {connected && (
                    <DetachMenuContainer>
                        <MenuModal sections={menuSections}/>
                    </DetachMenuContainer>
                )}
            </ArgumentElementSocket>
        </ArgumentElementSocketContainer>
    )
})

const DetachMenuContainer = styled.div`
  position: absolute;
  top: -1.4rem;
`

const flowDirection = (direction: SocketDirection): string => {
    switch (direction) {
        case SocketDirection.INPUT:
            return 'row-reverse';
        case SocketDirection.OUTPUT:
            return 'row'
    }
}

interface ArgumentElementSocketContainerProps {
    direction: SocketDirection;
}

const ArgumentElementSocketContainer = styled.div<ArgumentElementSocketContainerProps>`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  flex-direction: ${({direction}) => flowDirection(direction)};
  color: ${({theme}) => theme.color.ternaryFont};
`

const ArgumentElementSocketName = styled.span`
  margin-right: ${({theme}) => theme.spacing.small};
  margin-left: ${({theme}) => theme.spacing.small};
`

const socketColor = (socket: ArgumentSocket, theme: DefaultTheme): string => {
    if (typesEqual(socket.argumentType, AnyType) && socket.connectedSockets.length) {
        return typeColor(socket.connectedSockets[0].argumentType, theme);
    }
    return typeColor(socket.argumentType, theme);
}

const connectedMarker = ({socket, connected, direction, theme}: ArgumentElementSocketProps): string => {
    switch (direction) {
        case SocketDirection.INPUT:
            return `
              ::before {
                content: '';
                border-radius: 3px;
                position: absolute;
                margin-top: 3px;
                margin-left: -5px;
                width: 5px;
                height: 4px;
                background-color: ${connected ? socketColor(socket, theme as DefaultTheme) : 'transparent'};
              }
            `;
        case SocketDirection.OUTPUT:
            return `
              ::after {
                content: '';
                border-radius: 3px;
                position: absolute;
                margin-top: 3px;
                margin-left: 10px;
                width: 5px;
                height: 4px;
                background-color: ${connected ? socketColor(socket, theme as DefaultTheme) : 'transparent'};
              }
            `;
    }
}

interface ArgumentElementSocketProps {
    socket: ArgumentSocket;
    objectType: Type;
    connected: boolean;
    direction: SocketDirection;
    theme?: DefaultTheme;
}

const ArgumentElementSocket = styled.div<ArgumentElementSocketProps>`
  border-radius: 50%;
  border: 3px solid ${({theme, socket}) => socketColor(socket, theme)};
  width: 10px;
  height: 10px;
  background-color: ${({theme, objectType, connected}) => connected ? colorShade(typeColor(objectType, theme), -50) : 'transparent'};
  
  ${(props: ArgumentElementSocketProps) => props.connected && connectedMarker(props)};

  > ${DetachMenuContainer} {
    opacity: 0;
    visibility: hidden;
    transition: all;
  }

  :hover > ${DetachMenuContainer} {
    opacity: 1;
    visibility: visible;
    transition: all;
    transition-delay: 0.5s;
  }

  position: relative;

  :hover {
    cursor: pointer;
  }
`
