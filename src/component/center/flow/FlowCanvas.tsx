import React, {useCallback} from 'react';
import styled from 'styled-components';
import {Flow, FlowType} from '../../../store/model/Flow';
import {Position, sub} from '../../../model/Position';
import {ParametersFlowElement} from './parameters/ParametersFlowElement';
import {observer} from 'mobx-react-lite';
import {FlowEntryType} from '../../../store/model/StructureEntry';
import useCanvas from '../../../hooks/useCanvas';
import {zIndex} from '../../../theme/zIndex';
import {FlowContextMenu} from './FlowContextMenu';
import {useContextMenu} from '../../../hooks/useContextMenu';
import {LiteralFlowElement} from './literal/LiteralFlowElement';
import {ReturnFlowElement} from './return/ReturnFlowElement';
import {FieldFlowElement} from './field/FieldFlowElement';
import {IfFlowElement} from './if/IfFlowElement';
import {FunctionFlowElement} from './function/FunctionFlowElement';
import {MethodFlowElement} from './method/MethodFlowElement';
import {WhileFlowElement} from './while/WhileFlowElement';
import {SetFlowElement} from './set/SetFlowElement';
import {ThisFlowElement} from './this/LiteralFlowElement';
import {ConstructorFlowElement} from './constructor/ConstructorFlowElement';
import {VariableFlowElement} from './variable/VariableFlowElement';

interface FlowElementProps {
    flow: Flow;
    viewport: Position;
    zoom: number;
}

const FlowElement = ({flow, viewport, zoom}: FlowElementProps) => {
    switch (flow.flowType) {
        case FlowType.PARAMETERS:
            return <ParametersFlowElement flow={flow} viewport={viewport} zoom={zoom}/>;
        case FlowType.LITERAL:
            return <LiteralFlowElement flow={flow} viewport={viewport} zoom={zoom}/>
        case FlowType.RETURN:
            return <ReturnFlowElement flow={flow} viewport={viewport} zoom={zoom}/>
        case FlowType.FIELD:
            return <FieldFlowElement flow={flow} viewport={viewport} zoom={zoom}/>
        case FlowType.IF:
            return <IfFlowElement flow={flow} viewport={viewport} zoom={zoom}/>
        case FlowType.FUNCTION:
            return <FunctionFlowElement flow={flow} viewport={viewport} zoom={zoom}/>
        case FlowType.METHOD:
            return <MethodFlowElement flow={flow} viewport={viewport} zoom={zoom}/>
        case FlowType.WHILE:
            return <WhileFlowElement flow={flow} viewport={viewport} zoom={zoom}/>
        case FlowType.SET:
            return <SetFlowElement flow={flow} viewport={viewport} zoom={zoom}/>
        case FlowType.THIS:
            return <ThisFlowElement flow={flow} viewport={viewport} zoom={zoom}/>
        case FlowType.CONSTRUCTOR:
            return <ConstructorFlowElement flow={flow} viewport={viewport} zoom={zoom}/>
        case FlowType.VARIABLE:
            return <VariableFlowElement flow={flow} viewport={viewport} zoom={zoom}/>
    }
    return null;
}

interface FlowCanvasProps {
    entry: FlowEntryType;
}

export const FlowCanvas = observer(({entry}: FlowCanvasProps) => {
    const viewport = entry.flow.viewport;
    const {zoom, containerRef, backgroundRef, size, onScroll, onMouseDown} = useCanvas(viewport, entry);
    const {onContextMenu, position, visibility} = useContextMenu();
    const onFlowAdded = useCallback((flow: Flow) => {
        entry.flow.flows.push(flow);
    }, [entry.flow.flows]);
    const relativePosition = useCallback(() => {
        if (containerRef.current) {
            return sub(position, {
                x: containerRef.current?.offsetLeft,
                y: containerRef.current?.offsetTop
            });
        }
        return position;
    }, [containerRef, position]);
    return (
        <FlowCanvasContainer onContextMenu={onContextMenu} onMouseDown={onMouseDown} onWheel={onScroll} ref={containerRef}>
            <FlowCanvasBackground width={size.width} height={size.height} ref={backgroundRef}/>
            <FlowElements width={size.width} height={size.height}>
                {entry.flow?.flows.map((flow, index) => <FlowElement key={index} flow={flow} viewport={viewport} zoom={zoom}/>)}
            </FlowElements>
            <Zoom>
                Zoom: {Math.ceil(zoom * 100)}%
            </Zoom>
            <FlowContextMenu entry={entry} position={position} relativePosition={relativePosition()} viewport={viewport} visibility={visibility} onFlowAdded={onFlowAdded}/>
        </FlowCanvasContainer>
    )
});

const Zoom = styled.div`
  position: relative;
  font-size: ${({theme}) => theme.font.small};
  text-align: right;
  margin-right: ${({theme}) => theme.spacing.small};
`

interface FlowElementsProps {
    width: number;
    height: number;
}

const FlowElements = styled.div<FlowElementsProps>`
  overflow: hidden;
  position: absolute;
  width: ${({width}) => width}px;
  height: ${({height}) => height}px;
`

const FlowCanvasBackground = styled.canvas`
  position: absolute;
  z-index: ${zIndex.behind};
`

const FlowCanvasContainer = styled.div`
  width: 100%;
  height: 100%;
  overflow: hidden;
`

