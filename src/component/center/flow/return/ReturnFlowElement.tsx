import React from 'react';
import styled, {DefaultTheme} from 'styled-components';
import {ReturnFlow} from '../../../../store/model/Flow';
import {FlowElementContainer} from '../FlowElementContainer';
import {Position} from '../../../../model/Position';
import {ArgumentSocketElement} from '../socket/ArgumentSocketElement';
import {observer} from 'mobx-react-lite';
import {Type} from '../../../../lang/Type';
import {typeColor} from '../typeColor';
import {Movable} from '../../../shared/Movable';
import {FlowSocketElement} from '../socket/FlowSocketElement';
import CloseIcon from '../../../shared/CloseIcon';
import {useFlow} from '../../../../hooks/useFlow';

interface ReturnFlowProps {
    flow: ReturnFlow;
    viewport: Position;
    zoom: number;
}

export const ReturnFlowElement = observer(({flow, viewport, zoom}: ReturnFlowProps) => {
    const {removeFlow} = useFlow(flow);
    return (
        <FlowElementContainer position={{...flow.position}} viewport={{...viewport}} zoom={zoom}>
            <ReturnFlowContainer type={flow.inputSocket?.argumentType}>
                <FlowElementSockets flow={flow}/>
                <Movable position={flow.position}>
                    <ReturnText>Return</ReturnText>
                </Movable>
                <CloseIcon onClick={removeFlow} title={'Remove'}/>
            </ReturnFlowContainer>
        </FlowElementContainer>
    )
})

interface FlowElementSocketsProps {
    flow: ReturnFlow;
}

const FlowElementSockets = observer(({flow}: FlowElementSocketsProps) => {
    return (
        <FlowElementSocketsContainer>
            <FlowElementSocketList>
                <FlowSocketElement socket={flow.inputFlowSocket}/>
                {flow.inputSocket && <ArgumentSocketElement socket={flow.inputSocket}/>}
            </FlowElementSocketList>
        </FlowElementSocketsContainer>
    )
})

const ReturnText = styled.div`
  font-weight: bold;
  margin-left: ${({theme}) => theme.spacing.normal};
`

interface ReturnFlowContainerProps {
    type?: Type;
    theme?: DefaultTheme;
}

const ReturnFlowContainer = styled.div<ReturnFlowContainerProps>`
  border: 1px solid ${({theme, type}) => type ? typeColor(type, theme) : 'gray'};
  border-radius: 5px;
  display: flex;
  align-items: center;
  justify-content: space-between;
`

const FlowElementSocketsContainer = styled.div`
  display: flex;
  padding: ${({theme}) => theme.spacing.small};
`

const FlowElementSocketList = styled.div`
  display: flex;
  flex-direction: column;
`