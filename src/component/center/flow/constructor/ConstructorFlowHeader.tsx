import React from 'react';
import styled from 'styled-components';
import {Construction} from '@styled-icons/material-outlined/Construction';
import CloseIcon from '../../../shared/CloseIcon';
import {ConstructorFlow} from '../../../../store/model/Flow';
import {typeName} from '../../../../lang/Type';
import {observer} from 'mobx-react-lite';
import {useFlow} from '../../../../hooks/useFlow';

interface ConstructorHeaderProps {
    flow: ConstructorFlow;
}

export const ConstructorFlowHeader = observer(({flow}: ConstructorHeaderProps) => {
    const {removeFlow} = useFlow(flow);
    return (
        <ConstructorFlowHeaderContainer>
            <ConstructorFlowHeaderElementInfo>
                <Construction/>
                <ConstructorFlowHeaderElementName>
                    <ConstructorFlowElementHeaderTitle>
                        Constructor
                    </ConstructorFlowElementHeaderTitle>
                    {typeName(flow.class.type)}
                </ConstructorFlowHeaderElementName>
            </ConstructorFlowHeaderElementInfo>
            <ConstructorFlowHeaderClose>
                <CloseIcon onClick={removeFlow} title={'Remove'}/>
            </ConstructorFlowHeaderClose>
        </ConstructorFlowHeaderContainer>
    )
})

const ConstructorFlowHeaderContainer = styled.div`
  border-top-left-radius: 5px;
  border-top-right-radius: 5px;
  padding: ${({theme}) => theme.spacing.small};
  border: 1px solid rgba(255, 136, 0, 0.5);
  background-color: rgba(255, 136, 0, 0.5);
  display: flex;
  justify-content: space-between;

  :hover {
    cursor: move;
  }
`

const ConstructorFlowElementHeaderTitle = styled.div`
  font-weight: bold;

  :hover {
    cursor: pointer;
    text-decoration: underline;
  }
`

const ConstructorFlowHeaderElementInfo = styled.div`
  display: flex;
  align-items: center;
`

const ConstructorFlowHeaderClose = styled.div`
  display: flex;
  align-items: center;
`

const ConstructorFlowHeaderElementName = styled.div`
`