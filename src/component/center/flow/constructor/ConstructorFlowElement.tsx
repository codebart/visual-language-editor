import React from 'react';
import styled from 'styled-components';
import {ConstructorFlow} from '../../../../store/model/Flow';
import {ConstructorFlowHeader} from './ConstructorFlowHeader';
import {FlowElementContainer} from '../FlowElementContainer';
import {Position} from '../../../../model/Position';
import {ArgumentSocketElement} from '../socket/ArgumentSocketElement';
import {observer} from 'mobx-react-lite';
import {Movable} from '../../../shared/Movable';
import {FlowSocketElement} from '../socket/FlowSocketElement';

interface ConstructorFlowProps {
    flow: ConstructorFlow;
    viewport: Position;
    zoom: number;
}

export const ConstructorFlowElement = observer(({flow, viewport, zoom}: ConstructorFlowProps) => {
    return (
        <FlowElementContainer position={{...flow.position}} viewport={{...viewport}} zoom={zoom}>
            <Movable position={flow.position}>
                <ConstructorFlowHeader flow={flow}/>
            </Movable>
            <ConstructorFlowContainer>
                <InputSockets>
                    <FlowSocketElement socket={flow.inputFlowSocket}/>
                    {flow.inputSockets.map(socket => <ArgumentSocketElement key={socket.name} socket={socket}/>)}
                </InputSockets>
                <OutputSockets>
                    <FlowSocketElement socket={flow.outputFlowSocket}/>
                    <ArgumentSocketElement socket={flow.outputSocket}/>
                </OutputSockets>
            </ConstructorFlowContainer>
        </FlowElementContainer>
    )
})

const ConstructorFlowContainer = styled.div`
  display: flex;
  justify-content: space-between;
  min-width: 10rem;
`

const InputSockets = styled.div`
  display: flex;
  flex-direction: column;
  padding: ${({theme}) => theme.spacing.small};
`

const OutputSockets = styled.div`
  display: flex;
  flex-direction: column;
  padding: ${({theme}) => theme.spacing.small};
  align-items: flex-end;
`