import {ThisFlow} from '../../../../store/model/Flow';
import {Position} from '../../../../model/Position';
import {FlowElementContainer} from '../FlowElementContainer';
import React from 'react';
import {ArgumentSocketElement} from '../socket/ArgumentSocketElement';
import styled, {DefaultTheme} from 'styled-components';
import {typeColor} from '../typeColor';
import {Type} from '../../../../lang/Type';
import {Movable} from '../../../shared/Movable';
import {observer} from 'mobx-react-lite';
import CloseIcon from '../../../shared/CloseIcon';
import {useFlow} from '../../../../hooks/useFlow';

interface ThisFlowProps {
    flow: ThisFlow;
    viewport: Position;
    zoom: number;
}

export const ThisFlowElement = observer(({flow, viewport, zoom}: ThisFlowProps) => {
    const {removeFlow} = useFlow(flow);
    return (
        <FlowElementContainer position={{...flow.position}} viewport={{...viewport}} zoom={zoom}>
            <ThisFlowContainer type={flow.outputSocket.argumentType}>
                <Movable position={flow.position}>
                    <ThisIcon title={'This'} type={flow.outputSocket.argumentType}>
                        T
                    </ThisIcon>
                </Movable>
                <CloseIconContainer>
                    <CloseIcon onClick={removeFlow} title={'Remove'}/>
                </CloseIconContainer>
                <OutputSocket>
                    <ArgumentSocketElement socket={flow.outputSocket}/>
                </OutputSocket>
            </ThisFlowContainer>
        </FlowElementContainer>
    )
})

const CloseIconContainer = styled.div`
  display: flex;
  align-items: center;
`

interface ThisFlowStyleProps {
    type: Type;
    theme?: DefaultTheme;
}

const ThisFlowContainer = styled.div<ThisFlowStyleProps>`
  display: flex;
  align-items: center;
  padding-right: ${({theme}) => theme.spacing.small};
  padding-left: ${({theme}) => theme.spacing.small};
  border: 1px solid ${({theme, type}) => typeColor(type, theme)};
  border-radius: 5px;
  width: 6rem;
  height: 2rem;
`

const ThisIcon = styled.div<ThisFlowStyleProps>`
  background-color: ${({theme, type}) => typeColor(type, theme)};
  color: ${({theme}) => theme.color.backgroundDark};
  width: ${({theme}) => theme.font.normal};
  height: ${({theme}) => theme.font.normal};
  font-size: ${({theme}) => theme.font.small};
  border-radius: 25%;
  display: flex;
  align-items: center;
  font-weight: bold;
  justify-content: center;
  margin-right: ${({theme}) => theme.spacing.small};
`

const OutputSocket = styled.div`
  margin-left: ${({theme}) => theme.spacing.normal};
`