import {Position} from '../../../model/Position';
import styled from 'styled-components';

interface FlowElementContainerProps {
    zoom: number;
    viewport: Position;
    position: Position;
}

export const FlowElementContainer = styled.div.attrs(({zoom, viewport, position}: FlowElementContainerProps) => ({
    style: {
        left: ((viewport.x + position.x) * zoom) + 'px',
        top: ((viewport.y + position.y) * zoom) + 'px'
    }
}))<FlowElementContainerProps>`
  border-radius: 5px;
  box-shadow: 0 0 1px 1px rgba(0, 0, 0, 0.45);
  background-color: rgba(0, 0, 0, 0.45);
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  position: absolute;
  transform: scale(${({zoom}) => zoom});
`
