import React from 'react';
import styled, {DefaultTheme} from 'styled-components';
import CloseIcon from '../shared/CloseIcon';
import {FunctionalStructureEntryType} from '../../store/model/StructureEntry';
import {observer} from 'mobx-react-lite';
import {useTabs} from '../../hooks/useTabs';
import {wrap} from '../../util/wrap';
import {entryIcon} from '../shared/Icons';

const FileTabs = observer(() => {
    const {tabs} = useTabs();
    return (
        <FileTabsContainer>
            {tabs.map(tab => <FileTab key={tab.name} tab={tab}/>)}
        </FileTabsContainer>
    )
})

interface FileTabProps {
    tab: FunctionalStructureEntryType;
}

const FileTab = observer(({tab}: FileTabProps) => {
    const {closeTab, selectTab, isCurrent} = useTabs();
    return (
        <FileTabContainer selected={isCurrent(tab)}>
            <FileTabName onClick={wrap(selectTab)(tab)} >
                {entryIcon(tab.type)}
                {tab.name}
            </FileTabName>
            <CloseIcon onClick={wrap(closeTab)(tab)} title={'Close'}/>
        </FileTabContainer>
    )
})

const FileTabName = styled.div`
  display: flex;
  align-items: center;
`

const FileTabsContainer = styled.div`
  display: flex;
  width: 100%;
  background-color: ${({theme}) => theme.color.backgroundLight};
  border-bottom: 1px solid ${({theme}) => theme.color.borderDark};
  height: 2rem;
  flex-shrink: 0;
`

interface FileTabContainerProps {
    selected: boolean;
    theme?: DefaultTheme;
}

const FileTabContainer = styled.div<FileTabContainerProps>`
  display: flex;
  align-items: center;
  border-right: 1px solid ${({theme}) => theme.color.borderDark};
  padding-left: ${({theme}) => theme.spacing.normal};
  padding-right: ${({theme}) => theme.spacing.normal};
  border-bottom: 3px solid ${({theme, selected}) => selected ? theme.color.primaryLight : 'transparent'};
  background-color: ${({theme, selected}) => selected ? theme.color.backgroundDark : 'transparent'};
  color: ${({theme, selected}) => selected ? theme.color.ternaryFont : theme.color.primaryFont};
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  flex-shrink: 3;
  
  :hover {
    cursor: pointer;
    background-color: ${({theme}) => theme.color.backgroundDark};
  }
`

export default FileTabs;