import React from 'react';
import styled from 'styled-components';
import {Terminal} from '@styled-icons/boxicons-solid/Terminal';
import {Minus} from '@styled-icons/entypo/Minus';
import {useVisibility} from '../../hooks/useVisibility';

export const Console = () => {
    const {toggle, visible} = useVisibility(false)
    return (
        <ConsoleContainer>
            <ConsoleHeader>
                <ConsoleHeaderTitle>
                    <Terminal/>
                    Console
                </ConsoleHeaderTitle>
                <Minus onClick={toggle}/>
            </ConsoleHeader>
            {visible && (
                <ConsoleOutput>

                </ConsoleOutput>
            )}
        </ConsoleContainer>
    )
}

const ConsoleContainer = styled.div`
  width: 100%;
  background-color: ${({theme}) => theme.color.background};
  display: flex;
  flex-direction: column;
`

const ConsoleHeader = styled.div`
  padding: ${({theme}) => theme.spacing.tiny};
  border-top: 1px solid ${({theme}) => theme.color.borderDark};
  background-color: ${({theme}) => theme.color.backgroundLight};
  color: ${({theme}) => theme.color.ternaryFont};
  display: flex;
  align-items: center;
  justify-content: space-between;
`

const ConsoleOutput = styled.div`
  height: 20rem;
  border-top: 1px solid ${({theme}) => theme.color.borderLight};
`

const ConsoleHeaderTitle = styled.div`
  display: flex;
  align-items: center;
`