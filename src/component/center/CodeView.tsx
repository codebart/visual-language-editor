import React from 'react';
import styled from 'styled-components';
import {darcula} from 'react-syntax-highlighter/dist/esm/styles/prism';
import SyntaxHighlighter from 'react-syntax-highlighter/dist/cjs/prism';
import {codeGenerators} from '../../codegen/codeGenerators';
import {CodeGeneratingEntryType} from '../../store/model/StructureEntry';
import {observer} from 'mobx-react-lite';
import {CompilationTarget} from '../../codegen/CompilationTarget';

const language = (compilationTarget: CompilationTarget): string => {
    switch (compilationTarget) {
        case CompilationTarget.JAVA:
            return 'java';
        case CompilationTarget.PYTHON:
            return 'python';
        case CompilationTarget.JAVASCRIPT:
            return 'javascript';
    }
}

interface CodeViewProps {
    entry: CodeGeneratingEntryType;
    compilationTarget: CompilationTarget;
}

export const CodeView = observer(({entry, compilationTarget}: CodeViewProps) => {
    return (
        <ClassCodeContainer>
            <SyntaxHighlighter showLineNumbers language={language(compilationTarget)} style={darcula}>
                {codeGenerators[compilationTarget].generate(entry)}
            </SyntaxHighlighter>
        </ClassCodeContainer>
    )
})

const ClassCodeContainer = styled.div`
  height: 100%;
  overflow: auto;
  flex-shrink: 2.5;

  pre {
    padding: 0 !important;
    margin-top: 0 !important;
    margin-bottom: 0 !important;
    overflow: auto;

    code {
      .linenumber {
        background-color: ${({theme}) => theme.color.background};
        border-right: 1px solid ${({theme}) => theme.color.borderLight};
        margin-right: 1em;
        padding-right: 0;
      }
    }
  }
`