import React, {ReactChild} from 'react';
import styled from 'styled-components';
import {Position} from '../../model/Position';
import {useMove} from '../../hooks/useMove';

interface MovableProps {
    children: ReactChild;
    position: Position;
}

export const Movable = ({children, position}: MovableProps) => {
    const {onMouseDown} = useMove(position);
    return (
        <MovableContainer onMouseDown={onMouseDown}>
            {children}
        </MovableContainer>
    )
}

const MovableContainer = styled.div`
  :hover {
    cursor: move;
  }
`