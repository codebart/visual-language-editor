import React from 'react';
import styled, {DefaultTheme} from 'styled-components';

type ButtonType = 'primary' | 'secondary';

interface ButtonProps {
    disabled?: boolean;
    children?: any;
    buttonType?: ButtonType;
    onClick?(): void;
}


export const Button = ({children, disabled, onClick, buttonType}: ButtonProps) => {
    return (
        <ButtonContainer buttonType={buttonType} onClick={onClick}>
            {children}
        </ButtonContainer>
    )
}

interface ButtonContainerProps {
    buttonType?: ButtonType;
    theme?: DefaultTheme;
}

const ButtonContainer = styled.button`
  background-color: ${({theme, buttonType}: ButtonContainerProps) => buttonType !== 'primary' ? theme?.color.primary : theme?.color.secondary};
  border: 1px solid ${({theme, buttonType}: ButtonContainerProps) => buttonType !== 'primary' ? theme?.color.primaryLight : theme?.color.secondaryLight};
  border-radius: 5px;
  padding: ${({theme}) => theme.spacing.small};
  min-width: 5rem;
  color: ${({theme}) => theme.color.primaryFont};
  cursor: pointer;
  
  :hover {
    filter: brightness(115%);
  }
`