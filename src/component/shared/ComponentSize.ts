import {DefaultTheme} from 'styled-components';

export type ComponentSize = 'mini' | 'tiny' | 'small' | 'normal' | 'medium' | 'large' | 'big' | 'huge' | 'massive';
export const themeSize = (theme: DefaultTheme, defaultSize: ComponentSize, size?: ComponentSize, ): string => {
    return theme.size[size || defaultSize];
}