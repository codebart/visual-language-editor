import React, {useCallback, useState} from 'react';
import {Modal} from './Modal';
import {Input} from './Input';
import {VisibilityControl} from '../../hooks/useVisibility';

export type NameCallback = (name: string) => void;

export interface NameInputModalProps {
    name: string;
    callback: NameCallback;
    visibility: VisibilityControl;
}

export const NameInputModal = ({name, callback, visibility}: NameInputModalProps) => {
    const [entryName, setEntryName] = useState('');
    const changeName = ({currentTarget: {value}}: React.FormEvent<HTMLInputElement>) => setEntryName(value);
    const nameKeyDown = useCallback(({key}: React.KeyboardEvent<HTMLInputElement>) => {
        switch (key) {
            case 'Enter': {
                if (entryName.length > 0) {
                    callback(entryName);
                }
                setEntryName('');
                visibility.hide();
                break;
            }
            case 'escape': {
                setEntryName('');
                visibility.hide();
                break;
            }
        }
    }, [callback, entryName, visibility]);
    return (
        <Modal closeButton size={'large'} name={name} id={'nameModal'} visibility={visibility}>
            <Input value={entryName} onKeyDown={nameKeyDown} onChange={changeName} autoFocus expand placeholder={'Name...'}/>
        </Modal>
    )
}