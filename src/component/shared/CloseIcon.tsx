import styled from 'styled-components';
import {Close} from '@styled-icons/remix-fill/Close';

const CloseIcon = styled(Close)`
  margin-left: ${({theme}) => theme.spacing.small};
  margin-right: ${({theme}) => theme.spacing.small};
  
  :hover {
    color: ${({theme}) => theme.color.backgroundDark};
    background-color: ${({theme}) => theme.color.primaryFont};
    border-radius: 50%;
    cursor: pointer;
  }
`

export default CloseIcon;