import {PropertyType} from '../../store/model/PropertyType';
import styled from 'styled-components';
import React from 'react';
import StructureType from '../../store/model/StructureType';
import {FileEarmarkCodeFill} from '@styled-icons/bootstrap/FileEarmarkCodeFill';
import {Folder} from '@styled-icons/boxicons-solid/Folder';
import {Functions} from '@styled-icons/material/Functions';
import {StyledIcon} from '@styled-icons/styled-icon';
import {Build} from '@styled-icons/material/Build';

export const propertyIcon = (type: PropertyType) => {
    switch (type) {
        case PropertyType.INTERFACE:
            return <InterfaceIcon/>;
        case PropertyType.FIELD:
            return <FieldIcon/>;
        case PropertyType.CONSTRUCTOR:
            return <ConstructorIcon/>;
        case PropertyType.METHOD:
            return <MethodIcon/>;
        case PropertyType.PARAMETER:
            return <ParameterIcon/>;
        case PropertyType.TYPE:
            return <TypeIcon/>;
    }
}

export const entryIcon = (structureType: StructureType) => {
    switch (structureType) {
        case StructureType.INTERFACE:
            return <InterfaceIcon/>;
        case StructureType.DIRECTORY:
            return <Folder/>;
        case StructureType.CLASS:
            return <ClassIcon/>;
        case StructureType.METHOD:
            return <MethodIcon/>;
        case StructureType.FUNCTION:
            return <FunctionIcon/>;
        case StructureType.CONSTRUCTOR:
            return <ConstructorIcon/>;
    }
}

export const entryIconType = (structureType: StructureType): StyledIcon => {
    switch (structureType) {
        case StructureType.DIRECTORY:
            return Folder;
        case StructureType.CLASS:
        case StructureType.INTERFACE:
            return FileEarmarkCodeFill;
        case StructureType.METHOD:
        case StructureType.FUNCTION:
            return Functions;
        case StructureType.CONSTRUCTOR:
            return Build;
    }
}

const FieldIcon = () => (
    <FieldIconContainer>
        F
    </FieldIconContainer>
)

const ConstructorIcon = () => (
    <ConstructorIconContainer>
        C
    </ConstructorIconContainer>
)

const MethodIcon = () => (
    <MethodIconContainer>
        M
    </MethodIconContainer>
)

const ParameterIcon = () => (
    <ParameterIconContainer>
        P
    </ParameterIconContainer>
)

const TypeIcon = () => (
    <TypeIconContainer>
        T
    </TypeIconContainer>
)

const FunctionIcon = () => (
    <FunctionIconContainer>
        F
    </FunctionIconContainer>
)

const InterfaceIcon = () => (
    <InterfaceIconContainer>
        I
    </InterfaceIconContainer>
)

const ClassIcon = () => (
    <ClassIconContainer>
        C
    </ClassIconContainer>
)

const IconContainer = styled.div`
  color: ${({theme}) => theme.color.backgroundDark};
  width: ${({theme}) => theme.font.normal};
  height: ${({theme}) => theme.font.normal};
  font-size: ${({theme}) => theme.font.tiny};
  border-radius: 25%;
  display: flex;
  align-items: center;
  font-weight: bold;
  justify-content: center;
  margin-right: ${({theme}) => theme.spacing.small};
`

const FieldIconContainer = styled(IconContainer)`
  background-color: ${({theme}) => theme.color.purple};
`

const ConstructorIconContainer = styled(IconContainer)`
  background-color: ${({theme}) => theme.color.pink};
`

const MethodIconContainer = styled(IconContainer)`
  background-color: ${({theme}) => theme.color.orange};
`

const ParameterIconContainer = styled(IconContainer)`
  background-color: ${({theme}) => theme.color.purple};
`

const InterfaceIconContainer = styled(IconContainer)`
  background-color: ${({theme}) => theme.color.lightGreen};
`

const FunctionIconContainer = styled(IconContainer)`
  background-color: ${({theme}) => theme.color.darkRed};
`

const ClassIconContainer = styled(IconContainer)`
  background-color: ${({theme}) => theme.color.blue};
`

const TypeIconContainer = styled(IconContainer)`
  background-color: ${({theme}) => theme.color.lightBlue};
`