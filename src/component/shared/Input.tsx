import styled, {DefaultTheme} from 'styled-components';

interface InputProps {
    expand?: boolean;
    theme?: DefaultTheme;
}

export const Input = styled.input<InputProps>`
  background-color: transparent;
  border: none;
  color: ${({theme}) => theme.color.primaryFont};
  padding: ${({theme}) => theme?.spacing.normal};
  width: ${({theme, expand}: InputProps) => expand ? `calc(100% - 2 * ${theme?.spacing.normal})` : 'auto'};
  outline: none;
`