import React, {ReactChild, ReactChildren} from 'react';
import styled, {DefaultTheme} from 'styled-components';
import usePortal from '../../hooks/usePortal';
import {createPortal} from 'react-dom';
import {useShortcut} from '../../hooks/useShortcut';
import {zIndex} from '../../theme/zIndex';
import CloseIcon from './CloseIcon';
import {ComponentSize} from './ComponentSize';
import {VisibilityControl} from '../../hooks/useVisibility';

export interface ModalProps {
    id: string;
    name?: string;
    closeButton?: boolean;
    children: ReactChild | ReactChildren[] | any;
    size: ComponentSize;
    visibility: VisibilityControl;
}

export const Modal = ({id, name, children, visibility, closeButton, size}: ModalProps) => {
    const portal = usePortal(id, visibility.visible);
    useShortcut({key: 'escape'}, visibility.hide);
    if (!visibility.visible) {
        return null;
    }
    return createPortal((
        <ModalPositioningContainer>
            <ModalWindowContainer size={size}>
                {name && (
                    <ModalHeader>
                        {name}
                        {closeButton && <CloseIcon title={'Close'} onClick={visibility.hide}/>}
                    </ModalHeader>
                )}
                <ModalContainer>
                    {children}
                </ModalContainer>
            </ModalWindowContainer>
        </ModalPositioningContainer>
    ), portal);
}

interface ModalWindowContainerProps {
    size: ComponentSize;
    theme?: DefaultTheme;
}

const ModalWindowContainer = styled.div<ModalWindowContainerProps>`
  width: ${({theme, size}: ModalWindowContainerProps) => theme?.size[size]};
`

const ModalHeader = styled.div`
  padding: ${({theme}) => theme?.spacing.small};
  padding-left: ${({theme}) => theme?.spacing.normal};
  background-color: ${({theme}) => theme.color.backgroundLight};
  border-left: 1px solid ${({theme}) => theme.color.borderLight};
  border-top: 1px solid ${({theme}) => theme.color.borderLight};
  border-right: 1px solid ${({theme}) => theme.color.borderLight};
  display: flex;
  justify-content: space-between;
  align-items: center;
`

const ModalContainer = styled.div`
  background-color: ${({theme}) => theme.color.background};
  border: 1px solid ${({theme}) => theme.color.borderLight};
`

const ModalPositioningContainer = styled.div`
  position: absolute;
  left: 0;
  top: 0;
  width: 95%;
  height: 95%;
  display: flex;
  align-items: center;
  justify-items: center;
  justify-content: space-around;
  z-index: ${zIndex.modal};
  flex-direction: column;
  padding: 1rem;
`