import React, {createElement} from 'react';
import styled, {DefaultTheme} from 'styled-components';
import {StyledIcon} from '@styled-icons/styled-icon';
import {ChevronRight} from '@styled-icons/boxicons-solid/ChevronRight';
import {zIndex} from '../../theme/zIndex';

export interface MenuCategoryDefinition {
    text: string;
    sections: MenuSectionDefinition[];
}

export interface MenuSectionDefinition {
    items: MenuItemDefinition[];
    enabled?(): boolean;
}

export interface MenuItemDefinition {
    text: string;
    icon?: StyledIcon;
    children?: MenuSectionDefinition[];
    enabled?(): boolean;
    onClick?(): void;
}

interface MenuItemProps {
    category: MenuCategoryDefinition;
}

export const MenuCategory = ({category}: MenuItemProps) => {
    return (
        <MenuCategoryContainer>
            {category.text}
            {category.sections.length > 0 && <MenuModal sections={category.sections.filter(section => !section.enabled || section.enabled())}/>}
        </MenuCategoryContainer>
    )
}

interface MenuModalProps {
    sections: MenuSectionDefinition[];
}

export const MenuModal = ({sections}: MenuModalProps) => {
    return (
        <MenuModalContainer>
            {sections.map((section, index) => <MenuModalSection key={index} items={section.items}/>)}
        </MenuModalContainer>
    )
}

const MenuModalRight = ({sections}: MenuModalProps) => {
    return (
        <MenuModalRightContainer>
            {sections.map((section, index) => <MenuModalSection key={index} items={section.items}/>)}
        </MenuModalRightContainer>
    )
}

interface MenuModalSectionProps {
    items: MenuItemDefinition[];
}

const MenuModalSection = ({items}: MenuModalSectionProps) => {
    return (
        <MenuModalSectionContainer>
            {items.filter(item => !item.enabled || item.enabled()).map(item => <MenuModalItem key={item.text} item={item}/>)}
        </MenuModalSectionContainer>
    )
}

interface MenuModalItemProps {
    item: MenuItemDefinition;
}

const MenuModalItem = ({item: {children, onClick, icon, text}}: MenuModalItemProps) => {
    const onClickSuppress = (event: React.MouseEvent) => {
        event.stopPropagation();
        if (onClick) {
            onClick();
        }
    }
    return (
        <MenuModalItemContainer onClick={onClickSuppress} hasIcon={!!icon}>
            <MenuModalItemText>
                {icon && createElement(icon)}
                {text}
            </MenuModalItemText>
            {children && children.length > 0 && (
                <MenuModalItemChildren>
                    <ChevronRight/>
                    <MenuModalRight sections={children}/>
                </MenuModalItemChildren>
            )}
        </MenuModalItemContainer>
    )
}

const MenuModalItemText = styled.div`
  display: flex;
  align-items: center;
`

const MenuModalRightContainer = styled.div`
  background-color: ${({theme}) => theme.color.backgroundLight};
  border-left: 1px solid ${({theme}) => theme.color.borderLight};
  border-right: 1px solid ${({theme}) => theme.color.borderLight};
  border-top: 1px solid ${({theme}) => theme.color.borderLight};
  position: absolute;
  left: 1.5rem;
  min-width: 10rem;
  top: -0.5rem;
`

const MenuModalItemChildren = styled.div`
  display: flex;
  align-items: center;
  position: relative;
`

interface MenuModalItemContainerProps {
    hasIcon: boolean;
    theme?: DefaultTheme;
}

const MenuModalItemContainer = styled.div<MenuModalItemContainerProps>`
  display: flex;
  align-items: center;
  padding: ${({theme}) => theme.spacing.small};
  padding-left: ${({theme, hasIcon}) => hasIcon ? theme.spacing.small : theme.spacing.large};
  justify-content: space-between;

  :hover {
    cursor: pointer;
    background-color: ${({theme}) => theme.color.primaryLight};
  }

  ${MenuModalRightContainer} {
    display: none;
  }

  &:hover > ${MenuModalItemChildren} > ${MenuModalRightContainer} {
    display: block;
  }
`

const MenuModalSectionContainer = styled.div`
  border-bottom: 1px solid ${({theme}) => theme.color.borderLight};
`

const MenuModalContainer = styled.div`
  background-color: ${({theme}) => theme.color.backgroundLight};
  border-left: 1px solid ${({theme}) => theme.color.borderLight};
  border-right: 1px solid ${({theme}) => theme.color.borderLight};
  border-top: 1px solid ${({theme}) => theme.color.borderLight};
  position: absolute;
  left: 0;
  top: 2rem;
  min-width: 10rem;
  z-index: ${zIndex.modal};
`

const MenuCategoryContainer = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  padding-left: ${({theme}) => theme.spacing.normal};
  padding-right: ${({theme}) => theme.spacing.normal};
  position: relative;
  
  :hover {
    cursor: pointer;
    background-color: ${({theme}) => theme.color.primaryLight};
  }

  ${MenuModalContainer} {
    display: none;
  }
  
  &:hover ${MenuModalContainer} {
    display: block;
  }
`