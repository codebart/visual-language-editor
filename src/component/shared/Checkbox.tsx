import React, {useState} from 'react';
import {CheckboxChecked} from '@styled-icons/boxicons-regular/CheckboxChecked';
import {Checkbox as CheckboxIcon} from '@styled-icons/boxicons-regular/Checkbox';
import styled from 'styled-components';

interface CheckboxProps {
    initialValue?: boolean;
    onChange?(checked: boolean): void;
}

const Checkbox = ({initialValue, onChange}: CheckboxProps) => {
    const [checked, setChecked] = useState<boolean>(!!initialValue);
    const onClick = () => setChecked(!checked);
    return (
        <CheckboxContainer>
            {checked ? <CheckboxChecked onClick={onClick}/> : <CheckboxIcon onClick={onClick}/>}
        </CheckboxContainer>
    )
}

const CheckboxContainer = styled.div`
  width: 34px;
  
  svg {
    width: 100%;
    margin: 0;

    :hover {
      color: ${({theme}) => theme.color.ternaryFont};
      cursor: pointer;
    }
  }
`