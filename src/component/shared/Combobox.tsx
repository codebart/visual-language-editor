import React from 'react';
import styled, {DefaultTheme} from 'styled-components';
import {ComponentSize, themeSize} from './ComponentSize';

interface ComboboxProps {
    items: string[];
    defaultValue?: string;
    size?: ComponentSize;
    onChange(value: string): void;
}

export const Combobox = ({items, defaultValue, size, onChange}: ComboboxProps) => {
    const onComboboxChange = (event: React.ChangeEvent<HTMLSelectElement>) => onChange(event.currentTarget.value);
    return (
        <ComboboxContainer defaultValue={defaultValue ? defaultValue : items[0]} componentSize={size} onChange={onComboboxChange}>
            {items.map(item => <option key={item} value={item}>{item}</option>)}
        </ComboboxContainer>
    )
}

interface ComboboxContainerProps {
    componentSize?: ComponentSize;
    theme?: DefaultTheme;
}

const ComboboxContainer = styled.select<ComboboxContainerProps>`
  width: ${({theme, componentSize}: ComboboxContainerProps) => themeSize(theme as DefaultTheme, 'normal', componentSize)};
  background-color: transparent;
  border: 1px solid ${({theme}) => theme.color.borderLight};
  border-radius: 4px;
  margin-left: ${({theme}) => theme.spacing.normal};
  padding: ${({theme}) => theme.spacing.tiny};
  color: ${({theme}) => theme.color.primaryFont};
  
  option {
    color: ${({theme}) => theme.color.backgroundDark};
  }
`