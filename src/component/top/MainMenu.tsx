import React from 'react';
import styled from 'styled-components';
import logo from '../../logo.png';
import {Folder} from '@styled-icons/boxicons-solid/Folder'
import {Settings} from '@styled-icons/material/Settings';
import {Save} from '@styled-icons/material-rounded/Save';
import {useAppStore} from '../../store/AppStore';
import {observer} from 'mobx-react-lite';
import {MenuCategory, MenuCategoryDefinition} from '../shared/MenuModal';
import {useVisibility} from '../../hooks/useVisibility';
import {NameInputModal} from '../shared/NameInputModal';
import {DocumentBulletList} from '@styled-icons/fluentui-system-filled/DocumentBulletList';
import {HelpWithCircle} from '@styled-icons/entypo/HelpWithCircle';
import {useNavigate} from 'react-router';
import {wrap} from '../../util/wrap';

const MainMenu = observer(() => {
    const {project, projects, newProject, loadProject, projectOpen, closeProject, saveProject} = useAppStore();
    const navigate = useNavigate();
    const modalVisibility = useVisibility(false);
    const openProjectsMenuItems = projects.length < 1 ? [] : [
        {
            items: projects.map(project => ({
                text: project, onClick() {
                    loadProject(project)
                }
            }))
        }
    ];
    const menuCategories: MenuCategoryDefinition[] = [
        {
            text: 'Project',
            sections: [
                {
                    items: [
                        {text: 'New', onClick: modalVisibility.show},
                        {text: 'Open', icon: Folder, children: openProjectsMenuItems},
                        {text: 'Save', icon: Save, onClick: saveProject},
                    ]
                },
                {
                    items: [
                        {text: 'Settings', icon: Settings},
                    ]
                },
                {
                    items: [
                        {
                            text: 'Export',
                            children: [
                                {
                                    items: [
                                        {text: 'Project'},
                                        {text: 'Code'},
                                    ]
                                }
                            ]
                        },
                    ]
                },
                {
                    items: [
                        {
                            text: 'Close', onClick: closeProject
                        },
                    ],
                    enabled: () => projectOpen
                }
            ]
        },
        {text: 'Edit', sections: []},
        {text: 'Help', sections: [
                {
                    items: [
                        {text: 'Specification', icon: DocumentBulletList},
                        {text: 'Guide', icon: HelpWithCircle, onClick: wrap(navigate)('/docs')},
                    ]
                },
            ]},
    ]
    return (
        <MenuContainer>
            <Logo src={logo}/>
            {menuCategories.map(menuCategory => <MenuCategory key={menuCategory.text} category={menuCategory}/>)}
            {project && <ProjectName>{project.name}</ProjectName>}
            <NameInputModal name={'New project'} callback={newProject} visibility={modalVisibility}/>
        </MenuContainer>
    )
})

const Logo = styled.img`
  object-fit: none;
`

const ProjectName = styled.span`
  color: ${({theme}) => theme.color.secondaryFont};
  font-size: ${({theme}) => theme.font.small};
  display: flex;
  align-items: center;
  margin-left: ${({theme}) => theme.spacing.huge};
`

const MenuContainer = styled.nav`
  background-color: ${({theme}) => theme.color.backgroundLight};
  height: 2rem;
  border-bottom: 1px solid ${({theme}) => theme.color.borderLight};
  display: flex;
`

export default MainMenu;