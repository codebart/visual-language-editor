import React, { Fragment } from 'react';
import styled from 'styled-components';
import {ChevronCompactRight} from '@styled-icons/bootstrap/ChevronCompactRight';
import {useAppStore} from '../../store/AppStore';
import {observer} from 'mobx-react-lite';
import StructureType from '../../store/model/StructureType';
import {FunctionalStructureEntryType} from '../../store/model/StructureEntry';
import {entryIcon} from '../shared/Icons';

const LocationMenu = observer(() => {
    const store = useAppStore();
    const entries = [];
    const currentTab = store.currentProject.currentTab;
    let entry = currentTab?.parent;
    while (entry?.parent) {
        entries.push(entry);
        entry = entry.parent;
    }
    entries.reverse();
    const onClick = (entry: FunctionalStructureEntryType) => () => store.openTab(entry as FunctionalStructureEntryType);
    return (
        <LocationMenuContainer>
            <b>{store.currentProject.name}</b>
            {entries.map(entry => (
                <Fragment key={entry.name}>
                    <ChevronCompactRight/>
                    {entry.type !== StructureType.DIRECTORY ? (
                        <ParentFunctionalStructureContainer onClick={onClick(entry)}>
                            {entryIcon(entry.type)}
                            <span>{entry.name}</span>
                        </ParentFunctionalStructureContainer>
                    ) : entry.name}
                </Fragment>
            ))}
            {currentTab && (
                <>
                    <ChevronCompactRight/>
                    <FileContainer>
                        {entryIcon(currentTab.type)}
                        <span>{currentTab.name}</span>
                    </FileContainer>
                </>
            )}
        </LocationMenuContainer>
    )
});

const LocationMenuContainer = styled.nav`
  display: flex;
  align-items: center;
  padding-left: ${({theme}) => theme.spacing.normal};
`

const FileContainer = styled.div`
  color: ${({theme}) => theme.color.ternaryFont};
  display: flex;
  align-items: center;
`

const ParentFunctionalStructureContainer = styled(FileContainer)`
  :hover span {
    cursor: pointer;
    text-decoration: underline;
  }
`

export default LocationMenu;