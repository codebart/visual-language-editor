import React from 'react';
import styled from 'styled-components';
import {Combobox} from '../shared/Combobox';
import {TriangleRight} from '@styled-icons/entypo/TriangleRight';
import {useVisibility} from '../../hooks/useVisibility';
import {Run} from './Run';

export const Compilation = () => {
    const runVisibility = useVisibility();
    return (
        <CompilationContainer>
            Target language:
            <Combobox size={'tiny'} items={['Java', 'Python', 'Javascript']} onChange={() => {
            }}/>
            <RunIcon onClick={runVisibility.show} title={'Run'}/>
            <Run visibility={runVisibility}/>
        </CompilationContainer>
    )
}

const CompilationContainer = styled.div`
  display: flex;
  align-items: center;
  border-left: 1px solid ${({theme}) => theme.color.borderLight};
  padding-left: ${({theme}) => theme.spacing.normal};
  margin-right: ${({theme}) => theme.spacing.normal};
`

const RunIcon = styled(TriangleRight)`
  color: forestgreen;
  width: 1.5rem !important;
  margin: 0;
  
  :hover {
    cursor: pointer;
    color: lawngreen;
  }
`