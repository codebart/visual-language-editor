import React from 'react';
import styled from 'styled-components';
import {VisibilityControl} from '../../hooks/useVisibility';
import {Modal} from '../shared/Modal';
import {Combobox} from '../shared/Combobox';
import {Button} from '../shared/Button';
import {useAppStore} from '../../store/AppStore';
import {observer} from 'mobx-react-lite';
import {Func} from '../../lang/Function';
import {typeName} from '../../lang/Type';

const mainCandidates = (functions: Func[]): string[] =>
    functions.filter(func => !func.return && func.type.name === 'Main').map(func => typeName(func.type));

interface RunProps {
    visibility: VisibilityControl;
}

export const Run = observer(({visibility}: RunProps) => {
    const {functions} = useAppStore();
    return (
        <Modal name={'Run configuration'} id={'run'} visibility={visibility} size={'big'} closeButton>
            <RunContainer>
                <MainFunction>
                    Main function
                    <Combobox items={mainCandidates(functions)} onChange={() => {}}/>
                    <MainFunctionCandidatesDescription>
                        A main function is a function without return type named "Main"
                    </MainFunctionCandidatesDescription>
                </MainFunction>
                <hr/>
                <Actions>
                    <Button onClick={visibility.hide} buttonType={'primary'}>Cancel</Button>
                    <Button>Run</Button>
                </Actions>
            </RunContainer>
        </Modal>
    )
})

const MainFunctionCandidatesDescription = styled.div`
  margin-top: ${({theme}) => theme.spacing.normal};
  margin-bottom: ${({theme}) => theme.spacing.normal};
  font-size: ${({theme}) => theme.font.small};
`

const RunContainer = styled.div`
    padding: ${({theme}) => theme.spacing.normal};
`

const MainFunction = styled.div`
    
`

const Actions = styled.div`
  margin-top: ${({theme}) => theme.spacing.normal};
  display: flex;
  justify-content: flex-end;

  button:first-child {
    margin-right: 0.5rem;
  }
`