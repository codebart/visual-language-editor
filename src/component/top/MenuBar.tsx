import styled from 'styled-components';
import React, {ReactChild} from 'react';

interface MenuBarProps {
    children: ReactChild | ReactChild[];
}

export const MenuBar = ({children}: MenuBarProps) => {
    return (
        <LocationMenuContainer>
            {children}
        </LocationMenuContainer>
    )
};

const LocationMenuContainer = styled.nav`
  background-color: ${({theme}) => theme.color.backgroundLight};
  border-bottom: 1px solid ${({theme}) => theme.color.borderDark};
  height: 2rem;
  display: flex;
  align-items: center;
  justify-content: space-between;
`
