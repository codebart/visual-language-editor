import React from 'react';
import styled from 'styled-components';
import logo from '../../logo.png';
import {Link, Route, Routes, useLocation} from 'react-router-dom';
import {DocsFlow} from './DocsFlow';

interface DocumentationSection {
    name: string;
    references: DocumentationReference[];
}

interface DocumentationReference {
    name: string;
    url: string;
}

interface DocumentationRoute {
    url: string;
    component: React.ReactElement;
}

const documentationSections: DocumentationSection[] = [
    {
        name: 'About',
        references: [{name: 'Motivation', url: '/motivation'}, {name: 'Inspiration', url: '/inspiration'}]
    },
    {
        name: 'IDE',
        references: [
            {name: 'Menu', url: '/menu'},
            {name: 'Navigation', url: '/navigation'},
            {name: 'Properties', url: '/properties'},
            {name: 'Console', url: '/console'},
            {name: 'Structure', url: '/structure'},
        ]
    },
    {
        name: 'Terminology',
        references: [
            {name: 'Box', url: '/box'},
            {name: 'Flow', url: '/flow'},
            {name: 'Socket', url: '/socket'},
            {name: 'Connection', url: '/connection'},
        ]
    },
    {
        name: 'Basics',
        references: [
            {name: 'Structure', url: '/structure'},
            {name: 'Namespace', url: '/namespace'},
            {name: 'Class', url: '/class'},
            {name: 'Field', url: '/field'},
            {name: 'Method', url: '/method'},
            {name: 'Constructor', url: '/constructor'},
            {name: 'Function', url: '/function'},
            {name: 'Routine entry point', url: '/routine-entry-point'},
        ]
    }
]

export const Docs = () => {
    const routes: DocumentationRoute[] = [
        {url: '/flow', component: <DocsFlow/>}
    ]
    const location = useLocation();
    return (
        <DocsContainer>
            <ContentsTable>
                <Header>
                    <LogoContainer>
                        <Logo src={logo}/>
                    </LogoContainer>
                    <VisualLanguage>
                        Visual Language
                    </VisualLanguage>
                    Documentation
                </Header>
                <DocsSection>
                    {documentationSections.map((section: DocumentationSection) => (
                        <DocsSectionElement key={section.name}>
                            {section.name}
                            <DocsHeader>
                                {section.references.map((reference: DocumentationReference) => (
                                    <DocsReference match={location.pathname.includes(reference.url)} key={reference.name}>
                                        <Link to={'/docs' + reference.url}>{reference.name}</Link>
                                    </DocsReference>
                                ))}
                            </DocsHeader>
                        </DocsSectionElement>
                    ))}
                </DocsSection>
            </ContentsTable>
            <Guide>
                <Routes>
                    {routes.map(route => <Route key={route.url} path={route.url} element={route.component}/>)}
                </Routes>
            </Guide>
        </DocsContainer>
    )
}

const DocsContainer = styled.div`
  display: flex;
  height: 100%;
`

const LogoContainer = styled.div`
  margin-bottom: ${({theme}) => theme.spacing.normal};
  padding: 0.5rem;
  border-radius: 50%;
  border: 2px solid ${({theme}) => theme.color.primaryLight};
  background-color: ${({theme}) => theme.color.background};
  width: 1rem;
  height: 1rem;
`

const Logo = styled.img`
  object-fit: none;
`

const DocsSection = styled.ul`
  list-style-type: none;
  counter-reset: item;
  padding-left: ${({theme}) => theme.spacing.large};
`

const DocsSectionElement = styled.li`
  font-weight: bold;
  margin-bottom: ${({theme}) => theme.spacing.large};
`

const DocsHeader = styled.ul`
  list-style-type: none;
  padding-left: ${({theme}) => theme.spacing.normal};
  margin-top: ${({theme}) => theme.spacing.normal};
`

interface DocsReferenceProps {
  match: boolean;
}

const DocsReference = styled.li<DocsReferenceProps>`
  a {
    font-weight: normal;
    color: ${({match, theme}) => match ? theme.color.ternaryFont : theme.color.primaryFont};
    text-decoration: ${({match}) => match ? 'underline' : 'none'};

    :hover {
      text-decoration: underline;
      color: ${({match, theme}) => match ? theme.color.ternaryFont : theme.color.ternaryFont};
      cursor: pointer;
    }
    
    :visited {
      color: ${({match, theme}) => match ? theme.color.ternaryFont : theme.color.primaryFont};
    }
  }
`

const ContentsTable = styled.div`
  overflow-y: auto;
  background-color: ${({theme}) => theme.color.backgroundLight};
  height: 100%;
  width: 18rem;
  border-right: 1px solid ${({theme}) => theme.color.borderLight};
`

const VisualLanguage = styled.div`
  font-size: ${({theme}) => theme.font.large};
  color: ${({theme}) => theme.color.ternaryFont};
`

const Header = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: ${({theme}) => theme.spacing.large};
  padding-bottom: ${({theme}) => theme.spacing.normal};
  border-bottom: 1px solid ${({theme}) => theme.color.borderLight};
`

const Guide = styled.div`
  display: flex;
  flex-direction: column;
  width: calc(100% - 18rem);
  border-right: 1px solid ${({theme}) => theme.color.borderLight};
  background-color: ${({theme}) => theme.color.background};
  padding-top: ${({theme}) => theme.spacing.massive};
  padding-left: ${({theme}) => theme.spacing.massive};
  padding-right: ${({theme}) => theme.spacing.massive};
`