import React, {useState} from 'react';
import {CodeView} from '../center/CodeView';
import {CompilationTarget} from '../../codegen/CompilationTarget';
import {CodeGeneratingEntryType} from '../../store/model/StructureEntry';
import {Combobox} from '../shared/Combobox';
import styled from 'styled-components';

interface DocsCodeViewProps {
    entry: CodeGeneratingEntryType;
}

export const DocsCodeView = ({entry}: DocsCodeViewProps) => {
    const [compilationTarget, setCompilationTarget] = useState<CompilationTarget>(CompilationTarget.JAVA);
    return (
        <DocsCodeViewContainer>
            <LanguageSelector>
                Language: <Combobox items={['JAVA', 'PYTHON', 'JAVASCRIPT']} onChange={value => setCompilationTarget(value as CompilationTarget)}/>
            </LanguageSelector>
            <CodeView entry={entry} compilationTarget={compilationTarget}/>
        </DocsCodeViewContainer>
    )
}

const LanguageSelector = styled.div`
  padding: 0.5rem;
  margin-left: 46px;
  background-color: ${({theme}) => theme.color.backgroundDark};
  border-left: 1px solid ${({theme}) => theme.color.borderLight};
  border-top: 1px solid ${({theme}) => theme.color.borderLight};
  border-bottom: 1px solid ${({theme}) => theme.color.borderLight};
`

const DocsCodeViewContainer = styled.div`
  border-bottom: 1px solid ${({theme}) => theme.color.borderLight};
  border-right: 1px solid ${({theme}) => theme.color.borderLight};
`