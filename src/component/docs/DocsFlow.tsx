import React from 'react';
import {ClassEntry} from '../../store/model/StructureEntry';
import StructureType from '../../store/model/StructureType';
import {StdString} from '../../lang/std/classes/String';
import {DocsCodeView} from './DocsCodeView';
import {FlowCanvas} from '../center/flow/FlowCanvas';
import {flowImplementation} from '../../store/model/Flow';
import {Constructor, Field, Method} from '../../lang/Class';
import styled from 'styled-components';
import {observable} from 'mobx';
import {observer} from 'mobx-react-lite';
import {Interface} from '../../lang/Interface';

export const DocsFlow = observer(() => {
    const method: Method = {
        name: 'test',
        parameters: []
    }
    const entry: ClassEntry = observable({
        children: [{
            name: 'test',
            type: StructureType.METHOD,
            method: method,
            flow: flowImplementation(method)
        }],
        class: {
            type: {
                name: 'Test',
                namespace: 'test'
            },
            interfaces: [],
            constructors: [],
            methods: [method],
            fields: [],
        },
        name: '',
        type: StructureType.CLASS
    });
    return (
        <div>
            <h1>Flow</h1>
            <CanvasBackground>
                <FlowCanvas entry={entry.children[0]}/>
            </CanvasBackground>
            <DocsCodeView entry={entry}/>
        </div>
    )
})

const CanvasBackground = styled.div`
  margin-left: 46px;
  background-color: ${({theme}) => theme.color.backgroundDark};
  height: 10rem;
  border-left: 1px solid ${({theme}) => theme.color.borderLight};
  border-top: 1px solid ${({theme}) => theme.color.borderLight};
  border-right: 1px solid ${({theme}) => theme.color.borderLight};
`