import React from 'react';
import styled from 'styled-components';
import {observer} from 'mobx-react-lite';
import {Route, Routes} from 'react-router-dom';
import Editor from './Editor';
import {Docs} from './docs/Docs';

const App = observer(() => {
    return (
        <AppContainer>
            <Routes>
                <Route path={'/'} element={<Editor/>}/>
                <Route path={'/docs/*'} element={<Docs/>}/>
            </Routes>
        </AppContainer>
    );
});

const AppContainer = styled.div`
  height: 99.9vh;
  display: flex;
  width: 99.9%;
  flex-direction: column;
`

export default App;