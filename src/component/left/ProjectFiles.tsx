import React, {useState} from 'react';
import styled, {DefaultTheme} from 'styled-components';
import {WindowHeaderHorizontal} from '@styled-icons/fluentui-system-filled/WindowHeaderHorizontal';
import {ChevronDown} from '@styled-icons/boxicons-solid/ChevronDown';
import {ChevronUp} from '@styled-icons/boxicons-solid/ChevronUp';
import StructureType from '../../store/model/StructureType';
import {useVisibility} from '../../hooks/useVisibility';
import {useAppStore} from '../../store/AppStore';
import {observer} from 'mobx-react-lite';
import {
    children,
    FunctionalStructureEntryType,
    isFunctional, ParentEntryType,
    StructureEntryType
} from '../../store/model/StructureEntry';
import {MenuModal, MenuSectionDefinition} from '../shared/MenuModal';
import {Trash2Fill} from '@styled-icons/bootstrap/Trash2Fill';
import {Edit} from '@styled-icons/boxicons-solid/Edit';
import {Constructor, Method} from '../../lang/Class';
import {NameCallback, NameInputModal} from '../shared/NameInputModal';
import {flowImplementation} from '../../store/model/Flow';
import {useTabs} from '../../hooks/useTabs';
import {observable} from 'mobx';
import {entryIcon, entryIconType} from '../shared/Icons';

const ProjectFiles = observer(() => {
    const {currentProject} = useAppStore();
    return (
        <ProjectFilesContainer>
            <ProjectFilesHeader>
                <WindowHeaderHorizontal/>
                Structure
            </ProjectFilesHeader>
            <ProjectFileTree>
                <ProjectEntry root={true} entry={currentProject.structure}/>
            </ProjectFileTree>
        </ProjectFilesContainer>
    )
})


const sanitizingRegex = /[^a-zA-Z0-9_]/g;
const spaceRegex = /[ ]/g;
const sanitizeName = (name: string): string => name.replaceAll(spaceRegex, '_').replaceAll(sanitizingRegex, '');

const classFileName = (name: string): string => {
    if (name.endsWith('.vl')) {
        return sanitizeName(name.replaceAll(/.vl/g, '')) + '.vl';
    }
    return sanitizeName(name) + '.vl';
}

const className = (name: string): string => {
    return name.replaceAll(/\.vl/g, '');
}

const namespace = (entry: ParentEntryType): string => {
    if (!entry.parent) {
        return '';
    }
    let e = entry;
    let namespaceParts = [e.name];
    while (e.parent) {
        e = e.parent;
        if (e.parent) {
            namespaceParts.push(e.name);
        }
    }
    return namespaceParts.reverse().join('.');
}

interface ProjectEntryProps {
    entry: StructureEntryType;
    root: boolean;
}

const ProjectEntry =  observer(({entry, root}: ProjectEntryProps) => {
    const store = useAppStore();
    const {visible, toggle} = useVisibility(true);
    const modalVisibility = useVisibility(false);
    const [addEntry, setAddEntry] = useState<NameCallback>(() => () => {});
    const isNotRoot = () => !root;
    const {openTab} = useTabs();
    const section: MenuSectionDefinition = {
        items: [
            {
                text: 'Add', enabled: () => [StructureType.DIRECTORY, StructureType.CLASS].includes(entry.type),
                children: [
                    {
                        items: [
                            {
                                text: 'Directory', enabled: () => entry.type === StructureType.DIRECTORY, icon: entryIconType(StructureType.DIRECTORY), onClick() {
                                    setAddEntry(() => (name: string) => {
                                        if (entry.type === StructureType.DIRECTORY) {
                                            store.addStructure({
                                                name: sanitizeName(name),
                                                type: StructureType.DIRECTORY,
                                                children: [],
                                                parent: entry
                                            }, entry);
                                        }
                                    });
                                    modalVisibility.show();
                                }
                            },
                            {
                                text: 'Class', enabled: () => entry.type === StructureType.DIRECTORY, icon: entryIconType(StructureType.CLASS), onClick() {
                                    setAddEntry(() => (name: string) => {
                                        if (entry.type === StructureType.DIRECTORY) {
                                            entry.children.push({
                                                name: classFileName(name),
                                                type: StructureType.CLASS,
                                                children: [],
                                                parent: entry,
                                                class: {
                                                    type: {
                                                        name: className(classFileName(name)),
                                                        namespace: namespace(entry)
                                                    },
                                                    interfaces: [],
                                                    constructors: [],
                                                    methods: [],
                                                    fields: []
                                                }
                                            })
                                        }
                                    })
                                    modalVisibility.show();
                                }
                            },
                            {
                                text: 'Interface', enabled: () => entry.type === StructureType.DIRECTORY, icon: entryIconType(StructureType.INTERFACE), onClick() {
                                    setAddEntry(() => (name: string) => {
                                        if (entry.type === StructureType.DIRECTORY) {
                                            entry.children.push({
                                                name: classFileName(name),
                                                type: StructureType.INTERFACE,
                                                children: [],
                                                parent: entry,
                                                interface: {
                                                    type: {
                                                        name: className(classFileName(name)),
                                                        namespace: namespace(entry)
                                                    },
                                                    methods: [],
                                                    interfaces: []
                                                }
                                            })
                                        }
                                    })
                                    modalVisibility.show();
                                }
                            },
                            {
                                text: 'Function', enabled: () => entry.type === StructureType.DIRECTORY, icon: entryIconType(StructureType.FUNCTION), onClick() {
                                    setAddEntry(() => (name: string) => {
                                        if (entry.type === StructureType.DIRECTORY) {
                                            const func = {
                                                type: {
                                                    name: name,
                                                    namespace: namespace(entry)
                                                },
                                                parameters: []
                                            };
                                            entry.children.push({
                                                name: classFileName(name),
                                                type: StructureType.FUNCTION,
                                                parent: entry,
                                                function: func,
                                                flow: flowImplementation(func)
                                            })
                                        }
                                    })
                                    modalVisibility.show();
                                }
                            },
                            {
                                text: 'Method', enabled: () => entry.type === StructureType.CLASS, icon: entryIconType(StructureType.METHOD), onClick() {
                                    setAddEntry(() => (name: string) => {
                                        if (entry.type === StructureType.CLASS) {
                                            const method: Method = observable({
                                                name: sanitizeName(name),
                                                parameters: [],
                                            });
                                            entry.children.push({
                                                name: sanitizeName(name),
                                                type: StructureType.METHOD,
                                                parent: entry,
                                                method: method,
                                                flow: flowImplementation(method)
                                            });
                                            entry.class.methods.push(method);
                                        }
                                    })
                                    modalVisibility.show();
                                }
                            },
                            {
                                text: 'Constructor', enabled: () => entry.type === StructureType.CLASS, icon: entryIconType(StructureType.CONSTRUCTOR), onClick() {
                                    setAddEntry(() => (name: string) => {
                                        if (entry.type === StructureType.CLASS) {
                                            const constructor: Constructor = observable({
                                                parameters: [],
                                                name: sanitizeName(name)
                                            });
                                            entry.children.push({
                                                name: sanitizeName(name),
                                                type: StructureType.CONSTRUCTOR,
                                                parent: entry,
                                                constructor: constructor,
                                                flow: flowImplementation(constructor)
                                            });
                                            entry.class.constructors.push(constructor);
                                        }
                                    })
                                    modalVisibility.show();
                                }
                            },
                        ]
                    }
                ]
            },
            {text: 'Rename', icon: Edit, enabled: isNotRoot},
            {
                text: 'Delete', icon: Trash2Fill, enabled: isNotRoot, onClick() {
                    store.removeEntry(entry);
                }
            }
        ]
    };
    const entryChildren = children(entry);
    const match = store.currentProject.currentTab === entry;
    const addTab = () => {
        if (isFunctional(entry)) {
            openTab(entry as FunctionalStructureEntryType);
        }
    }
    return (
        <ProjectDirectoryContainer root={root} hasChildren={entryChildren.length > 0}>
            <ProjectEntryContainer>
                <ProjectEntryItemContainer match={match} root={root} onClick={addTab}>
                    {entryChildren.length > 0 && (visible ? <ChevronDown title={'Collapse'} onClick={toggle}/> : <ChevronUp title={'Show'} onClick={toggle}/>)}
                    {entryIcon(entry.type)}
                    {entry.name}
                </ProjectEntryItemContainer>
                <ProjectEntryMenuContainer>
                    <MenuModal sections={[section]}/>
                </ProjectEntryMenuContainer>
            </ProjectEntryContainer>
            {visible && children(entry).map((entry: any) => <ProjectEntry key={entry.name} root={false} entry={entry}/>)}
            <NameInputModal name={'Add structure'} callback={addEntry} visibility={modalVisibility}/>
        </ProjectDirectoryContainer>
    );
});

const ProjectFilesHeader = styled.div`
  padding-left: ${({theme}) => theme.spacing.small};
  color: ${({theme}) => theme.color.ternaryFont};
  display: flex;
  align-items: center;
  height: 2rem;
  width: 100%;
  border-bottom: 1px solid ${({theme}) => theme.color.borderDark};
`

const ProjectFilesContainer = styled.aside`
  background-color: ${({theme}) => theme.color.backgroundLight};
  height: 100%;
  border-right: 1px solid ${({theme}) => theme.color.borderDark};
  border-left: 1px solid ${({theme}) => theme.color.borderDark};
  display: flex;
  flex-direction: column;
`

const ProjectFileTree = styled.div`
  margin-top: ${({theme}) => theme.spacing.normal};
  color: ${({theme}) => theme.color.primaryFont};
`

interface ProjectDirectoryContainerProps {
    hasChildren: boolean;
    root: boolean;
    theme?: DefaultTheme;
}

const ProjectEntryMenuContainer = styled.div`
  position: absolute;
  top: -0.82rem;
`

const ProjectDirectoryContainer = styled.div<ProjectDirectoryContainerProps>`
  padding-left: ${({theme, root}) => root ? 0 : theme.spacing.large};
  margin-left: ${({theme, hasChildren}) => hasChildren ? 0 : theme.spacing.medium};
  margin-top: 0;
  margin-bottom: 0;
  color: ${({theme}) => theme.color.primaryFont};
`

const ProjectEntryContainer = styled.div`
  > ${ProjectEntryMenuContainer} {
    opacity: 0;
    visibility: hidden;
    transition: all;
  }

  :hover > ${ProjectEntryMenuContainer} {
    opacity: 1;
    visibility: visible;
    transition: all;
    transition-delay: 0.5s;
  }

  position: relative;
`

interface ProjectEntryContainerProps {
    match: boolean;
    root: boolean;
    theme?: DefaultTheme;
}

const ProjectEntryItemContainer = styled.div<ProjectEntryContainerProps>`
  display: flex;
  align-items: center;
  margin-bottom: ${({theme}) => theme.spacing.tiny};
  font-weight: ${({root, match}) => root || match ? 'bold' : 'normal'};
  color: ${({theme, root, match}) => root || match ? theme.color.ternaryFont : theme.color.primaryFont};
  background-color: ${({theme, match}) => match ? theme.color.primaryLight : 'auto'};

  :hover {
    cursor: pointer;
    background-color: ${({theme}) => theme.color.primaryLight};
  }
`

export default ProjectFiles;