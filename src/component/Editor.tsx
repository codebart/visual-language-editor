import React from 'react';
import styled from 'styled-components';
import MainMenu from './top/MainMenu';
import LocationMenu from './top/LocationMenu';
import ProjectFiles from './left/ProjectFiles';
import Properties from './right/Properties';
import ProjectStatus from './bottom/ProjectStatus';
import FileTabs from './center/FileTabs';
import {useAppStore} from '../store/AppStore';
import {observer} from 'mobx-react-lite';
import {FlowCanvas} from './center/flow/FlowCanvas';
import {CodeView} from './center/CodeView';
import StructureType from '../store/model/StructureType';
import {Console} from './center/Console';
import {MenuBar} from './top/MenuBar';
import {Compilation} from './top/Compilation';
import {ClassEntry, FlowEntryType} from '../store/model/StructureEntry';

const Editor = observer(() => {
    const {projectOpen, currentProject} = useAppStore();
    const showFlow = currentProject?.currentTab?.type === StructureType.METHOD
        || currentProject?.currentTab?.type === StructureType.FUNCTION
        || currentProject?.currentTab?.type === StructureType.CONSTRUCTOR;
    const showCode = currentProject?.currentTab?.type === StructureType.CLASS
        || currentProject?.currentTab?.type === StructureType.FUNCTION
        || currentProject?.currentTab?.type === StructureType.INTERFACE;
    const tab = currentProject?.currentTab as ClassEntry
    return (
        <EditorContainer>
            <TopLayout>
                <MainMenu/>
                {projectOpen && (
                    <MenuBar>
                        <LocationMenu/>
                        <Compilation/>
                    </MenuBar>
                )}
            </TopLayout>
            {projectOpen && (
                <CenterLayout>
                    <LeftLayout>
                        <ProjectFiles/>
                    </LeftLayout>
                    <ContentLayout>
                        <FileTabs/>
                        {showCode && <CodeView entry={tab} compilationTarget={currentProject.compilationTarget}/>}
                        {showFlow && <FlowCanvas entry={currentProject.currentTab as FlowEntryType}/>}
                        <Console/>
                    </ContentLayout>
                    <RightLayout>
                        <Properties/>
                    </RightLayout>
                </CenterLayout>
            )}
            {projectOpen && (
                <BottomLayout>
                    <ProjectStatus/>
                </BottomLayout>
            )}
            {!projectOpen && (
                <CreateNewProject>
                    Create a new project
                </CreateNewProject>
            )}
        </EditorContainer>
    );
});

const CreateNewProject = styled.div`
  height: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: ${({theme}) => theme.font.huge};
  color: ${({theme}) => theme.color.secondaryFont};
`

const EditorContainer = styled.div`
  height: 99.9vh;
  display: flex;
  width: 99.9%;
  flex-direction: column;
`

const TopLayout = styled.div`
  width: 100%;
`

const LeftLayout = styled.div`
  height: 100%;
  min-width: 20rem;
`

const RightLayout = styled.div`
  max-width: 15rem;
`

const ContentLayout = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`

const CenterLayout = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
`

const BottomLayout = styled.div`
  height: 1.5rem;
`

export default Editor;