import React from 'react';
import styled from 'styled-components';
import {observer} from 'mobx-react-lite';
import {useAppStore} from '../../store/AppStore';
import {Save} from '@styled-icons/material-rounded/Save';

const ProjectStatus = observer(() => {
    const store = useAppStore();
    return (
        <ProjectStatusContainer>
            <Save/> Last saved: {store.lastSaved?.toDateString()} {store.lastSaved?.toLocaleTimeString()}
        </ProjectStatusContainer>
    )
})

const ProjectStatusContainer = styled.aside`
  background-color: ${({theme}) => theme.color.backgroundLight};
  height: 100%;
  border-top: 1px solid ${({theme}) => theme.color.borderDark};
  display: flex;
  align-items: center;
  font-size: ${({theme}) => theme.font.small};
  padding-left: ${({theme}) => theme.spacing.small};
`

export default ProjectStatus;