import {StructureEntry} from '../store/model/StructureEntry';

export const routes = {
    home: '/',
    file(entry: StructureEntry) {
        let route = 'file/';
        const path = [];
        while (entry.parent) {
            path.push(entry.name);
            entry = entry.parent;
        }
        return route + path.reverse().join('/');
    }
};