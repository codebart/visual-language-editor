import React, {useCallback} from 'react';
import {Position} from '../model/Position';
import {useLocalObservable} from 'mobx-react-lite';
import {runInAction} from 'mobx';

interface MoveControl {
    onMouseDown(event: React.MouseEvent<HTMLDivElement>): void;
}

interface MoveStore {
    preMovePosition: Position;
    startMovePosition: Position;
    changePreMovePosition(position: Position): void;
    changeStartMovePosition(position: Position): void;
}

export const useMove = (position: Position): MoveControl => {
    const store = useLocalObservable<MoveStore>((): MoveStore => ({
        preMovePosition: {x: 0, y: 0},
        startMovePosition: {x: 0, y: 0},
        changePreMovePosition(position: Position) {
            this.preMovePosition.x = position.x;
            this.preMovePosition.y = position.y;
        },
        changeStartMovePosition(position: Position) {
            this.startMovePosition.x = position.x;
            this.startMovePosition.y = position.y;
        }
    }));
    const onMouseMove = useCallback((event: MouseEvent): void => {
        runInAction(() => {
            position.x = store.preMovePosition.x + (event.screenX - store.startMovePosition.x);
            position.y = store.preMovePosition.y + (event.screenY - store.startMovePosition.y);
        })
    }, [position, store.preMovePosition.x, store.preMovePosition.y, store.startMovePosition.x, store.startMovePosition.y]);
    const onMouseUp = useCallback(() => {
        document.body.style.cursor = 'auto';
        document.removeEventListener('mousemove', onMouseMove, true);
        document.removeEventListener('mouseup', onMouseUp, true);
    }, [onMouseMove]);
    const onMouseDown = useCallback((event: React.MouseEvent<HTMLDivElement>) => {
        if (event.button === 0) {
            event.stopPropagation();
            document.body.style.cursor = 'move';
            document.addEventListener('mousemove', onMouseMove, true);
            document.addEventListener('mouseup', onMouseUp, true);
            store.changePreMovePosition(position);
            store.changeStartMovePosition({x: event.screenX, y: event.screenY});
        }
    }, [onMouseMove, onMouseUp, position, store]);
    return {
        onMouseDown
    }
}