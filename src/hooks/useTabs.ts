import {FunctionalStructureEntryType} from '../store/model/StructureEntry';
import {useCallback} from 'react';
import {useAppStore} from '../store/AppStore';
import {runInAction} from 'mobx';

interface TabsControl {
    tabs: FunctionalStructureEntryType[];
    closeTab(tab: FunctionalStructureEntryType): void;
    openTab(tab: FunctionalStructureEntryType): void;
    selectTab(tab: FunctionalStructureEntryType): void;
    isCurrent(tab: FunctionalStructureEntryType): boolean;
}

export const useTabs = (): TabsControl => {
    const {currentProject, openTab} = useAppStore();
    const closeTab = useCallback((tab: FunctionalStructureEntryType) => {
        runInAction(() => {
            currentProject.tabs.removeElements(t => t === tab);
            if (tab === currentProject.currentTab) {
                if (currentProject.tabs.length > 0) {
                    currentProject.currentTab = currentProject.tabs[0];
                } else {
                    currentProject.currentTab = undefined;
                }
            }
        })
    }, [currentProject]);
    const selectTab = useCallback((tab: FunctionalStructureEntryType) => runInAction(() => currentProject.currentTab = tab), [currentProject]);
    const isCurrent = useCallback((tab: FunctionalStructureEntryType) => runInAction(() => currentProject.currentTab === tab), [currentProject.currentTab]);
    return {
        tabs: currentProject.tabs,
        closeTab: closeTab,
        openTab: openTab,
        selectTab: selectTab,
        isCurrent: isCurrent
    }
}