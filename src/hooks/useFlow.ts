import {detachFlow, Flow} from '../store/model/Flow';
import {useAppStore} from '../store/AppStore';
import StructureType from '../store/model/StructureType';

interface FlowControl {
    removeFlow(): void;
}

export const useFlow = (flow: Flow): FlowControl => {
    const {currentProject} = useAppStore();
    const removeFlow = () => {
        detachFlow(flow);
        switch (currentProject.currentTab?.type) {
            case StructureType.METHOD:
            case StructureType.FUNCTION: {
                currentProject.currentTab?.flow.flows.removeElements(flowElement => flowElement === flow);
            }
        }
    }
    return {
        removeFlow: removeFlow
    }
}