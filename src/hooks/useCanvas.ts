import React, {useCallback, useEffect, useRef} from 'react';
import {Position} from '../model/Position';
import {useLocalObservable} from 'mobx-react-lite';
import {useAppStore} from '../store/AppStore';
import {FlowEntryType, FunctionalStructureEntryType, FunctionEntry, hasFlow} from '../store/model/StructureEntry';
import {ArgumentSocket, Socket, SocketType, flowOutputSockets} from '../store/model/Flow';
import {typeColor} from '../component/center/flow/typeColor';
import {useTheme} from 'styled-components';
import {runInAction} from 'mobx';

interface CanvasControl {
    zoom: number;
    size: Size;
    containerRef: React.RefObject<HTMLDivElement>;
    backgroundRef: React.RefObject<HTMLCanvasElement>;
    onScroll(event: React.WheelEvent): void;
    onMouseDown(event: React.MouseEvent<HTMLDivElement>): void;
}

interface Size {
    width: number;
    height: number;
}

interface CanvasStore {
    zoom: number;
    size: Size;
    viewport: Position;
    preMoveViewport: Position;
    startMoveViewport: Position;
    changeZoom(zoom: number): void;
    changeSize(width: number, height: number): void;
    changeViewport(viewport: Position): void;
    changePreMoveViewport(viewport: Position): void;
    changeStartMoveViewport(viewport: Position): void;
}

const baseSocketSize = 15;

const useCanvas = (viewport: Position, entry: FlowEntryType): CanvasControl => {
    const theme = useTheme();
    const containerRef = useRef<HTMLDivElement>(null);
    const backgroundRef = useRef<HTMLCanvasElement>(null);
    const {canvas: canvasStore, lastUpdated} = useAppStore();
    const store = useLocalObservable<CanvasStore>((): CanvasStore => ({
        zoom: 1,
        size: {width: 0, height: 0},
        viewport: viewport,
        preMoveViewport: {x: 0, y: 0},
        startMoveViewport: {x: 0, y: 0},
        changeZoom(zoom: number) {
            this.zoom = zoom;
        },
        changeSize(width: number, height: number) {
            this.size = {width, height};
        },
        changeViewport(viewport: Position) {
            this.viewport.x = viewport.x;
            this.viewport.y = viewport.y;
        },
        changePreMoveViewport(viewport: Position) {
            this.preMoveViewport.x = viewport.x;
            this.preMoveViewport.y = viewport.y;
        },
        changeStartMoveViewport(viewport: Position) {
            this.startMoveViewport.x = viewport.x;
            this.startMoveViewport.y = viewport.y;
        }
    }));
    useEffect(() => {
        runInAction(() => store.viewport = viewport);
    }, [store, viewport]);
    const socketSize = useCallback(() => {
        return baseSocketSize * store.zoom;
    }, [store.zoom]);
    const onScroll = (event: React.WheelEvent) => {
        let newZoom = store.zoom;
        if (event.deltaY > 0) {
            newZoom *= 0.9;
        } else {
            newZoom *= 1.1;
        }
        const zoomDiff = Math.abs(newZoom - 1);
        if (zoomDiff < 0.05 ) {
            store.changeZoom(1);
        } else {
            store.changeZoom(newZoom);
        }
    };
    const drawGrid = useCallback((canvas: HTMLCanvasElement) => {
        const context = canvas.getContext('2d');
        const cellSize = 15 * store.zoom;
        if (context) {
            const width = canvas.width;
            const height = canvas.height;
            const xDiff = store.viewport.x % cellSize;
            const yDiff = store.viewport.y % cellSize;
            context.clearRect(0, 0, width, height);
            if (store.zoom > 0.3) {
                context.beginPath();
                for (let x = xDiff; x <= width; x += cellSize) {
                    context.moveTo(0.5 + x, 0);
                    context.lineTo(0.5 + x, height);
                }
                for (let y = yDiff; y <= height; y += cellSize) {
                    context.moveTo(0, 0.5 + y);
                    context.lineTo(width, 0.5 + y);
                }
                context.strokeStyle = '#3c3f41';
                context.stroke();
            }
        }
    }, [store]);
    const socketLineColor = useCallback((socket: Socket) => {
        switch (socket.socketType) {
            case SocketType.ARGUMENT:
                return typeColor((socket as ArgumentSocket).argumentType, theme);
            case SocketType.FLOW:
                return '#FFFFFF';
        }
    }, [theme]);
    const drawBezierCurve = useCallback((context: CanvasRenderingContext2D, from: Position, to: Position, color: string): void => {
        context.beginPath();
        context.moveTo(from.x, from.y);
        context.bezierCurveTo(
            to.x,
            from.y,
            from.x,
            to.y,
            to.x,
            to.y
        );
        context.lineWidth = store.zoom;
        context.strokeStyle = color;
        context.stroke();
    }, [store.zoom]);
    const drawLines = useCallback((canvas: HTMLCanvasElement) => {
        const context = canvas.getContext('2d');
        const ref = containerRef.current;
        if (context && ref && entry && hasFlow(entry)) {
            const flowImplementation = entry.flow;
            for (let flow of flowImplementation.flows) {
                const sockets = flowOutputSockets(flow);
                for (let socket of sockets) {
                    for (let connectedSocket of socket.connectedSockets) {
                        const startSocketElement = canvasStore.sockets.get(socket);
                        const endSocketElement = canvasStore.sockets.get(connectedSocket);
                        if (startSocketElement && endSocketElement) {
                            const start = {
                                x: startSocketElement.getBoundingClientRect().left - ref.getBoundingClientRect().left + socketSize(),
                                y: startSocketElement.getBoundingClientRect().top - ref.getBoundingClientRect().top + socketSize() / 2
                            };
                            const end = {
                                x: endSocketElement.getBoundingClientRect().left - ref.getBoundingClientRect().left + socketSize(),
                                y: endSocketElement.getBoundingClientRect().top - ref.getBoundingClientRect().top + socketSize() / 2
                            }
                            drawBezierCurve(context, start, end, socketLineColor(socket));
                        }
                    }
                }
            }
            const selectedSocket = canvasStore.selectedSocket;
            if (selectedSocket) {
                const start = {
                    x: selectedSocket.socketElement.getBoundingClientRect().left - ref.getBoundingClientRect().left + socketSize(),
                    y: selectedSocket.socketElement.getBoundingClientRect().top - ref.getBoundingClientRect().top + socketSize() / 2
                };
                const end = {
                    x: selectedSocket.currentPosition.x - ref.getBoundingClientRect().left,
                    y: selectedSocket.currentPosition.y - ref.getBoundingClientRect().top
                }
                drawBezierCurve(context, start, end, socketLineColor(selectedSocket.socket));
            }
        }
    }, [canvasStore.selectedSocket, canvasStore.sockets, drawBezierCurve, entry, socketLineColor, socketSize]);
    useEffect(() => {
        if (backgroundRef.current) {
            drawGrid(backgroundRef.current);
            drawLines(backgroundRef.current);
        }
    }, [lastUpdated, backgroundRef, drawGrid, store.zoom, store.viewport.x, store.viewport.y, store.size.width, store.size.height, drawLines, canvasStore.selectedSocket?.currentPosition]);
    useEffect(() => {
        if (containerRef.current) {
            store.changeSize(containerRef.current.clientWidth, containerRef.current.clientHeight);
        }
    }, [containerRef, store]);
    useEffect(() => {
        const ref = containerRef.current;
        const resizeObserver = new ResizeObserver(() => {
            if (ref) {
                store.changeSize(ref.clientWidth, ref.clientHeight);
            }
        });
        if (ref) {
            resizeObserver.observe(ref);
        }
        return () => {
            if (ref) {
                resizeObserver.unobserve(ref);
            }
        }
    }, [containerRef, store]);
    const onMouseMove = useCallback((event: MouseEvent): void => {
        store.changeViewport({
            x: store.preMoveViewport.x + (event.screenX - store.startMoveViewport.x) * (1 / store.zoom),
            y: store.preMoveViewport.y + (event.screenY - store.startMoveViewport.y) * (1 / store.zoom)
        });
    }, [store]);
    const onMouseUp = useCallback((event: MouseEvent) => {
        document.body.style.cursor = 'auto';
        document.removeEventListener('mousemove', onMouseMove, true);
        document.removeEventListener('mouseup', onMouseUp, true);
    }, [onMouseMove]);
    const onMouseDown = useCallback((event: React.MouseEvent<HTMLDivElement>) => {
        if (event.button === 0) {
            document.body.style.cursor = 'move';
            document.addEventListener('mousemove', onMouseMove, true);
            document.addEventListener('mouseup', onMouseUp, true);
            store.changePreMoveViewport(store.viewport);
            store.changeStartMoveViewport({x: event.screenX, y: event.screenY});
        }
    }, [onMouseMove, onMouseUp, store]);
    return {
        zoom: store.zoom,
        size: store.size,
        containerRef,
        backgroundRef,
        onScroll,
        onMouseDown
    };
}

export default useCanvas;