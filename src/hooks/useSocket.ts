import React, {useCallback, useEffect, useRef} from 'react';
import {useAppStore} from '../store/AppStore';
import {ArgumentSocket, detachSocket, FlowSocket, Socket, SocketDirection, SocketType} from '../store/model/Flow';
import {runInAction} from 'mobx';
import {typesEqual} from '../lang/Type';

const inputSocket = (...sockets: Socket[]): Socket => sockets.find(socket => socket.direction === SocketDirection.INPUT) as Socket;
const outputSocket = (...sockets: Socket[]): Socket => sockets.find(socket => socket.direction === SocketDirection.OUTPUT) as Socket;

export interface UseSocketControl {
    containerRef: React.RefObject<HTMLDivElement>;
    onMouseDown(event: React.MouseEvent<HTMLDivElement>): void;
    onSocketDrop(): void;
}

export const useSocket = (socket: Socket): UseSocketControl => {
    const {canvas} = useAppStore();
    const containerRef = useRef<HTMLDivElement>(null);
    const onMouseMove = useCallback((event: MouseEvent): void => {
        runInAction(() => {
            if (canvas.selectedSocket) {
                canvas.selectedSocket.currentPosition = {
                    x: event.clientX,
                    y: event.clientY
                };
            }
        })
    }, [canvas.selectedSocket]);
    useEffect(() => {
        if (containerRef.current) {
            canvas.sockets.set(socket, containerRef.current);
        }
    }, [canvas.sockets, socket, containerRef]);
    const onMouseUp = useCallback((event: MouseEvent) => {
        setImmediate(() => canvas.selectedSocket = undefined);
        document.body.style.cursor = 'auto';
        document.removeEventListener('mousemove', onMouseMove, true);
        document.removeEventListener('mouseup', onMouseUp, true);
    }, [canvas, onMouseMove]);
    const onMouseDown = useCallback((event: React.MouseEvent<HTMLDivElement>) => {
        event.stopPropagation();
        if (containerRef.current) {
            canvas.selectedSocket = {
                socket: socket,
                socketElement: containerRef.current,
                currentPosition: {
                    x: event.clientX,
                    y: event.clientY
                }
            }
            document.body.style.cursor = 'grab';
            document.addEventListener('mousemove', onMouseMove, true);
            document.addEventListener('mouseup', onMouseUp, true);
        }
    }, [canvas, onMouseMove, onMouseUp, socket]);
    const onSocketDrop = useCallback(() => {
        if (canvas.selectedSocket && socket !== canvas.selectedSocket.socket && socket.socketType === canvas.selectedSocket.socket.socketType) {
            switch (socket.socketType) {
                case SocketType.ARGUMENT: {
                    const startArgumentSocket = socket as ArgumentSocket;
                    const endArgumentSocket = canvas.selectedSocket.socket as ArgumentSocket;
                    if (startArgumentSocket.direction !== endArgumentSocket.direction && typesEqual(startArgumentSocket.argumentType, endArgumentSocket.argumentType)) {
                        const input = inputSocket(startArgumentSocket, endArgumentSocket);
                        const output = outputSocket(startArgumentSocket, endArgumentSocket);
                        output.connectedSockets.push(input);
                        detachSocket(input);
                        input.connectedSockets.push(output);
                    }
                    break;
                }
                case SocketType.FLOW: {
                    const startFlowSocket = socket as FlowSocket;
                    const endFlowSocket = canvas.selectedSocket.socket as FlowSocket;
                    if (startFlowSocket.direction !== endFlowSocket.direction) {
                        const input = inputSocket(startFlowSocket, endFlowSocket);
                        const output = outputSocket(startFlowSocket, endFlowSocket);
                        input.connectedSockets.push(output);
                        detachSocket(output);
                        output.connectedSockets.push(input);
                    }
                    break;
                }
            }
        }
        canvas.selectedSocket = undefined;
    }, [canvas, socket]);
    return {
        containerRef: containerRef,
        onMouseDown: onMouseDown,
        onSocketDrop: onSocketDrop,
    }
}