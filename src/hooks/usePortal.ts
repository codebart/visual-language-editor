import {useLayoutEffect, useState} from 'react';

const createRootElement = (id: string): HTMLElement => {
    const rootContainer = document.createElement('div');
    rootContainer.setAttribute('id', id);
    return rootContainer;
}

const usePortal = (id: string, opened: boolean) => {
    const [container, setContainer] = useState<HTMLElement>(createRootElement(id));
    useLayoutEffect(() => {
        const currentPortal = document.getElementById(id);
        if (opened && !currentPortal) {
            const portal = createRootElement(id);
            document.body.append(portal);
            setContainer(portal);
        } else if (!opened && currentPortal) {
            currentPortal.remove();
        }
        return () => container?.remove();
    }, [id, opened, container, setContainer])
    return container;
}

export default usePortal;