import {useCallback, useState} from 'react';

export interface VisibilityControl {
    visible: boolean;
    show(): void;
    hide(): void;
    toggle(): void;
}

export const useVisibility = (initial: boolean = false): VisibilityControl => {
    const [visible, setVisible] = useState(initial);
    const changeVisibility = useCallback((change: boolean) => {
        setVisible(change);
    }, [setVisible]);
    const show = useCallback(() => changeVisibility(true), [changeVisibility])
    const hide = useCallback(() => changeVisibility(false), [changeVisibility])
    const toggle = useCallback(() => changeVisibility(!visible), [visible, changeVisibility])
    return {
        visible,
        show,
        hide,
        toggle
    }
}