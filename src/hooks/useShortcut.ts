import {useCallback, useEffect} from 'react';

interface ShortcutConfiguration {
    key: string;
    alt?: boolean;
    shift?: boolean;
    ctrl?: boolean;
    input?: boolean;
}

export const useShortcut = (key: ShortcutConfiguration, callback: () => void) => {
    const shortcutCheck = useCallback((e: KeyboardEvent) => {
        if (e.key.toLowerCase() === key.key.toLowerCase()
            && ((key.alt && e.altKey) || !key.alt)
            && ((key.ctrl && e.ctrlKey) || !key.ctrl)
            && ((key.shift && e.shiftKey) || !key.shift)
            && ((!key.input && !isActiveElementContentEditable() && !isActiveElementInput()) || !!key.input)) {
            callback();
        }
    }, [key, callback])
    useEffect(() => {
        document.addEventListener('keydown', shortcutCheck);
        return () => document.removeEventListener('keydown', shortcutCheck);
    }, [shortcutCheck])
}

const isActiveElementInput = () => document.activeElement?.tagName === 'INPUT';
const isActiveElementContentEditable = () => document.activeElement?.getAttribute('contenteditable') === 'true';