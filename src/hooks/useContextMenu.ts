import {useVisibility, VisibilityControl} from './useVisibility';
import React, {useCallback, useState} from 'react';
import {Position} from '../model/Position';

interface ContextMenuControl {
    position: Position;
    visibility: VisibilityControl;
    onContextMenu(event: React.MouseEvent<HTMLDivElement>): void;
}

export const useContextMenu = (): ContextMenuControl => {
    const visibility = useVisibility(false);
    const [position, setPosition] = useState<Position>({x: 0, y: 0});
    const onContextMenu = useCallback((event: React.MouseEvent<HTMLDivElement>) => {
        event.preventDefault();
        setPosition({
            x: event.clientX,
            y: event.clientY
        });
        visibility.show();
    }, [visibility]);
    return {
        position,
        visibility,
        onContextMenu
    }
}