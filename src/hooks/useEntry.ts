import {ConstructorEntry, FunctionalStructureEntryType, MethodEntry, ParentEntryType} from '../store/model/StructureEntry';
import StructureType from '../store/model/StructureType';
import {anonymousInputSocket, detachFlow, detachSocket, flowImplementation, FlowType, parameterSocket} from '../store/model/Flow';
import {Constructor, containsMethod, Field, Method} from '../lang/Class';
import {Parameter} from '../lang/Parameter';
import {Type} from '../lang/Type';
import {useAppStore} from '../store/AppStore';
import {observable} from 'mobx';
import {Interface} from '../lang/Interface';

const methodEntry = (method: Method, parent: ParentEntryType): MethodEntry => ({
    name: method.name,
    type: StructureType.METHOD,
    method: method,
    parent: parent,
    flow: flowImplementation(method)
});

interface EntryControl {
    addMethod(name: string): void;
    removeMethod(method: Method): void;
    addConstructor(name: string): void;
    removeConstructor(constructor: Constructor): void;
    addField(field: Field): void;
    removeField(field: Field): void;
    addParameter(parameter: Parameter): void;
    removeParameter(parameter: Parameter): void;
    addReturnType(type: Type): void;
    removeReturnType(): void;
    addInterface(iface: Interface): void;
    removeInterface(iface: Interface): void;
}

export const useEntry = (entry: FunctionalStructureEntryType): EntryControl => {
    const store = useAppStore();
    const addMethod = (name: string): void => {
        const method = observable({
            name: name,
            parameters: []
        });
        switch (entry.type) {
            case StructureType.CLASS: {
                entry.class.methods.push(method);
                entry.children.push(methodEntry(method, entry));
                break;
            }
            case StructureType.INTERFACE: {
                entry.interface.methods.push(method);
                entry.children.push(methodEntry(method, entry));
                break;
            }
        }
    };
    const removeMethod = (method: Method): void => {
        switch (entry.type) {
            case StructureType.CLASS: {
                const methodEntry = entry.children.find((child: MethodEntry | ConstructorEntry) => child.name === method.name) as MethodEntry;
                store.removeEntry(methodEntry);
                entry.class.methods.removeElements((classMethod: Method) => classMethod === method);
                break;
            }
            case StructureType.INTERFACE: {
                const methodEntry = entry.children.find(child => child.name === method.name) as MethodEntry;
                store.removeEntry(methodEntry);
                entry.interface.methods.removeElements((interfaceMethod: Method) => interfaceMethod === method);
                break;
            }
        }
    };
    const addConstructor = (name: string): void => {
        if (entry.type === StructureType.CLASS) {
            const constructor = observable({
                name: name,
                basic: true,
                parameters: []
            });
            entry.class.constructors.push(constructor);
            entry.children.push({
                name: name,
                type: StructureType.CONSTRUCTOR,
                constructor: constructor,
                parent: entry,
                flow: flowImplementation(constructor)
            });
        }
    };
    const removeConstructor = (constructor: Constructor): void => {
        if (entry.type === StructureType.CLASS) {
            const constructorEntry = entry.children.find((child: MethodEntry | ConstructorEntry) => child.name === constructor.name) as ConstructorEntry;
            store.removeEntry(constructorEntry);
        }
    };
    const addField = (field: Field): void => {
        if (entry.type === StructureType.CLASS) {
            entry.class.fields.push(field);
        }
    };
    const removeField = (field: Field): void => {
        if (entry.type === StructureType.CLASS) {
            entry.class.fields.removeElements((classField: Field) => classField.name === field.name);
            for (let child of entry.children) {
                const removedFlows = child.flow.flows.removeElements(flow => flow.flowType === FlowType.FIELD && flow.field === field);
                removedFlows.forEach(detachFlow);
            }
        }
    };
    const addParameter = (parameter: Parameter): void => {
        switch (entry.type) {
            case StructureType.FUNCTION: {
                entry.function.parameters.push(parameter);
                entry.flow.entryPoint.outputSockets.push(parameterSocket(parameter));
                break;
            }
            case StructureType.CONSTRUCTOR: {
                entry.constructor.parameters.push(parameter);
                entry.flow.entryPoint.outputSockets.push(parameterSocket(parameter));
                break;
            }
            case StructureType.METHOD: {
                entry.method.parameters.push(parameter);
                entry.flow.entryPoint.outputSockets.push(parameterSocket(parameter));
                break;
            }
        }
    };
    const removeParameter = (parameter: Parameter): void => {
        let parameters: Parameter[] = [];
        switch (entry.type) {
            case StructureType.FUNCTION: {
                parameters = entry.function.parameters;
                break;
            }
            case StructureType.CONSTRUCTOR: {
                parameters = entry.constructor.parameters;
                break;
            }
            case StructureType.METHOD: {
                parameters = entry.method.parameters;
                break;
            }
        }
        switch (entry.type) {
            case StructureType.FUNCTION:
            case StructureType.CONSTRUCTOR:
            case StructureType.METHOD: {
                for (let flow of entry.flow.flows) {
                    if (flow.flowType === FlowType.PARAMETERS) {
                        flow.outputSockets.removeElements(outputSocket => outputSocket.name === parameter.name).forEach(detachSocket);
                    }
                }
            }
        }
        parameters.removeElements((element: Parameter) => element === parameter);
    };
    const addReturnType = (type: Type): void => {
        switch (entry.type) {
            case StructureType.FUNCTION: {
                entry.function.return = type;
                break;
            }
            case StructureType.METHOD: {
                entry.method.return = type;
                break;
            }
        }
        switch (entry.type) {
            case StructureType.FUNCTION:
            case StructureType.METHOD: {
                for (let flow of entry.flow.flows) {
                    if (flow.flowType === FlowType.RETURN) {
                        flow.inputSocket = anonymousInputSocket(type);
                    }
                }
            }
        }
    };
    const removeReturnType = (): void => {
        switch (entry.type) {
            case StructureType.FUNCTION: {
                entry.function.return = undefined;
                break;
            }
            case StructureType.METHOD: {
                entry.method.return = undefined;
                break;
            }
        }
        switch (entry.type) {
            case StructureType.FUNCTION:
            case StructureType.METHOD: {
                for (let flow of entry.flow.flows) {
                    if (flow.flowType === FlowType.RETURN) {
                        if (flow.inputSocket) {
                            detachSocket(flow.inputSocket);
                            flow.inputSocket = null;
                        }
                    }
                }
            }
        }
    };
    const addInterface = (iface: Interface): void => {
        switch (entry.type) {
            case StructureType.CLASS: {
                if (!entry.class.interfaces.includes(iface)) {
                    entry.class.interfaces.push(iface);
                    const methodsToAdd = iface.methods.filter(method => !containsMethod(entry.class.methods, method));
                    entry.class.methods.push(...methodsToAdd);
                    entry.children.push(...methodsToAdd.map(method => methodEntry(method, entry)));
                }
                break;
            }
            case StructureType.INTERFACE: {
                if (!entry.interface.interfaces.includes(iface) && entry.interface !== iface) {
                    entry.interface.interfaces.push(iface);
                }
                break;
            }
        }
    };
    const removeInterface = (iface: Interface): void => {
        switch (entry.type) {
            case StructureType.CLASS: {
                entry.class.interfaces.removeElements(i => i === iface);
                break;
            }
            case StructureType.INTERFACE: {
                entry.interface.interfaces.removeElements(i => i === iface);
                break;
            }
        }
    };
    return {
        addConstructor,
        addField,
        addMethod,
        addParameter,
        addReturnType,
        removeConstructor,
        removeField,
        removeMethod,
        removeParameter,
        removeReturnType,
        addInterface,
        removeInterface,
    }
}