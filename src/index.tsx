import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './component/App';
import reportWebVitals from './reportWebVitals';
import {ThemeProvider} from 'styled-components';
import darkTheme from './theme/dark';
import GlobalStyle from './theme/GlobalStyle';
import {AppStoreProvider} from './store/AppStore';
import './extensions';
import {BrowserRouter} from 'react-router-dom';

ReactDOM.render(
    <React.StrictMode>
        <AppStoreProvider>
            <ThemeProvider theme={darkTheme}>
                <GlobalStyle/>
                <BrowserRouter>
                    <App/>
                </BrowserRouter>
            </ThemeProvider>
        </AppStoreProvider>
    </React.StrictMode>,
    document.getElementById('root')
);

reportWebVitals();
