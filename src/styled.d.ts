import 'styled-components';

declare module 'styled-components' {
    export interface DefaultTheme {
        color: {
            primary: string;
            primaryLight: string;
            primaryDark: string;
            secondary: string;
            secondaryLight: string;
            background: string;
            backgroundLight: string;
            backgroundDark: string;
            primaryFont: string;
            secondaryFont: string;
            ternaryFont: string;
            borderLight: string;
            borderDark: string;
            orange: string;
            red: string;
            darkRed: string;
            green: string;
            lightGreen: string;
            pink: string;
            purple: string;
            blue: string;
            lightBlue: string;
        },
        font: {
            family: string;
            tiny: string;
            small: string;
            normal: string;
            medium: string;
            large: string;
            huge: string;
        },
        spacing: {
            tiny: string;
            small: string;
            normal: string;
            medium: string;
            large: string;
            huge: string;
            massive: string;
        },
        size: {
            mini: string;
            tiny: string;
            small: string;
            normal: string;
            medium: string;
            large: string;
            big: string;
            huge: string;
            massive: string;
        },
        flow: {
            color: {
                boolean: string;
                string: string;
                integer: string;
                float: string;
                array: string;
                object: string;
                method: string;
                constructor: string;
                function: string;
                field: string;
                any: string;
            }
        }
    }
}