declare global {
    interface Array<T> {
        removeElements(predicate: (value: T) => boolean): T[];
        clear(): T[];
        containsAll(array: T[]): boolean;
    }
    interface String {
        uncapitalize(): string;
    }
}

// eslint-disable-next-line no-extend-native
Array.prototype.removeElements = function<T>(predicate: (value: T) => boolean): T[] {
    const removedElements: T[] = [];
    for (let i = this.length - 1; i >= 0; --i) {
        if (predicate(this[i])) {
            removedElements.push(this[i]);
            this.splice(i, 1);
        }
    }
    return removedElements;
}

// eslint-disable-next-line no-extend-native
Array.prototype.clear = function<T>(): T[] {
    const elements: T[] = [...this];
    this.splice(0, this.length);
    return elements;
}

// eslint-disable-next-line no-extend-native
Array.prototype.containsAll = function<T>(array: T[]): boolean {
    return array && this.every(element => array.includes(element))
}

// eslint-disable-next-line no-extend-native
String.prototype.uncapitalize = function(): string {
    return this.charAt(0).toLowerCase() + this.slice(1);
}

export {};