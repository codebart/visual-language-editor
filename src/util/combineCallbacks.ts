const combineCallbacks = (...callbacks: any) => {
    return (...args: any) => {
        for (let callback of callbacks) {
            callback(...args);
        }
    }
}

export default combineCallbacks;