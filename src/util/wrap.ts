export const wrap = (fn: (...args: any[]) => any): (...args: any[]) => () => any => {
    return (...args: any[]) => () => fn(...args);
};