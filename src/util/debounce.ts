export const debounce = (func: (...args: any[]) => any, timeout: number): (...args: any[]) => any => {
    let timer: any;
    return (...args: any[]) => {
        clearTimeout(timer);
        timer = setTimeout(() => func(...args), timeout);
    };
}